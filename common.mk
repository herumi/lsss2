
# common definition for Makefile
# for GNU c++
#CCACHE=$(shell eval ls /usr/local/bin/ccache 2>/dev/null)

CXX = g++ #-g
CC = gcc #-g
LD = g++
CP = cp -f
AR = ar r
MKDIR=mkdir -p
RM=rm -rf
COREI7=-march=native 
#########  optimizacion entre archivos
########  -flto 

CFLAGS = -fomit-frame-pointer -DNDEBUG -msse4.2
ifeq ($(CXX),clang++)
CFLAGS += -O3
else
CFLAGS += -Ofast
endif
#CFLAGS = -O3  -fomit-frame-pointer -DNDEBUG -msse2 -mfpmath=sse #-v -mtune=core2
CFLAGS_WARN=-Wall -Wextra -Wformat=2 -Wcast-qual -Wcast-align -Wwrite-strings -Wfloat-equal -Wpointer-arith #-Wswitch-enum -Wstrict-aliasing=2
CFLAGS_ALWAYS = -D_FILE_OFFSET_BITS=64 -fno-operator-names
LDFLAGS =  -lm -lgmpxx -lgmp -lzm -lcrypto $(LIB_DIR) $(INC_DIR)
AS = nasm
AFLAGS = -f elf -D__unix__

BIT=-m32
ifeq ($(shell uname -m),x86_64)
BIT=-m64
endif
ifeq ($(shell uname -s),Darwin)
BIT=-m64
endif
ifeq ($(USE_GMP),1)
CFLAGS += -DMIE_ATE_USE_GMP
endif


ifeq ($(DBG),on)
CFLAGS += -O0 -g3 -fno-omit-frame-pointer -UNDEBUG
LDFLAGS += -g3
else
LDFLAGS += -s
endif

.SUFFIXES: .cpp
.cpp.o:
	$(CXX) -c $< -o $@ $(CFLAGS) $(CFLAGS_WARN) $(CFLAGS_ALWAYS) $(INC_DIR) $(BIT)

.c.o:
	$(CC) -c $< -o $@ $(CFLAGS) $(CFLAGS_WARN) $(CFLAGS_ALWAYS) $(INC_DIR) $(BIT)

INC_DIR= -I../../ate/include -I../../xbyak -I../src -I../include
LIB_DIR= -L../../ate/lib

ARCs = $(shell ls ../../ate/lib/*.a)

