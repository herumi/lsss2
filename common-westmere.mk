
# common definition for Makefile
# for GNU c++
CCACHE=$(shell eval ls /usr/local/bin/ccache 2>/dev/null)

CXX = $(CCACHE) g++ -x c++ -c #-g
CC = $(CCACHE) gcc -c #-g
LD = g++
CP = cp -f
AR = ar r
MKDIR=mkdir -p
RM=rm -rf
COREI7=-march=native 
#########  optimizacion entre archivos
########  -flto 

CFLAGS = -fopenmp -ftree-parallelize-loops=2 -mavx -Ofast -fomit-frame-pointer -DNDEBUG -msse4.2 -flto -mtune=corei7-avx #-fopenmp -mtune=core2
#CFLAGS = -O3  -fomit-frame-pointer -DNDEBUG -msse2 -mfpmath=sse #-v -mtune=core2
####CFLAGS_WARN=-Wall -Wextra -Wformat=2 -Wcast-qual -Wcast-align -Wwrite-strings -Wfloat-equal -Wpointer-arith #-Wswitch-enum -Wstrict-aliasing=2
CFLAGS_ALWAYS = -D_FILE_OFFSET_BITS=64 -fno-operator-names
LDFLAGS =  -lm -lgmpxx -lgmp -lzm -s  $(LIB_DIR) $(INC_DIR) -fopenmp
AS = nasm
AFLAGS = -f elf -D__unix__

BIT=-m32
ifeq ($(shell uname -m),x86_64)
BIT=-m64
endif
ifeq ($(shell uname -s),Darwin)
BIT=-m64
endif

.SUFFIXES: .cpp
.cpp.o:
	$(CXX) $< -o $@ $(CFLAGS) $(CFLAGS_WARN) $(CFLAGS_ALWAYS) $(INC_DIR) $(BIT)

.c.o:
	$(CC) $< -o $@ $(CFLAGS) $(CFLAGS_WARN) $(CFLAGS_ALWAYS) $(INC_DIR) $(BIT)

INC_DIR= -I../src -I../xbyak -I../include -I./miracl  -I./include 
LIB_DIR= -L../lib

# -I../xbyak
