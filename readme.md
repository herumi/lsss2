# implementation of attribute-based encryption

# Compile:

## Checkout:

    git clone https://github.com/herumi/xbyak.git
    git clone https://github.com/herumi/ate-pairing.git
    mv ate-pairing ate
    git clone https://bitbucket.org/herumi/lsss2.git
    (cd lsss2 && git checkout -b work lsss2-released)

## Build:

    (cd ate && make)
    cd lsss2
    make
    make test

# Benchmark:

See [Software implementation of Atribute-Based Encryption](http://sandia.cs.cinvestav.mx/index.php?n=Site.CPABE).

# License:

modified new BSD License (http://opensource.org/licenses/BSD-3-Clause)

# Author

* Eric Zavattoni
* Luis J. Dominguez Perez (ldominguez@tamps.cinvestav.mx, twitter:@luisjdominguezp)
* MITSUNARI Shigeo(herumi@nifty.com, twitter:@herumi)
* Ana H Sanchez-Ramirez
* Tadanori Teruya (tadanori.teruya@gmail.com, [twitter:@t_teruya](https://twitter.com/t_teruya "twitter"))
* Francisco Rodriguez-Henriquez

