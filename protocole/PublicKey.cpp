/**
	@file
	@brief Public Key functions
    @author Zavattoni et al.
	@license modified new BSD license
	http://www.opensource.org/licenses/BSD-3-Clause
*/

#include "PublicKey.h"

std::ostream& operator<<(std::ostream& os, const PublicKey& pk)
{
	serialize(os, pk.g1, NUM_OF_ARRAY(pk.g1));
	serialize(os, pk.g2, NUM_OF_ARRAY(pk.g2));
	os << pk.e_alpha << ' ';
	serialize(os, pk.g_a, NUM_OF_ARRAY(pk.g_a));
	return os;
}

std::istream& operator>>(std::istream& is, PublicKey& pk)
{
	deserialize(is, pk.g1, NUM_OF_ARRAY(pk.g1));
	deserialize(is, pk.g2, NUM_OF_ARRAY(pk.g2));
	is >> pk.e_alpha;
	deserialize(is, pk.g_a, NUM_OF_ARRAY(pk.g_a));
	return is;
}

