/**
	@file
	@brief Setup and KeyGen steps (G22 version)
    @author Zavattoni et al.
	@license modified new BSD license
	http://www.opensource.org/licenses/BSD-3-Clause
*/

#include "ServerG2.h"
#include <string>
#include "bench.h"
#include "cpabe.h"
#include "PrecomputeG2.h"
#include "opti.h"
#include "bn-multi.h"
#include "random.h"

extern uint64_t PT[8][256];

using namespace mie;

void ServerG2::Setup(PublicKeyG2& PK, PrecomputeG2& precompute)
{
	RandInt rg;
	const Fp12 (*fe) = precompute.getFePtr();
	Fp12 fe_alpha[PRECOMPUTEknownS];
	Fp2 alpha_g2[3];
	Fp2 g_a [3];
	Fp12 e_alpha;
	const M_sint* WBPrec = precompute.getWB();
	const M_sint (*BBPrec)[4] = precompute.getBB();
	// @note generating random number
	// @note removed const quantifier
	M_uint alpha = rg.get();

	GT_expoknownSC(e_alpha, alpha, fe, PT, precompute.t256, precompute.Half_Zr);
	precomputeGTknownSC(fe_alpha, e_alpha, precompute.getWB(), precompute.getBB());
	// @note calling not const qualified member function, why?
	precompute.setFeAlpha(fe_alpha);
	// @note generating random number //this is called delta in the paper
	// @note removed const quantifier
	M_uint a = rg.get();
	G2_mulknownSC(g_a, a, precompute.getQsIPtr(), PT, precompute.t256, precompute.Half_Zr);

	if (g_a[2] != 1) bn::ecop::NormalizeJac(g_a, g_a);

	Fp2 g_a_pack[2];
	g_a_pack[0] = g_a[0];
	g_a_pack[1] = g_a[1];
	// @note calling not const qualified member function, why?
	precomputeG2knownSC(precompute.getQsIga_W(), g_a_pack, WBPrec, BBPrec);
	// @note a variable PK appears at first time
	PK.Set_Element(cpabe::Param::getG1Gen(), cpabe::Param::getG2Gen(), e_alpha, g_a);
	G2_mulknownSC(alpha_g2, alpha, precompute.getQsIPtr(), PT, precompute.t256, precompute.Half_Zr);
	if (alpha_g2[2] != 1) bn::ecop::NormalizeJac(alpha_g2, alpha_g2);

	this->MK.SetGalpha(alpha_g2);
}

void ServerG2::KeyGen(KeyG2& UserKey, const Bob& bob, PublicKeyG2* /*PK*/, PrecomputeG2* precompute)
{
	Fp2_map& K_x = UserKey.getK_Att_W();
	RandInt rg;
	const Fp (*PsIPrec)[2] = precompute->getPsI();
	Fp12 e;
	M_uint t;
	Fp2 at_g2_b [3];
	Fp2 g_alpha [3];
	e.clear();
	e = precompute->getE();
#ifdef UNKNOWN_ATTRIBUTES
	const M_sint* WBPrec = precompute->getWB();
	const M_sint (*BBPrec)[4] = precompute->getBB();
#endif
	Fp2_map& attributes = precompute->getSetOfAttributes_W();
	MK.getG_alpha(g_alpha[0], g_alpha[1], g_alpha[2]);
	// @note generating random number
	t = rg.get();
	G2_mulknownSC(at_g2_b, t, precompute->getQsIga(), PT, precompute->t256, precompute->Half_Zr);
	//if(at_g2_b[2]!=1) mie::bn::ecop::Normalize(at_g2_b,at_g2_b);
	addJJJG2(UserKey.getK_W(), at_g2_b, g_alpha);
	G1_mulknownSC(UserKey.getL_W(), t, PsIPrec, PT, precompute->t256, precompute->Half_Zr);
	const std::vector<std::string>& s = bob.getAtr();
	for (size_t i = 0; i < s.size(); i++) {
		PointG2& puntoG2 = K_x[s[i]];
		Fp2* tmp2 = puntoG2.getG2_W();
#ifdef UNKNOWN_ATTRIBUTES
        Fp2 tmp3[3];
		hash_and_map2(tmp[0], tmp[1], s[i].c_str());
		tmp[2] = Fp(1);
		HashtoG2(tmp3, tmp, WBPrec, BBPrec);
		G2_mul(tmp2, tmp3, t, WBPrec, BBPrec);
		// if(tmp[2]!=1)mie::bn::ecop::Normalize(tmp,tmp);
		//mie::bn::ecop::Normalize(tmp2,tmp2);
#else
		PointG2& test_att = attributes[s[i]];
		G2_mulknownSC(tmp2, t, test_att.getQsI(), PT, precompute->t256, precompute->Half_Zr);
#endif
		puntoG2.setAtt(s[i]);
	}
}

/***
    Local Variables:
    c-basic-offset: 4
    indent-tabs-mode: t
    tab-width: 4
    End:
*/

