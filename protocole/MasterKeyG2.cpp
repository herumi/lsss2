/**
	@file
	@brief Master Secret Key (G2 version)
    @author Zavattoni et al.
	@license modified new BSD license
	http://www.opensource.org/licenses/BSD-3-Clause
*/

#include "MasterKeyG2.h"


void MasterKeyG2::getG_alpha(Fp2& x, Fp2& y, Fp2& z) const
{
	x = this->g_alpha[0];
	y = this->g_alpha[1];
	z = this->g_alpha[2];
}

void MasterKeyG2::SetGalpha(const Fp2 g_alpha[3])
{
	this->g_alpha[0] = g_alpha[0];
	this->g_alpha[1] = g_alpha[1];
	this->g_alpha[2] = g_alpha[2];
}

std::ostream& operator<<(std::ostream& os, const MasterKeyG2& mk)
{
	for (size_t i = 0; i < NUM_OF_ARRAY(mk.g_alpha); i++) {
		if (i > 0) os << ' ';

		os << mk.g_alpha[i];
	}

	return os;
}
std::istream& operator>>(std::istream& os, MasterKeyG2& mk)
{
	//std::string str;
	os >> mk.g_alpha[0];
	os >> mk.g_alpha[1];
	os >> mk.g_alpha[2];
	return os;
}
