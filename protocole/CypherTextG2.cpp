/**
	@file
	@brief CypherText structure and functions (G2 version)
    @author Zavattoni et al.
	@license modified new BSD license
	http://www.opensource.org/licenses/BSD-3-Clause
*/

#include "CypherTextG2.h"
#include "cpabe.h"

std::ostream& operator<<(std::ostream& os, const CypherTextG2& ct)
{
	const char space = ' ';
	os << ct.C << space;
	serialize(os, ct.C_Prime, NUM_OF_ARRAY(ct.C_Prime));
	Fp2_map::const_iterator itFp2 = ct.CAtt.begin();
	Fp_map::const_iterator itFp = ct.DAtt.begin();
	os << ct.DAtt.size() << space;

	while (itFp2 != ct.CAtt.end() && itFp != ct.DAtt.end()) {
		os << itFp2->first << space
		   << itFp->second << space
		   << itFp2->second << space;
		++itFp;
		++itFp2;
	}

	return os;
}

std::istream& operator>>(std::istream& is, CypherTextG2& ct)
{
	is >> ct.C;
	deserialize(is, ct.C_Prime, NUM_OF_ARRAY(ct.C_Prime));
	size_t size;
	is >> size;
	Fp2_map& mapFp2 = ct.getCAtt_W();
	Fp_map& mapFp = ct.getDAtt_W();

	for (size_t i = 0; i < size; i++) {
		std::string name;
		PointG1 pg1;
		PointG2 pg2;
		is >> name;
		is >> pg1;
		is >> pg2;
		mapFp[name]  = pg1;
		mapFp2[name] = pg2;
	}

	return is;
}

