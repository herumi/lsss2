/**
	@file
	@brief Test Montgomery Normalization
    @author Zavattoni et al.
	@license modified new BSD license
	http://www.opensource.org/licenses/BSD-3-Clause
*/
#include <stdio.h>
#include <memory.h>
#include "xbyak/xbyak_util.h"
extern Xbyak::util::Clock sclk;
#include "bn.h"
#include <iostream>
#include "util.h"
#include "opti.h"
#include "bn-multi.h"
#include "random.h"

using namespace bn;
using namespace ecop;

static int s_errNum = 0;
static int s_testNum = 0;

#define TEST_EQUAL(x, y) { s_testNum++; if (x != y) { s_errNum++; printf("%s(%d): err %s != %s\n", __FILE__, __LINE__, #x, #y); std::cout << "lhs=" << (x) << "\nrhs=" << (y) << std::endl; } }
#define TEST_ASSERT(x) { s_testNum++; if (!(x)) { s_errNum++; printf("%s(%d): err assert %s\n", __FILE__, __LINE__, #x); } }

#define FATAL_EXIT(msg) { printf("%s(%d): err %s\n", __FILE__, __LINE__, msg); exit(1); }

const M_uint& p = M_param_p();
const M_uint& r = M_param_r();

#define P_INIT Fp(2), Fp("16740896641879863340107777353588575149660814923656713498672603551465628253431")

#define P2_INIT Fp("13438486984812665827952643313785387127911349697151255879146972340858203091778"), Fp("12741389316352206200828479361093127917015298445269456291074734498988157668221")

const size_t N = 10000;
const size_t N2= 10000;
const size_t K = 1000;

Fp getX(const Fp&) {
	return Fp(2);
}
Fp getY(const Fp&) {
	return Fp("16740896641879863340107777353588575149660814923656713498672603551465628253431");
}
Fp getZ(const Fp&) {
	return Fp(1);
}
Fp genSkew(const Fp&) {
	return Fp("12345");
}
void scan(const Fp *Ps, const size_t n)
{
	for (size_t i = 0; i < n; i++) {
		TEST_ASSERT(bn::ecop::isOnECJac3(&Ps[i * 3]));
	}
}

Fp2 getX(const Fp2&) {
	return Fp2(
			   Fp("12723517038133731887338407189719511622662176727675373276651903807414909099441"),
			   Fp("4168783608814932154536427934509895782246573715297911553964171371032945126671")
	);
}
Fp2 getY(const Fp2&) {
	return Fp2(
			   Fp("13891744915211034074451795021214165905772212241412891944830863846330766296736"),
			   Fp("7937318970632701341203597196594272556916396164729705624521405069090520231616")
	);
}
Fp2 getZ(const Fp2&) {
	return Fp2(
			   Fp("1"),
			   Fp("0")
	);
}
Fp2 genSkew(const Fp2&) {
	return Fp2(Fp("12345"), Fp("67890"));
}
void scan(const Fp2 *Ps, const size_t n)
{
	for (size_t i = 0; i < n; i++) {
		TEST_ASSERT(bn::ecop::isOnTwistECJac3(&Ps[i * 3]));
	}
}

template<class FF>
void genPoints(FF *Ps, const size_t n) {
	if (n == 0) return;
	const FF P[] = { getX(Ps[0]), getY(Ps[0]), getZ(Ps[0]) };
	FF t = genSkew(Ps[0]);
	Ps[0] = P[0] * t * t;
	Ps[1] = P[1] * t * t * t;
	Ps[2] = P[2] * t;
	TEST_ASSERT(!Ps[2].isZero());
	if (n == 1) return;
	bn::ecop::ECDouble(&Ps[3], &Ps[0]);
	TEST_ASSERT(!Ps[3 + 2].isZero());
	for (size_t i = 2; i < n; i++) {
		bn::ecop::ECAdd(&Ps[i * 3], &Ps[(i - 1) * 3], P);
		TEST_ASSERT(!Ps[i * 3 + 2].isZero());
	}
}

void testUnifiedMultiNormalization()
{
	const size_t maxn = 20;
	for (size_t G2n = 1; G2n <= maxn; G2n++) {
		// @note G1n starts from 2, because MultiNormalizeFp will be failed if not.
		for (size_t G1n = 2; G1n <= maxn; G1n++) {

			printf("# of points of G1 and G2: %lu, %lu\n", G1n, G2n);

			const size_t G1Size = G1n;
			const size_t G2Size = G2n;
			bn::Darray<Fp, 3> Ps(G1Size);
			bn::Darray<Fp2, 3> Qs(G2Size);

			genPoints(&Ps[0][0], G1Size);
			scan(&Ps[0][0], G1Size);

			genPoints(&Qs[0][0], G2Size);
			scan(&Qs[0][0], G2Size);

			bn::Darray<Fp, 2> NPsBuff(G1Size);
			bn::Darray<Fp2, 2> NQsBuff(G2Size);

			double mnFp;
			double mnFp2;
			double sum;
			double mnFpAndFp2;

			{
				bn::Darray<Fp, 2> NPs(G1Size);

				{
					Xbyak::util::Clock clkClock;
					clkClock.begin();
					for (size_t i = 0; i < N; i++) {
						MultiNormalizeFp(NPs, Ps.get(), G1Size);
					}
					clkClock.end();
					mnFp = (clkClock.getClock() / K) / double(N);
					printf("MultiNormalizeFp(%lu) \t% 10.3fKclk\n", G1Size, mnFp);
					sum = mnFp;
				}

				for (size_t i = 0; i < G1Size; i++) {
					Fp T[3];
					T[0] = NPs[i][0];
					T[1] = NPs[i][1];
					T[2] = 1;
					TEST_ASSERT(bn::ecop::isOnECJac3(T));
					Fp T2[3];
					bn::ecop::NormalizeJac(T2, Ps[i]);
					TEST_EQUAL(T2[0], T[0]);
					TEST_EQUAL(T2[1], T[1]);
					TEST_EQUAL(T2[2], T[2]);

					NPsBuff[i][0] = NPs[i][0];
					NPsBuff[i][1] = NPs[i][1];
				}
			}

			{
				bn::Darray<Fp2, 2> NQs(G2Size);

				{
					Xbyak::util::Clock clkClock;
					clkClock.begin();
					for (size_t i = 0; i < N; i++) {
						MultiNormalizeFp2(NQs, Qs.get(), G2Size);
					}
					clkClock.end();
					mnFp2 = (clkClock.getClock() / K) / double(N);
					printf("MultiNormalizeFp2(%lu) \t% 10.3fKclk\n", G2Size, mnFp2);
					sum += mnFp2;
				}

				for (size_t i = 0; i < G2Size; i++) {
					Fp2 T[3];
					T[0] = NQs[i][0];
					T[1] = NQs[i][1];
					T[2] = 1;
					TEST_ASSERT(bn::ecop::isOnTwistECJac3(T));
					Fp2 T2[3];
					bn::ecop::NormalizeJac(T2, Qs[i]);
					TEST_EQUAL(T2[0], T[0]);
					TEST_EQUAL(T2[1], T[1]);
					TEST_EQUAL(T2[2], T[2]);

					NQsBuff[i][0] = NQs[i][0];
					NQsBuff[i][1] = NQs[i][1];
				}
			}

			{
				bn::Darray<Fp, 2> NPs(G1Size);
				bn::Darray<Fp2, 2> NQs(G2Size);

				{
					Xbyak::util::Clock clkClock;
					clkClock.begin();
					for (size_t i = 0; i < N; i++) {
						MultiNormalizeJacobianFpAndFp2(&NPs[0][0], &NQs[0][0], &Ps[0][0], G1Size, &Qs[0][0], G2Size);
					}
					clkClock.end();
					mnFpAndFp2 = (clkClock.getClock() / K) / double(N);
					printf("Fp(%lu)+Fp2(%lu) \t% 10.3fKclk\n", G1Size, G2Size, sum);
					printf("FpAndFp2(%lu,%lu) \t% 10.3fKclk\n", G1Size, G2Size, mnFpAndFp2);
					printf("diff(%lu,%lu) \t% 10.3fKclk\n", G1Size, G2Size, sum - mnFpAndFp2);
				}

				for (size_t i = 0; i < G1Size; i++) {
					Fp T[3];
					T[0] = NPs[i][0];
					T[1] = NPs[i][1];
					T[2] = 1;
					TEST_ASSERT(bn::ecop::isOnECJac3(T));
					Fp T2[3];
					bn::ecop::NormalizeJac(T2, Ps[i]);
					TEST_EQUAL(T2[0], T[0]);
					TEST_EQUAL(T2[1], T[1]);
					TEST_EQUAL(T2[2], T[2]);

					TEST_EQUAL(NPsBuff[i][0], NPs[i][0]);
					TEST_EQUAL(NPsBuff[i][1], NPs[i][1]);
				}

				for (size_t i = 0; i < G2Size; i++) {
					Fp2 T[3];
					T[0] = NQs[i][0];
					T[1] = NQs[i][1];
					T[2] = 1;
					TEST_ASSERT(bn::ecop::isOnTwistECJac3(T));
					Fp2 T2[3];
					bn::ecop::NormalizeJac(T2, Qs[i]);
					TEST_EQUAL(T2[0], T[0]);
					TEST_EQUAL(T2[1], T[1]);
					TEST_EQUAL(T2[2], T[2]);

					TEST_EQUAL(NQsBuff[i][0], NQs[i][0]);
					TEST_EQUAL(NQsBuff[i][1], NQs[i][1]);
				}
			}
			std::cout.flush();
		}
	}
}

int main(int argc, char *argv[]) try
{
	argc--, argv++;
	int mode = -1;
	if (argc > 1 && strcmp(*argv, "-m") == 0) {
		mode = atoi(argv[1]);
	}
	Param::init(mode);

	testUnifiedMultiNormalization();

	printf("num of test = %d (err = %d)\n", s_testNum, s_errNum);
    if (s_errNum == 0) {
        return 0;
    } else {
        return 1;
    }

} catch (std::exception& e) {
	fprintf(stderr, "ExpCode ERR:%s\n", e.what());
	return 1;
}

/***
	Local Variables:
	c-basic-offset: 4
	indent-tabs-mode: t
	tab-width: 4
	End:
*/
