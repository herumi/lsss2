/**
	@file
	@brief Point in G2 structure
    @author Zavattoni et al.
	@license modified new BSD license
	http://www.opensource.org/licenses/BSD-3-Clause
*/

#include "PointG2.h"

PointG2::PointG2()
{
    for (size_t i = 0; i < NUM_OF_ARRAY(pt); i++) {
        pt[i].clear();
    }

    Fp2* p = &QsI[0][0];

    for (size_t i = 0; i < NUM_OF_DBL_ARRAY(QsI); i++) {
        p[i].clear();
    }
}

std::ostream& operator<<(std::ostream& os, const PointG2& pg2)
{
    serialize(os, pg2.pt, NUM_OF_ARRAY(pg2.pt));
    serialize(os, &pg2.QsI[0][0], NUM_OF_DBL_ARRAY(pg2.QsI));
    return os;
}
std::istream& operator>>(std::istream& is, PointG2& pg2)
{
	deserialize(is, pg2.pt, NUM_OF_ARRAY(pg2.pt));
	deserialize(is, &pg2.QsI[0][0], NUM_OF_DBL_ARRAY(pg2.QsI));
	return is;
}

