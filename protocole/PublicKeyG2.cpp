/**
	@file
	@brief Public Key functions (G2 version)
    @author Zavattoni et al.
	@license modified new BSD license
	http://www.opensource.org/licenses/BSD-3-Clause
*/

#include "PublicKeyG2.h"

std::ostream& operator<<(std::ostream& os, const PublicKeyG2& pk)
{
	serialize(os, pk.g1, NUM_OF_ARRAY(pk.g1));
	serialize(os, pk.g2, NUM_OF_ARRAY(pk.g2));
	os << pk.e_alpha << ' ';
	serialize(os, pk.g_a, NUM_OF_ARRAY(pk.g_a));
	return os;
}

std::istream& operator>>(std::istream& is, PublicKeyG2& pk)
{
	deserialize(is, pk.g1, NUM_OF_ARRAY(pk.g1));
	deserialize(is, pk.g2, NUM_OF_ARRAY(pk.g2));
	is >> pk.e_alpha;
	deserialize(is, pk.g_a, NUM_OF_ARRAY(pk.g_a));
	return is;
}

