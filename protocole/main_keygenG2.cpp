/**
	@file
	@brief Launch Key Generation (G2 version)
    @author Zavattoni et al.
	@license modified new BSD license
	http://www.opensource.org/licenses/BSD-3-Clause
*/
#include <fstream>

#include "Alice.h"
#include "ServerG2.h"
#include "Bob.h"
#include "bench.h"

#define BENCH_TIMES	50

const char* usage =
    "Usage: ./LSSS-keygenG2 [OPTION ...] ATTR [ATTR ...]\n"
    "\n"
    "Generate a key with the listed attributes using public key PublicKey and\n"
    "master secret key master_key. Output will be written to the file\n"
    "\"UserKey\".\n"
    "\n"
    " -h, --help               print this message\n\n";

void parse_args(int argc, char** argv)
{
	for (int i = 1; i < argc; i++ ) {
		if (      !strcmp(argv[i], "-h") || !strcmp(argv[i], "--help") ) {
			printf("%s", usage);
			exit(0);
		}
	}
}

int main(int argc, char** argv)
{
	parse_args(argc, argv);
	/* initializes the BN curve */
	bn::Param::init(0);
	/*
	  System initialization, setup external libraries,
	  is done until here.
	*/
	PublicKeyG2 PK;
	PK.load("public_key");
	ServerG2 server;
	server.load("master_key");
	/*User Bob is introduced with his attributes in argv the server creates his UserKey*/
	Bob bob;
	bob.set(argc, argv);
	PrecomputeG2 prec;
	prec.load("Precomputation");
//*
	BENCH_BEGIN("step keygen", BENCH_TIMES) {
		BENCH_ADD( KeyG2 UserKey; server.KeyGen(UserKey, bob, &PK, &prec);
		           , BENCH_TIMES);
	}
	BENCH_END(BENCH_TIMES);
// */
	KeyG2 UserKey;
	server.KeyGen(UserKey, bob, &PK, &prec);
	UserKey.PrecomputeForPairing(bob.getAtr());
	UserKey.save("UserKey");
}


/***
    Local Variables:
    c-basic-offset: 4
    indent-tabs-mode: t
    tab-width: 4
    End:
*/

