/**
	@file
	@brief Setup and KeyGen steps
    @author Zavattoni et al.
	@license modified new BSD license
	http://www.opensource.org/licenses/BSD-3-Clause
*/

#include "Server.h"
#include <string>
#include "bench.h"
#include "cpabe.h"
#include "Precompute.h"
#include "opti.h"
#include "bn-multi.h"
#include "random.h"

extern uint64_t PT[8][256];

using namespace mie;

void Server::Setup(PublicKey& PK, Precompute& precompute)
{
	RandInt rg;
	const Fp12 (*fe) = precompute.getFePtr();
	Fp12 fe_alpha[PRECOMPUTEknownS];
	Fp alpha_g1[3];
	Fp g_a [3];
	Fp12 e_alpha;
	const M_sint* WPrec = precompute.getW();
	const M_sint (*SBPrec)[2] = precompute.getSB();
	// @note generating random number
	// @note removed const quantifier
	M_uint alpha = rg.get();

	GT_expoknownSC(e_alpha, alpha, fe, PT, precompute.t256, precompute.Half_Zr);
	precomputeGTknownSC(fe_alpha, e_alpha, precompute.getWB(), precompute.getBB());
	// @note calling not const qualified member function, why?
	precompute.setFeAlpha(fe_alpha);
	// @note generating random number //this is called delta in the paper
	// @note removed const quantifier
	M_uint a = rg.get();
	G1_mulknownSC(g_a, a, precompute.getPsI(), PT, precompute.t256, precompute.Half_Zr);

	if (g_a[2] != 1) bn::ecop::NormalizeJac(g_a, g_a);

	Fp g_a_pack[2];
	g_a_pack[0] = g_a[0];
	g_a_pack[1] = g_a[1];
	// @note calling not const qualified member function, why?
	precomputeG1knownSC(precompute.getPsIga_W(), g_a_pack, WPrec, SBPrec, precompute.getBetaS());
	// @note a variable PK appears at first time
	PK.Set_Element(cpabe::Param::getG1Gen(), cpabe::Param::getG2Gen(), e_alpha, g_a);
	G1_mulknownSC(alpha_g1, alpha, precompute.getPsI(), PT, precompute.t256, precompute.Half_Zr);
	if (alpha_g1[2] != 1) bn::ecop::NormalizeJac(alpha_g1, alpha_g1);

	this->MK.SetGalpha(alpha_g1);
}

void Server::KeyGen(Key& UserKey, const Bob& bob, PublicKey* /*PK*/, Precompute* precompute)
{
	Fp_map& K_x = UserKey.getK_Att_W();
	RandInt rg;
	const Fp2 (*QsIPrec)[2] = precompute->getQsIPtr();
	Fp12 e;
	M_uint t;
	//Fp at_g1 [3];
	Fp at_g1_b [3];
	Fp g_alpha [3];
	//Fp g_a [3];
	e.clear();
	e = precompute->getE();

#ifdef UNKNOWN_ATTRIBUTES
	const M_sint* WPrec = precompute->getW();
	const M_sint (*SBPrec)[2] = precompute->getSB();
	const M_sint& BetaSPrec = precompute->getBetaS();
#endif
	Fp_map& attributes = precompute->getSetOfAttributes_W();
	MK.getG_alpha(g_alpha[0], g_alpha[1], g_alpha[2]);
	// @note generating random number
	t = rg.get();
	G1_mulknownSC(at_g1_b, t, precompute->getPsIga(), PT, precompute->t256, precompute->Half_Zr);

	addJJJG1(UserKey.getK_W(), g_alpha, at_g1_b);
	G2_mulknownSC(UserKey.getL_W(), t, QsIPrec, PT, precompute->t256, precompute->Half_Zr);
	const std::vector<std::string>& s = bob.getAtr();

	for (size_t i = 0; i < s.size(); i++) {
		PointG1& puntoG1 = K_x[s[i]];
		Fp* tmp2 = puntoG1.getG1_W();
#ifdef UNKNOWN_ATTRIBUTES
		hash_and_mapB(tmp[0], tmp[1], s[i].c_str());
		tmp[2] = Fp(1);
		G1_mul(tmp2, t, tmp, WPrec, SBPrec, BetaSPrec);
#else
		PointG1& test_att = attributes[s[i]];
		G1_mulknownSC(tmp2, t, test_att.getPsI(), PT, precompute->t256, precompute->Half_Zr);
#endif
		puntoG1.setAtt(s[i]);
	}
}

/***
    Local Variables:
    c-basic-offset: 4
    indent-tabs-mode: t
    tab-width: 4
    End:
*/

