/**
	@file
	@brief Public and Private Key structures
    @author Zavattoni et al.
	@license modified new BSD license
	http://www.opensource.org/licenses/BSD-3-Clause
*/

#include "Key.h"
#include "Attribute.h"
#include "Alice.h"

std::ostream& operator<<(std::ostream& os, const Key& key)
{
	const char space = ' ';
	serialize(os, key.K, NUM_OF_ARRAY(key.K));
	serialize(os, key.L, NUM_OF_ARRAY(key.L));
	os << key.K_Att.size() << space;

	for (Fp_map::const_iterator i = key.K_Att.begin(), ie = key.K_Att.end(); i != ie; ++i) {
		os << i->first << space << i->second << space;
	}

	serialize(os, &key.l[0][0], NUM_OF_DBL_ARRAY(key.l));

	return os;
}

std::istream& operator>>(std::istream& is, Key& key)
{
	deserialize(is, key.K, NUM_OF_ARRAY(key.K));
	deserialize(is, key.L, NUM_OF_ARRAY(key.L));
	size_t size;
	is >> size;
	Fp_map& mapFp = key.K_Att;
	mapFp.clear();

	for (size_t i = 0; i < size; i++) {
		std::string name;
		PointG1 pg1;
		is >> name;
		is >> pg1;
		mapFp[name] = pg1;
	}

	deserialize(is, &key.l[0][0], NUM_OF_DBL_ARRAY(key.l));

	return is;
}

