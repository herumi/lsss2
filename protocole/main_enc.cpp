/**
	@file
	@brief Launch Encryption
    @author Zavattoni et al.
	@license modified new BSD license
	http://www.opensource.org/licenses/BSD-3-Clause
*/
#include <fstream>

#include "Alice.h"
#include "Server.h"
#include "Bob.h"
#include "bench.h"
#include "MatrizLSSS.h"
#include "PublicKey.h"

#define BENCH_TIMES	50

const char* usage =
    "Usage: ./LSSS-enc [OPTION ...] POLICY\n"
    "\n"
    "Encrypt FILE under the decryption policy POLICY using public key\n"
    "public_key.\n\n"
    "Mandatory arguments to long options are mandatory for short options too.\n\n"
    " -h, --help               print this message\n\n"
    "\n"
    "";

void parse_args(int argc, char** argv)
{
	for (int  i = 1; i < argc; i++ ) {
		if ( !strcmp(argv[i], "-h") || !strcmp(argv[i], "--help") ) {
			printf("%s", usage);
			exit(0);
		}
	}
}

void encrypt(const char* msg)
{
	PublicKey PK;
	PK.load("public_key");
	MatrizLSSS mat(msg);
	std::cout << "---------------" << std::endl;
	std::cout << mat.toStringMatrizType();
	std::cout << "---------------" << std::endl;
	mat.save("matriz");
	Precompute prec;
	prec.load("Precomputation");
	Fp msj("112233445566778899");
	BENCH_BEGIN("step encryption", BENCH_TIMES) {
		BENCH_ADD( CypherText CT;
		           Alice::enc(&CT, PK, msj, mat.matrix, prec),
		           BENCH_TIMES);
	}
	BENCH_END(BENCH_TIMES);
	CypherText CT;
	Alice::enc(&CT, PK, msj, mat.matrix, prec);
	CT.save("CypherText");
}

int main(int argc, char** argv)
{
	parse_args(argc, argv);
	/* initializes the BN curve */
	bn::Param::init(0);
	cpabe::Param::init();
	/*
	  System initialization, setup external libraries,
	  is done until here.
	*/
	encrypt(argv[1]);
}

/***
    Local Variables:
    c-basic-offset: 4
    indent-tabs-mode: t
    tab-width: 4
    End:
*/

