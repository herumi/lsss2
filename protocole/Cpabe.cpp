/**
	@file
	@brief Input/Output helpers 
    @author Zavattoni et al.
	@license modified new BSD license
	http://www.opensource.org/licenses/BSD-3-Clause
*/
#include "cpabe.h"
#include "Tree.h"
#include "Key.h"
#include "CypherText.h"
#include "PublicKey.h"
#include <sstream>
#include "zm.h"

#include "bench.h"

#define BENCH_TIMES	500
///////////////////////
// Function
///////////////////////

M_uint inverse2(const M_uint& a, const M_uint& mod)
{
	M_sint a_signed;
	M_sint test3 = 0;
	M_sint inverse;
//	a_signed.set(a);
	M_set(a_signed, a);
	extended_gcd(a_signed, mod, inverse, test3);
	inverse = inverse % mod;
	return M_abs(inverse);
}

void extended_gcd(const M_sint& a, const M_uint& b2, M_sint& c, M_sint& d)
{
	M_sint b; M_set(b, b2);
	M_sint q, r, s, t;

	if (b == 0) {
		c = 1; d = 0;

	} else {
		M_divmod(q, r, a, b);
		extended_gcd(b, M_abs(r), s, t);
		c = t; d = s - q * t;
	}
}


bool isInSet(const std::string s, const std::vector <std::string> Set)
{
	for (unsigned int i = 0; i < Set.size(); i++) {
		if (!strcmp(s.c_str(), Set[i].c_str())) {
			return true;
		}
	}

	return false;
}

bool DecryptNode(CypherText* /*CT*/, Key* /*SK*/, Tree* /*x*/, Fp12& /*F*/)
{
	return true;
}

/***
    Local Variables:
    c-basic-offset: 4
    indent-tabs-mode: t
    tab-width: 4
    End:
*/
