/**
	@file
	@brief Precomputing steps
    @author Zavattoni et al.
	@license modified new BSD license
	http://www.opensource.org/licenses/BSD-3-Clause
*/

#include "Precompute.h"

void cpabe::Param::setG1Gen3(Fp* g1)
{
	g1[0] = Fp("1674578968009266105367653690721407808692458796109485353026408377634195183292");
	g1[1] = Fp("8299158460239932124995104248858950945965255982743525836869552923398581964065");
	g1[2] = Fp("1");
	assert(bn::ecop::isOnECJac3(g1));
#ifndef NDEBUG
	{
		Fp t[3];
		bn::ecop::ScalarMult(t, g1, M_param_r());
		assert(t[2].isZero());
	}
#endif
}

void cpabe::Param::setG1Gen2(Fp* g1)
{
	Fp t[3];
	setG1Gen3(t);
	g1[0] = t[0];
	g1[1] = t[1];
}

void cpabe::Param::setG2Gen3(Fp2* g2)
{
	g2[0] = Fp2(Fp("12723517038133731887338407189719511622662176727675373276651903807414909099441"),
	            Fp("4168783608814932154536427934509895782246573715297911553964171371032945126671"));
	g2[1] = Fp2(Fp("13891744915211034074451795021214165905772212241412891944830863846330766296736"),
	            Fp("7937318970632701341203597196594272556916396164729705624521405069090520231616"));
	g2[2] = Fp2(Fp("1"), Fp("0"));
	assert(bn::ecop::isOnTwistECJac3(g2));
#ifndef NDEBUG
	{
		Fp2 t[3];
		bn::ecop::ScalarMult(t, g2, M_param_r());
		assert(t[2].isZero());
	}
#endif
}

void cpabe::Param::setG2Gen2(Fp2* g2)
{
	Fp2 t[3];
	setG2Gen3(t);
	g2[0] = t[0];
	g2[1] = t[1];
}

Fp cpabe::Param::g1_[3];
Fp2 cpabe::Param::g2_[3];

void cpabe::Param::init()
{
	static bool isInitedGuard = false;

	if (isInitedGuard) return;

	setG1Gen3(g1_);
	setG2Gen3(g2_);
	isInitedGuard = true;
}

void Precompute::setElement()
{
	const Fp2* g2 = cpabe::Param::getG2Gen();
	this->g2_pack[0] = g2[0];
	this->g2_pack[1] = g2[1];
	/* generator g1 of G1 */
	const Fp* g1_ = cpabe::Param::getG1Gen();
	Fp g1[2];
	g1[0] = g1_[0]; // @note copy
	g1[1] = g1_[1]; // @note copy
	bn::opt_atePairing<Fp> (this->e, g2, g1_);
	const M_sint& z = M_param_z();

	this->BetaS_ = -(18 * z * z * z + 18 * z * z + 9 * z + 2);
	this->WB[0] = -(6 * z * z + 6 * z + 2);
	this->WB[1] = -1;
	this->WB[2] = z;
	this->WB[3] = 1 + 3 * z + 6 * z * z + 6 * z * z * z;
	this->BB[0][0] = 2 * z;
	this->BB[0][1] = 1 + z;
	this->BB[0][2] = -z;
	this->BB[0][3] = z;
	this->BB[1][0] = 1 + z;
	this->BB[1][1] = -1 - 3 * z;
	this->BB[1][2] = -1 + z;
	this->BB[1][3] = 1;
	this->BB[2][0] = -1;
	this->BB[2][1] = 2 + 6 * z;
	this->BB[2][2] = 2;
	this->BB[2][3] = -1;
	this->BB[3][0] = 2 + 6 * z;
	this->BB[3][1] = 1;
	this->BB[3][2] = -1;
	this->BB[3][3] = 1;
	this->SB[0][0] = 6 * z * z + 2 * z;
	this->SB[0][1] = -(2 * z + 1);
	this->SB[1][0] = -(2 * z + 1);
	this->SB[1][1] = -(6 * z * z + 4 * z + 1);
	this->W[0] = 6 * z * z + 4 * z + 1;	// This is first column of inverse of SB (without division by determinant)
	this->W[1] = -(2 * z + 1);

	int i;
	M_uint t0;
	for (i=0; i < 4; i++)
	{
		this->WB[i] <<= 256;
		this->WB[i] /= M_param_r();
	}
	for (i=0; i < 2; i++)
	{
		this->W[i] <<= 256;
		this->W[i] /= M_param_r();
	}

	precomputeG1knownSC(this->PsI, g1, W, SB, BetaS_);
	precomputeG2knownSC(this->QsI, this->g2_pack, WB, BB);
	precomputeGTknownSC(this->fe, this->e, WB, BB);
    M_setStr(t256, "0x10000000000000000000000000000000000000000000000000000000000000000");
    M_setStr(Half_Zr, "0x1291B24120000000DD1A26C000000003FFCFC000000000085080000000000007");
}

std::ostream& operator<<(std::ostream& os, const Precompute& prec)
{
	const char space = ' ';
	//serialize(os, prec.g2_pack, NUM_OF_ARRAY(prec.g2_pack));
	serialize(os, &prec.QsI[0][0], NUM_OF_DBL_ARRAY(prec.QsI));
	os << prec.e << space;
	serialize(os, prec.fe, NUM_OF_ARRAY(prec.fe));
	serialize(os, prec.fe_alpha, NUM_OF_ARRAY(prec.fe_alpha));
	os << prec.BetaS_ << space;
	serialize(os, prec.WB, NUM_OF_ARRAY(prec.WB));
	serialize(os, &prec.BB[0][0], NUM_OF_DBL_ARRAY(prec.BB));
	serialize(os, &prec.SB[0][0], NUM_OF_DBL_ARRAY(prec.SB));
	serialize(os, prec.W, NUM_OF_ARRAY(prec.W));
	serialize(os, &prec.PsI[0][0], NUM_OF_DBL_ARRAY(prec.PsI));
	serialize(os, &prec.PsIga[0][0], NUM_OF_DBL_ARRAY(prec.PsIga));
	os << prec.SetOfAttributes.size() << space;

	for (Fp_map::const_iterator i = prec.SetOfAttributes.begin(), ie = prec.SetOfAttributes.end(); i != ie; ++i) {
		os << i->first << space << i->second << space;
	}

	return os;
}

std::istream& operator>>(std::istream& is, Precompute& prec)
{
	//std::string str;
	//deserialize(is, prec.g2_pack, NUM_OF_ARRAY(prec.g2_pack));
	deserialize(is, &prec.QsI[0][0], NUM_OF_DBL_ARRAY(prec.QsI));
	is >> prec.e;
	deserialize(is, prec.fe, NUM_OF_ARRAY(prec.fe));
	deserialize(is, prec.fe_alpha, NUM_OF_ARRAY(prec.fe_alpha));
	is >> prec.BetaS_;
	deserialize(is, prec.WB, NUM_OF_ARRAY(prec.WB));
	deserialize(is, &prec.BB[0][0], NUM_OF_DBL_ARRAY(prec.BB));
	deserialize(is, &prec.SB[0][0], NUM_OF_DBL_ARRAY(prec.SB));
	deserialize(is, prec.W, NUM_OF_ARRAY(prec.W));
	deserialize(is, &prec.PsI[0][0], NUM_OF_DBL_ARRAY(prec.PsI));
	deserialize(is, &prec.PsIga[0][0], NUM_OF_DBL_ARRAY(prec.PsIga));
	size_t size;
	is >> size;
	Fp_map& mapFp = prec.getSetOfAttributes_W();

	for (size_t i = 0; i < size; i++) {
		std::string name;
		PointG1 pg1;
		is >> name;
		is >> pg1;
		mapFp[name] = pg1;
	}

	return is;
}

/***
    Local Variables:
    c-basic-offset: 4
    indent-tabs-mode: t
    tab-width: 4
    End:
*/

