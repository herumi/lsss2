/**
	@file
	@brief Master Secret Key structure
    @author Zavattoni et al.
	@license modified new BSD license
	http://www.opensource.org/licenses/BSD-3-Clause
*/

#include "MasterKey.h"


void MasterKey::getG_alpha(Fp& x, Fp& y, Fp& z) const
{
	x = this->g_alpha[0];
	y = this->g_alpha[1];
	z = this->g_alpha[2];
}

void MasterKey::SetGalpha(const Fp g_alpha[3])
{
	this->g_alpha[0] = g_alpha[0];
	this->g_alpha[1] = g_alpha[1];
	this->g_alpha[2] = g_alpha[2];
}

std::ostream& operator<<(std::ostream& os, const MasterKey& mk)
{
	for (size_t i = 0; i < NUM_OF_ARRAY(mk.g_alpha); i++) {
		if (i > 0) os << ' ';

		os << mk.g_alpha[i];
	}

	return os;
}
std::istream& operator>>(std::istream& os, MasterKey& mk)
{
	std::string str;
	os >> str;
	mk.g_alpha[0].set(str);
	os >> str;
	mk.g_alpha[1].set(str);
	os >> str;
	mk.g_alpha[2].set(str);
	return os;
}
