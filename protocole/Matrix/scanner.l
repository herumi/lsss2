%{
#include "parser.tab.hpp"
#include <iostream>
#include <string.h>
#include "global.h"
using namespace std;
%}

%option noyywrap

%%
","                    { return COMA; }
"("                    { return LBRACE; }
")"                    { return RBRACE; }
[a-zA-Z.]+[0-9]*       { yylval.string=strdup(yytext); 
						 return PALABRA; }
[0-9]+                 { yylval.number=atoi(yytext); return NUMERO; }
<<EOF>>  { return 0; }
[ \t\n]+ { }
.        { cerr << "Unrecognized token!" << endl; exit(1); }
%%
