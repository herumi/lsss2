/**
	@file
	@brief LSSS matrix structure and functions
    @author Zavattoni et al.
	@license modified new BSD license
	http://www.opensource.org/licenses/bsd-license.php
*/

#include "MatrizLSSS.h"
#include <cstdlib>
#include <cstring>
#include <vector>
#include "cpabe.h"

#ifdef MIE_ATE_USE_GMP
M_uint gcd(const M_uint& x, const M_uint& y)
{
	M_uint z;
	mpz_gcd(z.get_mpz_t(), x.get_mpz_t(), y.get_mpz_t());
	return z;
}
#else
template<class T>
T gcd(T x, T y)
{
	if (x < y) {
		std::swap(x, y);
	}

	// x >= y
	if (y == 0) return x;
	if (y == 1) return 1;

	return gcd(x % y, y);
}
#endif

void MatrizLSSS::iniciaMatriz()
{
	matrix.Mat = matrix_allocate(rowT, colT);
	Matt = matrix.Mat;
	Matt->ptr[0][0] = 1;
	matrix.rho = rho_allocate(rowT);
}

int MatrizLSSS::conteo(nodeType* p, int ind)
{
	if (!p) return ind;

	switch (p->type) {
	case typeId:
		return ind + 1;
	case typeCon:
		return ind + 1;
	case typeOpr:
		switch (p->opr.oper) {
		case ';':
			return ind + 1;
		case ',':
			ind = conteo(p->opr.op[0], ind);
			ind = conteo(p->opr.op[1], ind);
		}
	}

	return ind;
}

int MatrizLSSS::llena(int t2, int m2)
{
	int i, j, k;
	int am = m - 1 + m2;
	int ad = d - 1 + t2;
	MATRIX* MM = NULL;
	MM = matrix_allocate(rowT, colT);

	//guarda las filas posteriores
	for (j = ind + 1, k = 0; j < m; j++, k++) {
		for (i = 0; i < d; i++) {
			MM->ptr[k][i] = Matt->ptr[j][i];
		}
	}

	//Rellena con ceros las filas anteriores
	for (j = 0; j < ind; j++) {
		for (i = d; i < ad; i++) {
			Matt->ptr[j][i] = 0;
		}
	}

	//Realiza la insercion
	for (j = 0; j < m2; j++) {
		for (i = 0; i < d; i++) {
			Matt->ptr[ind + j][i] = Matt->ptr[ind][i];
		}

		for (i = 1; i < t2; i++) {
			Matt->ptr[ind + j][d + i - 1] = MSP[i][j];
		}
	}

	//Coloca Filas posteriores
	for (j = ind + m2, k = 0; j < am; j++, k++) {
		for (i = 0; i < d; i++) {
			Matt->ptr[j][i] = MM->ptr[k][i];
		}

		for (i = d; i < ad; i++) {
			Matt->ptr[j][i] = 0;
		}
	}

	m = am;
	d = ad;
	matrix_free(MM);
	Matt->rows = am;
	Matt->cols = ad;
	return 0;
}

int MatrizLSSS::ex(nodeType* p)
{
	int m2, t2;

	if (!p) {
		return 0;
	}

	switch (p->type) {
	case typeCon:
		m2 = p->con.value;
		return m2;

	case typeId:
		matrix.rho[ind].ind = ind;
		matrix.rho[ind].atributo = new char[strlen(p->id.str) + 1];
		strcpy(matrix.rho[ind].atributo, p->id.str);
		ind++;
		return 0;

	case typeOpr:
		switch (p->opr.oper) {
		case ';':
			m2 = conteo(p->opr.op[0], 0);
			t2 = ex(p->opr.op[1]);
			llena(t2, m2);
			ex(p->opr.op[0]);
			return 0;

		case ',':
			ex(p->opr.op[0]);
			ex(p->opr.op[1]);
		}
	}

	return 0;
}


MAP* MatrizLSSS::rho_allocate(int row)
{
	MAP* r = new MAP[row];

	for (int i = 0; i < row; i++ ) {
		r[i].atributo = NULL ;
	}

	return r;
}

MATRIX* MatrizLSSS::matrix_allocate(int row, int col)
{
	int i = 0;
	M_sint** paux = NULL;
	MATRIX* A = new MATRIX;
	/// Set size as requiered
	A->rows = row;
	A->cols = col;
	paux = new M_sint* [row];

	for ( i = 0; i < row; i++ ) {
		paux[i] = new M_sint [col];
	}

	A->ptr =  paux;
	return( A );
}

void MatrizLSSS::matrix_free(MATRIX* A)
{
	M_sint** a = (M_sint**)A->ptr;

	for (int i = 0; i < A->rows; i++) delete [] a[i] ;

	delete[] a;
	delete A;
}

bool MatrizLSSS::getCoeffients(MATRIX* A, COEFF* coef, M_sint* delta)
{
	int i = 0, j = 0, ind = 0;
	M_sint suma, aux, val, det, a, b;
	M_uint val2;

	//Mando los coeficientes a cero
	for (j = 0; j < A->rows; j++) {
		coef[j].omega = 0;
	}

	MATRIX* B = matrix_allocate(A->cols, A->rows + 1);

	for (j = 0; j < A->rows; j++) {
		for (i = 0; i < A->cols; i++) {
			B->ptr[i][j] = A->ptr[j][i];
		}
	}

	B->ptr[0][A->rows] = 1;
	trianguliza(B);
	//Transforma matriz cuadrada
	std::vector<int> map(A->rows);
	std::vector<M_sint> Bc(B->rows * B->rows);
	const Pos p(B->rows);
	ind = 0;
	det = 1;

	for (j = 0; j < B->cols - 1 && ind < B->rows; j++) {
		if (B->ptr[ind][j] != 0) {
			det *= B->ptr[ind][j];

			for (i = 0; i < B->rows; i++) {
				Bc[p.get(i, ind)] = B->ptr[i][j];
			}

			map[ind] = j;
			ind++;
		}
	}

	const M_uint& abs_det = M_abs(det);
	if (abs_det > 1) {
		for (j = 0; j < ind; j++) {
			B->ptr[j][B->cols - 1] *= abs_det;
		}
	}

	//Resuelve de abajo hacia arriba
	for (j = ind - 1; j >= 0; j--) {
		suma = 0;

		for (i = ind - 1; i >= 0; i--) {
			suma += Bc[p.get(j, i)] * coef[map[i]].omega;
		}

		const M_sint& aux = Bc[p.get(j, j)];
		coef[map[j]].omega = (B->ptr[j][B->cols - 1] - suma) / aux;
	}

	M_uint vgcd = M_abs(det);

	for (i = 0; i < A->rows; i++) {
		if (coef[i].omega != 0) {
			M_uint omega = M_abs(coef[i].omega);
			vgcd = gcd(vgcd, omega);
		}
	}

	if (vgcd > 1) {
		for (i = 0; i < A->rows; i++) {
			if (coef[i].omega != 0) {
				coef[i].omega /= vgcd;
			}
		}
	}

	*delta = M_abs(det) / vgcd;
	matrix_free(B);

	//VERIFICACION
	for (j = A->cols - 1; j >= 0; j--) {
		suma = 0;

		for (i = A->rows - 1; i >= 0; i--) {
			suma += A->ptr[i][j] * coef[i].omega;
		}

		if (j > 0 && suma != 0) {
			return false;
		}

		if (j == 0 && suma != (*delta)) return false;
	}

	//printf("Pasa\n");
	return true;
}

std::ostream& operator<<(std::ostream& os, const MatrizLSSS& M)
{
	MatrizType mlsss = M.matriz;
	MATRIX* mat = mlsss.Mat;
	MAP* rho = mlsss.rho;
	std::string space(" ");
	int i, j;
	os << mat->rows    << space;
	os << mat->cols    << space;

	for (i = 0; i < mat->rows; i++) {
		for (j = 0; j < mat->cols; j++) {
			os << mat->ptr[i][j] << space;
		}
	}

	for (i = 0; i < mat->rows; i++) {
		os << rho[i].ind << space << rho[i].atributo << space;
	}

	return os;
}


std::istream& operator>>(std::istream& is, MatrizLSSS& M)
{
	std::string str("");
	int rows, cols, i, j;
	is >> rows; is >> cols;
	M.matriz.Mat = M.matrix_allocate(rows, cols);

	for (i = 0; i < rows; i++) {
		for (j = 0; j < cols; j++) {
			is >> str;
			M_setStr(M.matriz.Mat->ptr[i][j], str);
		}
	}

	M.matriz.rho = new MAP[rows];

	for (i = 0; i < rows; i++) {
		is >> M.matriz.rho[i].ind;
		is >> str;
		M.matriz.rho[i].atributo = new char[str.length() + 1];
		strcpy(M.matriz.rho[i].atributo, str.c_str());
	}

	return is;
}


void MatrizLSSS::trianguliza(MATRIX* A)
{
	int i, j, k, l, ind, act, bol;
	M_sint val, aux, max, a;
	act = 0;

	//Busca la fila con el primer numero diferente de cero
	for (l = 0; l < A->cols; l++) {
		max = 0; ind = l;

		for (k = act; k < A->rows; k++) {
			if (A->ptr[k][l] == 1) {
				max = A->ptr[k][l];
				ind = k;
				break;
			}

			if (A->ptr[k][l] != 0) {
				if (max != 0 && M_abs(A->ptr[k][l]) < M_abs(max)) {
					max = A->ptr[k][l];
					ind = k;

				} else if (max == 0) {
					max = A->ptr[k][l];
					ind = k;
				}
			}
		}

		if (max != 0) {
			//Reemplaza por la fila actual
			for (i = 0; i < A->cols; i++) {
				aux = A->ptr[act][i];
				A->ptr[act][i] = A->ptr[ind][i];
				A->ptr[ind][i] = aux;
			}

			//Reduce
			for (j = act + 1; j < A->rows; j++) {
				if (A->ptr[j][l] != 0) {
					aux = A->ptr[j][l] / max;

					for (i = l; i < A->cols; i++) {
						A->ptr[j][i] = A->ptr[j][i] - A->ptr[act][i] * aux;
					}
				}
			}

			val = M_abs(A->ptr[act][l]);

			if (val > 0) {
				bol = 1;

				for (i = l; i < A->cols; i++) {
					if (M_abs(A->ptr[act][i]) % val != 0) {
						bol = 0; break;
					}
				}

				if (bol) {
					for (i = l; i < A->cols; i++) {
						A->ptr[act][i] = A->ptr[act][i] / val;
					}
				}
			}

			act++;
		}
	}
}

