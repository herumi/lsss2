#ifndef GLOBAL_H
#define	GLOBAL_H

#define tmsp 20
//#include <string>
#include <string.h>


//using namespace std;
#include "../../include/cpabe.h"

typedef enum { typeCon, typeId, typeOpr } nodeEnum;

/* constants */
typedef struct {
    int value;                  /* value of constant */
} conNodeType;

/* identifiers */
typedef struct {
    char str[50];         /* subscript to sym array */
} idNodeType;

/* operators */
typedef struct {
    int oper;                   /* operator */
    int nops;                   /* number of operands */
    struct nodeTypeTag *op[1];  /* operands (expandable) */
} oprNodeType;

typedef struct nodeTypeTag {
    nodeEnum type;              /* type of node */

    /* union must be last entry in nodeType */
    /* because operNodeType may dynamically increase */
    union {
        conNodeType con;        /* constants */
        idNodeType id;          /* identifiers */
        oprNodeType opr;        /* operators */
    };
} nodeType;


/* Matriz */
typedef struct {
	int rows;
	int cols;
	M_sint **ptr;
} MATRIX;

typedef struct {
	char *atributo;
	int ind;
} MAP;

typedef struct matrizLSSSType {
	MATRIX *Mat;
	MAP *rho;
} MatrizType;

typedef struct {
	M_sint omega;
	int ind;
} COEFF;

//extern int sym[26];

#endif
