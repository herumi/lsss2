/**
	@file
	@brief Public and Private Key structures (G2 version)
    @author Zavattoni et al.
	@license modified new BSD license
	http://www.opensource.org/licenses/BSD-3-Clause
*/

#include "KeyG2.h"
#include "Attribute.h"
#include "Alice.h"

std::ostream& operator<<(std::ostream& os, const KeyG2& key)
{
	const char space = ' ';
	serialize(os, key.K, NUM_OF_ARRAY(key.K));
	serialize(os, key.L, NUM_OF_ARRAY(key.L));
	os << key.K_Att.size() << space;

	for (Fp2_map::const_iterator i = key.K_Att.begin(), ie = key.K_Att.end(); i != ie; ++i) {
		os << i->first << space << i->second << space;
	}
	
	serialize(os, &key.l[0][0], NUM_OF_DBL_ARRAY(key.l));

	return os;
}

std::istream& operator>>(std::istream& is, KeyG2& key)
{
	deserialize(is, key.K, NUM_OF_ARRAY(key.K));
	deserialize(is, key.L, NUM_OF_ARRAY(key.L));
	size_t size;
	is >> size;
	Fp2_map& mapFp2 = key.K_Att;
	mapFp2.clear();

	for (size_t i = 0; i < size; i++) {
		std::string name;
		PointG2 pg2;
		is >> name;
		is >> pg2;
		mapFp2[name] = pg2;
	}

    deserialize(is, &key.l[0][0], NUM_OF_DBL_ARRAY(key.l));
    
	return is;
}

