/**
	@file
	@brief Launch Delegation (G2 version)
    @author Zavattoni et al.
	@license modified new BSD license
	http://www.opensource.org/licenses/BSD-3-Clause
*/

#include <fstream>

#include "Alice.h"
#include "Server.h"
#include "Bob.h"
#include "bench.h"
#include "opti.h"

#define BENCH_TIMES	50

int main(int argc, char** argv)
{
	/* initializes the BN curve */
	bn::ParamT<Fp2>::init(0);
	Key UserKey;
	{
		std::ifstream fUk("UserKey");
		fUk >> UserKey;
	}
	PublicKey PK;
	{
		std::ifstream fPk("public_key");
		fPk >> PK;
	}
	std::vector<std::string> S;

	for (int i = 1; i < argc; i++) {
		S.push_back(argv[i]);
	}

	Precompute prec;
	{
		std::ifstream fPrec("Precomputation");
		fPrec >> prec;
	}
	Key KeyDelegate;
	Bob bob;
	bob.delegate(&KeyDelegate, PK, UserKey, S, prec);
	BENCH_BEGIN("step keygen", BENCH_TIMES) {
		BENCH_ADD(Key key; bob.delegate(&key, PK, UserKey, S, prec);
		          , BENCH_TIMES);
	}
	BENCH_END(BENCH_TIMES);
	{
		std::ofstream fSkDel("UserKeyDel");
		fSkDel << KeyDelegate;
	}
	return 0;
}

