/**
	@file
	@brief Point in G1 structure
    @author Zavattoni et al.
	@license modified new BSD license
	http://www.opensource.org/licenses/BSD-3-Clause
*/

#include "PointG1.h"

PointG1::PointG1()
{
	for (size_t i = 0; i < NUM_OF_ARRAY(pt); i++) {
		pt[i].clear();
	}

	Fp* p = &PsI[0][0];

	for (size_t i = 0; i < NUM_OF_DBL_ARRAY(PsI); i++) {
		p[i].clear();
	}
}

std::ostream& operator<<(std::ostream& os, const PointG1& pg1)
{
	serialize(os, pg1.pt, NUM_OF_ARRAY(pg1.pt));
	serialize(os, &pg1.PsI[0][0], NUM_OF_DBL_ARRAY(pg1.PsI));
	return os;
}
std::istream& operator>>(std::istream& is, PointG1& pg1)
{
	deserialize(is, pg1.pt, NUM_OF_ARRAY(pg1.pt));
	deserialize(is, &pg1.PsI[0][0], NUM_OF_DBL_ARRAY(pg1.PsI));
	return is;
}

