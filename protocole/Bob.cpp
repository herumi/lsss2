/**
	@file
	@brief Decryption and Delegate steps
    @author Zavattoni et al.
	@license modified new BSD license
	http://www.opensource.org/licenses/BSD-3-Clause
*/

#include "Bob.h"
#include "Alice.h"
#include "cpabe.h"
#include "bench.h"
#include "PublicKey.h"
#include "opti.h"
#include "global.h"
#include "sha256.h"
#include <vector>
#include "random.h"

#define BENCH_TIMES	50
extern uint64_t PT[8][256];

void Bob::set(int argc, char** argv)
{
	for (int i = 1; i < argc; i++) {
		attr.push_back(argv[i]);
	}
}

void Bob::delegate(Key* /*key*/, const PublicKey& PK, const Key& UserKey, const std::vector<std::string>& S, const Precompute& precompute)
{
	using namespace mie;
	RandInt rg;
	//////// PRECOMP
	Fp2 QsI[PRECOMPUTEknownS][2];
	M_uint tTilde;
	Fp g_atTilde [3];
	Fp2 g2_tTilde[3];
	const Fp_map& KAtt = UserKey.getK_Att();
	Key Delegation;
	Fp_map& KAttDel = Delegation.getK_Att_W();
	const M_sint& BetaS = precompute.getBetaS();
	const M_sint* W = precompute.getW();
	precompute.getQsI(QsI);
	const M_sint (*SB)[2] = precompute.getSB();
	const Fp_map& attributes = precompute.getSetOfAttributes();
	const Fp2* L = UserKey.getL();
	const Fp* K = UserKey.getK();
	tTilde = rg.get();
	G1_mul(g_atTilde, tTilde, PK.getg_a(), W, SB, BetaS);
	///////if(g_atTilde[2]!=1)mie::bn::ecop::Normalize(g_atTilde,g_atTilde);
	addJJJG1(Delegation.getK_W(), K, g_atTilde);
	G2_mulknownSC(g2_tTilde, tTilde, QsI, PT, precompute.t256, precompute.Half_Zr);
	//////////if(g2_tTilde[2]!=1)mie::bn::ecop::Normalize(g2_tTilde,g2_tTilde);
	addJJJG2(Delegation.getL_W(), L, g2_tTilde);

	for (size_t i = 0; i < S.size(); i++) {
		if (UserKey.isOnAttributes(S[i])) {
			PointG1& puntoG1 = KAttDel[S[i]];
			Fp H_tTilde[3];
			const PointG1& test_att = attributes.get(S[i]);
			const Fp* test = test_att.getG1();
			const PointG1& p_g1_D = KAtt.get(S[i]);
			G1_mul(H_tTilde, tTilde, test, W, SB, BetaS);
			//////////if(H_tTilde[2]!=1)mie::bn::ecop::Normalize(H_tTilde,H_tTilde);
			const Fp* K_x = p_g1_D.getG1();
			addJJJG1(puntoG1.getG1_W(), K_x, H_tTilde);
			puntoG1.setAtt(S[i]);
			KAttDel[S[i]] = puntoG1;
		}
	}
}

Fp Bob::dec(CypherText* CT, const MatrizType& mlsss, const Key& SK, const Precompute& precompute, MatrizLSSS* M)
{
	using namespace mie;
	int l, n, lval;
	const M_uint& r = M_param_r();
	Fp2 QsI[PRECOMPUTEknownS][2];
	Fp12 C_12;
	Fp12 DecryptKey;
	Fp12 e4;
	Fp12 e4_inv;
	Fp12 e3;
	Fp12 e;
	M_uint omega;
	Fp C_sum[3];
	Fp K_Delta[3];
	C_sum[0] = 1;
	C_sum[1] = 1;
	C_sum[2] = 0;
	const Fp_map& K_Att = SK.getK_Att();
	const Fp_map& C = CT->getCAtt();
	const Fp2_map& D = CT->getDAtt();
	precompute.getQsI(QsI);
	const M_sint* W = precompute.getW();
	const M_sint* WB = precompute.getWB();
	const M_sint (*SB)[2] = precompute.getSB();
	const M_sint& BetaS = precompute.getBetaS();
	const M_sint (*BB)[4] = precompute.getBB();
	//////// PRECOMP
	DecryptKey.clear();
	e4.clear();
	e4_inv.clear();
	e.clear();
	C_12.clear();
	e3.clear();
	e3 = 1;
	C_12 = CT->getC();
	const Fp2* C_prime = CT->getC_Prime();
	const Fp* K = SK.getK();
	l = mlsss.Mat->rows;
	n = mlsss.Mat->cols;
	//MatrizLSSS * M=new MatrizLSSS();
	MATRIX* Mattdos = M->matrix_allocate(l, n);
	std::vector<COEFF> coef(l);
	lval = 0;

	for (int i = 0; i < l; i++) {
		if (SK.isOnAttributes(mlsss.rho[i].atributo)) {
			coef[lval].ind = i;

			for (int j = 0; j < n; j++) {
				Mattdos->ptr[lval][j] = mlsss.Mat->ptr[i][j];
			}

			lval++;
		}
	}

	Mattdos->rows = lval;
	int cptr = 0;
	M_sint Delta;
	bool passe = M->getCoeffients(Mattdos, &coef[0], &Delta);
	M->matrix_free(Mattdos);

	if (!passe) {
		return DecryptKey.a_.a_.a_;
	}

	//PUT(Delta);
	for (int j = 0; j < l; j++) {
		//PUT(coef[j].omega);
		if (!M_isZero(coef[j].omega)) cptr++;
	}

	Fp2 QQ[cptr + 1][3];
	Fp PP[cptr + 2][3];
	cptr = 0;

	if (Delta == 1) {
		//PUT(lval);
		for (int i = 0; i < lval; i++) {
			if (!M_isZero(coef[i].omega)) {
				std::stringstream oss;
				oss << mlsss.rho[coef[i].ind].atributo << mlsss.rho[coef[i].ind].ind ;
				const std::string cad = oss.str();
				Fp C_omega [3];
				Fp K_omega [3];
				omega = M_abs(coef[i].omega);
				const PointG1& C_Point = C.get(cad);
				const PointG1& p_g1_D = K_Att.get(mlsss.rho[coef[i].ind].atributo);
				const Fp* C_i = C_Point.getG1();
				G1_mul(C_omega, omega, C_i, W, SB, BetaS);
				//////////if(C_omega[2]!=1)mie::bn::ecop::Normalize(C_omega,C_omega);

				if (coef[i].omega < 0)Fp::neg(C_omega[1], C_omega[1]);

				const Fp* K_i = p_g1_D.getG1();
				const PointG2& D_Point = D.get(cad);
				//G1_mulSmall(K_omega,omega,K_i);
				G1_mul(K_omega, omega, K_i, W, SB, BetaS);

				//////////if(K_omega[2]!=1)mie::bn::ecop::Normalize(K_omega,K_omega);
				if (coef[i].omega < 0)Fp::neg(K_omega[1], K_omega[1]);

				if (C_sum[2].isZero()) {
					C_sum[0] = C_omega[0];
					C_sum[1] = C_omega[1];
					C_sum[2] = C_omega[2];

				} else {
					addJJJG1(C_sum, C_sum, C_omega);
				}

				//////////if(C_sum[2]!=1)mie::bn::ecop::Normalize(C_sum,C_sum);
				const Fp2* D_i = D_Point.getG2();

				QQ[cptr][0] = D_i[0];
				QQ[cptr][1] = D_i[1];
				QQ[cptr][2] = D_i[2];
				PP[cptr+1][0] = K_omega[0];
				PP[cptr+1][1] = K_omega[1];
				PP[cptr+1][2] = K_omega[2];
				cptr++;
			}
		}
		QQ[cptr][0] = C_prime[0];
		QQ[cptr][1] = C_prime[1];
		QQ[cptr][2] = C_prime[2];
		PP[cptr+1][0] = K[0];
		PP[cptr+1][1] = -K[1];
		PP[cptr+1][2] = K[2];

		PP[0][0] = C_sum[0];
		PP[0][1] = C_sum[1];
		PP[0][2] = C_sum[2];

		Fp6 l_T[1][70];
		SK.getPrecomputeForPairing(l_T);

		Fp12 eeeb;
		//eeeb.clear();
		
		bn::opt_atePairingKnownG2Mixed(eeeb, 1, l_T, cptr+1, QQ, PP);
		//  TEST_EQUAL(eee,eeeb);
		DecryptKey = C_12 * eeeb;
	}

	if (Delta != 1) {
		//PUT(lval);
		for (int i = 0; i < lval; i++) {
			if (!M_isZero(coef[i].omega)) {
				std::stringstream oss;
				oss << mlsss.rho[coef[i].ind].atributo << mlsss.rho[coef[i].ind].ind ;
				const std::string cad = oss.str();
				Fp C_omega [3];
				Fp K_omega [3];
				omega = M_abs(coef[i].omega);
				const PointG1& C_Point = C.get(cad);
				const PointG1& p_g1_D = K_Att.get(mlsss.rho[coef[i].ind].atributo);
				const Fp* C_i = C_Point.getG1();
				//G1_mulSmall(C_omega,omega,C_i);
				G1_mul(C_omega, omega, C_i, W, SB, BetaS);
				//////////if(C_omega[2]!=1)mie::bn::ecop::Normalize(C_omega,C_omega);

				if (coef[i].omega < 0)Fp::neg(C_omega[1], C_omega[1]);

				const Fp* K_i = p_g1_D.getG1();
				const PointG2& D_Point = D.get(cad);
				//G1_mulSmall(K_omega,omega,K_i);
				G1_mul(K_omega, omega, K_i, W, SB, BetaS);

				//////////if(K_omega[2]!=1)mie::bn::ecop::Normalize(K_omega,K_omega);
				if (coef[i].omega < 0)Fp::neg(K_omega[1], K_omega[1]);

				if (C_sum[2].isZero()) {
					C_sum[0] = C_omega[0];
					C_sum[1] = C_omega[1];
					C_sum[2] = C_omega[2];

				} else {
					addJJJG1(C_sum, C_sum, C_omega);
				}

				////////// if(C_sum[2]!=1)mie::bn::ecop::Normalize(C_sum,C_sum);
				const Fp2* D_i = D_Point.getG2();
				
				QQ[cptr][0] = D_i[0];
				QQ[cptr][1] = D_i[1];
				QQ[cptr][2] = D_i[2];
				PP[cptr+1][0] = K_omega[0];
				PP[cptr+1][1] = K_omega[1];
				PP[cptr+1][2] = K_omega[2];
				cptr++;
			}
		}

		//PUT("Delta != 1");
		G1_mul(K_Delta, M_abs(Delta), K, W, SB, BetaS);
		//G1_mulSmall(K_Delta,Delta.get(),K);
		//////////if(K_Delta[2]!=1)bn::ecop::Normalize(K_Delta,K_Delta);

		if (Delta > 0)Fp::neg(K_Delta[1], K_Delta[1]);

		QQ[cptr][0] = C_prime[0];
		QQ[cptr][1] = C_prime[1];
		QQ[cptr][2] = C_prime[2];
		PP[cptr+1][0] = K_Delta[0];
		PP[cptr+1][1] = K_Delta[1];
		PP[cptr+1][2] = K_Delta[2];

		PP[0][0] = C_sum[0];
		PP[0][1] = C_sum[1];
		PP[0][2] = C_sum[2];

		Fp6 l_T[1][70];
		SK.getPrecomputeForPairing(l_T);

		Fp12 eeeb;
		//eeeb.clear();
		
		//bn::opt_ateMultiPairingJ(eeeb, cptr + 1, QQ, PP);
		bn::opt_atePairingKnownG2Mixed(eeeb, 1, l_T, cptr+1, QQ, PP);

		Fp12 eeee;
		eeee.clear();

		M_uint Delta_pos = M_abs(Delta);
		M_uint Delta_pos_inv;
		Delta_pos_inv = inverse2(Delta_pos, r);
		//PUT(Delta_pos_inv);
		//PUT((Delta_pos_inv*Delta_pos)%r);
		GT_expo(eeee, eeeb, Delta_pos_inv, WB, BB);

		if (Delta < 0) {
			Fp12 e5;
			e5.clear();
			e5 = eeee;
			e5.inverse();
			DecryptKey = C_12 * e5;

		} else {
			DecryptKey = C_12 * eeee;
		}
	}

	return DecryptKey.a_.a_.a_;
}

void Bob::delegateG2(KeyG2* /*key*/, const PublicKeyG2& PK, const KeyG2& UserKey, const std::vector<std::string>& S, const PrecomputeG2& precompute)
{
    using namespace mie;
	RandInt rg;
	//////// PRECOMP
	mie::Fp PsI[PRECOMPUTEknownS][2];
	M_uint tTilde;
	Fp2 g2_atTilde [3];
	mie::Fp  g1_tTilde[3];
    Fp2 Qa[3]; Qa[0] = PK.getg_a()[0]; Qa[1] = PK.getg_a()[1]; Qa[2] = PK.getg_a()[2];
    bn::ecop::NormalizeJac(Qa, Qa);
	const Fp2_map& KAtt = UserKey.getK_Att();
	KeyG2 Delegation;
	Fp2_map& KAttDel = Delegation.getK_Att_W();
	const M_sint* WB = precompute.getWB();
	precompute.getPsI(PsI);
	const M_sint (*BB)[4] = precompute.getBB();
	const Fp2_map& attributes = precompute.getSetOfAttributes();
	const Fp* L = UserKey.getL();
	const Fp2* K = UserKey.getK();
	tTilde = rg.get();
	G2_mul(g2_atTilde, Qa, tTilde, WB, BB);
	///////if(g_atTilde[2]!=1)mie::bn::ecop::Normalize(g_atTilde,g_atTilde);
	addJJJG2(Delegation.getK_W(), K, g2_atTilde);
	G1_mulknownSC(g1_tTilde, tTilde, PsI, PT, precompute.t256, precompute.Half_Zr);
	//////////if(g2_tTilde[2]!=1)mie::bn::ecop::Normalize(g2_tTilde,g2_tTilde);
	addJJJG1(Delegation.getL_W(), L, g1_tTilde);

   	for (size_t i = 0; i < S.size(); i++) {
		if (UserKey.isOnAttributes(S[i])) {
			PointG2& puntoG2 = KAttDel[S[i]];
			Fp2 H_tTilde[3];
			const PointG2& test_att = attributes.get(S[i]);
			Fp2 test[3];
            test[0] = test_att.getG2()[0]; test[1] = test_att.getG2()[1]; test[2] = test_att.getG2()[2];
            //bn::ecop::NormalizeJac(test,test);
			const PointG2& p_g2_D = KAtt.get(S[i]);
			G2_mul(H_tTilde, test, tTilde, WB, BB);
			//////////if(H_tTilde[2]!=1)mie::bn::ecop::Normalize(H_tTilde,H_tTilde);
			const Fp2* K_x = p_g2_D.getG2();
			addJJJG2(puntoG2.getG2_W(), K_x, H_tTilde);
			puntoG2.setAtt(S[i]);
			KAttDel[S[i]] = puntoG2;
		}
	}
}

Fp Bob::decG2(CypherTextG2* CT, const MatrizType& mlsss, const KeyG2& SK, const PrecomputeG2& precompute, MatrizLSSS* M)
{
	using namespace mie;
	int l, n, lval;
	const M_uint& r = M_param_r();
	Fp2 QsI[PRECOMPUTEknownS][2];
	Fp12 C_12;
	Fp12 DecryptKey;
	Fp12 e4;
	Fp12 e4_inv;
	Fp12 e3;
	Fp12 e;
	M_uint omega;
	Fp2 C_sum[3];
	Fp2 K_Delta[3];
	C_sum[0] = 1;
	C_sum[1] = 1;
	C_sum[2] = 0;
	const Fp2_map& K_Att = SK.getK_Att();
	const Fp2_map& C = CT->getCAtt();
	const Fp_map& D = CT->getDAtt();
	precompute.getQsI(QsI);
	const M_sint* WB = precompute.getWB();
	const M_sint (*BB)[4] = precompute.getBB();
	//////// PRECOMP
	DecryptKey.clear();
	e4.clear();
	e4_inv.clear();
	e.clear();
	C_12.clear();
	e3.clear();
	e3 = 1;
	C_12 = CT->getC();
	const Fp* C_prime = CT->getC_Prime();
	const Fp* L = SK.getL();
	Fp2 K[3]; K[0] = SK.getK()[0];
    K[1] = SK.getK()[1]; K[2] = SK.getK()[2];
	l = mlsss.Mat->rows;
	n = mlsss.Mat->cols;
	//MatrizLSSS * M=new MatrizLSSS();
	MATRIX* Mattdos = M->matrix_allocate(l, n);
	std::vector<COEFF> coef(l);
	lval = 0;

	for (int i = 0; i < l; i++) {
		if (SK.isOnAttributes(mlsss.rho[i].atributo)) {
			coef[lval].ind = i;

			for (int j = 0; j < n; j++) {
				Mattdos->ptr[lval][j] = mlsss.Mat->ptr[i][j];
			}

			lval++;
		}
	}

	Mattdos->rows = lval;
	int cptr = 0;
	M_sint Delta;
	bool passe = M->getCoeffients(Mattdos, &coef[0], &Delta);
	M->matrix_free(Mattdos);

	if (!passe) {
		return DecryptKey.a_.a_.a_;
	}

	//PUT(Delta);
	for (int j = 0; j < l; j++) {
		//PUT(coef[j].omega);
		if (!M_isZero(coef[j].omega)) cptr++;
	}

	Fp2 QQ[cptr + 2][3];
	Fp  PP[cptr + 2][3];
	cptr = 0;

	if (Delta == 1) {

		//PUT(lval);
		for (int i = 0; i < lval; i++) {
			if (!M_isZero(coef[i].omega)) {
				std::stringstream oss;
				oss << mlsss.rho[coef[i].ind].atributo << mlsss.rho[coef[i].ind].ind ;
				const std::string cad = oss.str();

				Fp2 C_omega [3];
				omega = M_abs(coef[i].omega);

				const PointG2& C_Point = C.get(cad);
				const PointG2& p_g2_D = K_Att.get(mlsss.rho[coef[i].ind].atributo);
				Fp2 C_i[3]; C_i[0] = C_Point.getG2()[0];
                C_i[1] = C_Point.getG2()[1]; C_i[2] = C_Point.getG2()[2];
                //bn::ecop::NormalizeJac(C_i, C_i);

				G2_mul(C_omega, C_i, omega, WB, BB);
				if (coef[i].omega < 0) Fp2::neg(C_omega[1], C_omega[1]);

				if (C_sum[2].isZero()) {
					C_sum[0] = C_omega[0];
					C_sum[1] = C_omega[1];
					C_sum[2] = C_omega[2];

				} else {
					addJJJG2(C_sum, C_sum, C_omega);
				}

                Fp2 K_i[3]; K_i[0] = p_g2_D.getG2()[0];
                K_i[1] = p_g2_D.getG2()[1];K_i[2] = p_g2_D.getG2()[2];

				const PointG1& D_Point = D.get(cad);
				Fp D_i[3] = { D_Point.getG1()[0], D_Point.getG1()[1], D_Point.getG1()[2] };
				G1_mul(D_i, omega, D_i, precompute.getW(), precompute.getSB(), precompute.getBetaS());
				if (coef[i].omega < 0) Fp::neg(D_i[1], D_i[1]);

				PP[cptr][0] = D_i[0];
				PP[cptr][1] = D_i[1];
				PP[cptr][2] = D_i[2];
				cptr++;
				//PUT(cptr);
			}
		}

		PP[cptr][0] = C_prime[0];
		PP[cptr][1] = C_prime[1];
    		PP[cptr][2] = C_prime[2];
		cptr++;

		PP[cptr][0] = L[0];
		PP[cptr][1] = L[1];
		PP[cptr][2] = L[2];
		QQ[0][0] = C_sum[0];
		QQ[0][1] = C_sum[1];
		QQ[0][2] = C_sum[2];

        Fp6 l_T[lval+1][70];
        SK.getPrecomputeForPairing(l_T, lval+1);

		Fp12 eeeb;
		//eeeb.clear();

		bn::opt_atePairingKnownG2Mixed(eeeb, lval+1, l_T, 1, QQ, PP);
		//  TEST_EQUAL(eee,eeeb);
		DecryptKey = C_12 * eeeb;
	}

	if (Delta != 1) {
		//PUT("Delta != 1");

		//PUT(lval);
		for (int i = 0; i < lval; i++) {
			if (!M_isZero(coef[i].omega)) {
				std::stringstream oss;
				oss << mlsss.rho[coef[i].ind].atributo << mlsss.rho[coef[i].ind].ind ;
				const std::string cad = oss.str();
				Fp2 C_omega [3];
				omega = M_abs(coef[i].omega);

				const PointG2& C_Point = C.get(cad);
				const PointG2& p_g2_D = K_Att.get(mlsss.rho[coef[i].ind].atributo);
				Fp2 C_i[3]; C_i[0] = C_Point.getG2()[0];
                C_i[1] = C_Point.getG2()[1]; C_i[2] = C_Point.getG2()[2];
                //bn::ecop::NormalizeJac(C_i, C_i);

				G2_mul(C_omega, C_i, omega, WB, BB);
				if (coef[i].omega < 0) Fp2::neg(C_omega[1], C_omega[1]);

                if (C_sum[2].isZero()) {
					C_sum[0] = C_omega[0];
					C_sum[1] = C_omega[1];
					C_sum[2] = C_omega[2];

				} else {
					addJJJG2(C_sum, C_sum, C_omega);
				}

				Fp2 K_i[3]; K_i[0] = p_g2_D.getG2()[0];
				K_i[1] = p_g2_D.getG2()[1];
                K_i[2] = p_g2_D.getG2()[2];
                bn::ecop::NormalizeJac(K_i, K_i);

                const PointG1& D_Point = D.get(cad);
				Fp D_i[3] = { D_Point.getG1()[0], D_Point.getG1()[1], D_Point.getG1()[2] };
                G1_mul(D_i, omega, D_i, precompute.getW(), precompute.getSB(), precompute.getBetaS());
				if (coef[i].omega < 0) Fp::neg(D_i[1], D_i[1]);

				PP[cptr][0] = D_i[0];
				PP[cptr][1] = D_i[1];
				PP[cptr][2] = D_i[2];
				cptr++;
			}
		}

		G2_mul(K_Delta,  K, M_abs(Delta), WB, BB);

		if (Delta > 0) Fp2::neg(K_Delta[1], K_Delta[1]);

		PP[cptr][0] = C_prime[0];
		PP[cptr][1] = C_prime[1];
		PP[cptr][2] = C_prime[2];
		QQ[cptr][0] = K_Delta[0];
		QQ[cptr][1] = K_Delta[1];
		QQ[cptr][2] = K_Delta[2];
		cptr++;

		PP[cptr][0] = L[0];
		PP[cptr][1] = L[1];
		PP[cptr][2] = L[2];
		QQ[cptr][0] = C_sum[0];
		QQ[cptr][1] = C_sum[1];
		QQ[cptr][2] = C_sum[2];

		Fp6 l_T[lval+1][70];
        SK.getPrecomputeForPairing(l_T, lval+1);

		Fp12 eeeb;
		//eeeb.clear();

		bn::opt_atePairingKnownG2Mixed(eeeb, lval+1, l_T, 1, QQ, PP);
		DecryptKey = C_12 * eeeb;

		Fp12 eeee;
		eeee.clear();
		M_uint Delta_pos = M_abs(Delta);
		M_uint Delta_pos_inv;
		Delta_pos_inv = inverse2(Delta_pos, r);
		//PUT(Delta_pos_inv);
		//PUT((Delta_pos_inv*Delta_pos)%r);
		GT_expo(eeee, eeeb, Delta_pos_inv, WB, BB);

		if (Delta < 0) {
			Fp12 e5;
			e5.clear();
			e5 = eeee;
			e5.inverse();
			DecryptKey = C_12 * e5;

		} else {
			DecryptKey = C_12 * eeee;
		}
	}

	return DecryptKey.a_.a_.a_;

}


/***
    Local Variables:
    c-basic-offset: 4
    indent-tabs-mode: t
    tab-width: 4
    End:
*/

