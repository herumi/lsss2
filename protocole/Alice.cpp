/**
	@file
	@brief Encryption steps
    @author Zavattoni et al.
	@license modified new BSD license
	http://www.opensource.org/licenses/BSD-3-Clause
*/

#include "Alice.h"
#include "cpabe.h"
#include "PublicKey.h"
#include <sstream>
#include <vector>
#include "MatrizLSSS.h"
#include "Precompute.h"
#include "bench.h"
#include "PT.h"
#include "random.h"

extern uint64_t PT[8][256];

void Alice::enc(CypherText* CT, const PublicKey& /* PK */, const Fp& M, const MatrizType& mlsss, const Precompute& precompute)
{
	using namespace mie;
	RandInt rg;
	//////// PRECOMP
	const Fp2 (*QsIPrec)[2] = precompute.getQsIPtr();
	const Fp12 (*fe_alpha) = precompute.getFeAlphaPtr();
	Fp_map& C_Att = CT->getCAtt_W();
	Fp2_map& D_Att = CT->getDAtt_W();
	//const M_sint *WBPrec = precompute.getWB();
	//const M_sint (*BBPrec)[4] = precompute.getBB();
#ifdef UNKNOWN_ATTRIBUTES
	const M_sint* WPrec = precompute.getW();
	const M_sint (*SBPrec)[2] = precompute.getSB();
	const M_sint& BetaSPrec = precompute.getBetaS();
#endif
	const Fp_map& attributes = precompute.getSetOfAttributes();
	const M_uint s = rg.get();
	Fp12 C_s;
	GT_expoknownSC(C_s, s, fe_alpha, PT, precompute.t256, precompute.Half_Zr);
	{
		Fp12 C(C_s);
		bn::Fp2::mul_Fp_0(C.a_.a_, C.a_.a_, M);
		bn::Fp2::mul_Fp_0(C.a_.b_, C.a_.b_, M);
		bn::Fp2::mul_Fp_0(C.a_.c_, C.a_.c_, M);
		bn::Fp2::mul_Fp_0(C.b_.a_, C.b_.a_, M);
		bn::Fp2::mul_Fp_0(C.b_.b_, C.b_.b_, M);
		bn::Fp2::mul_Fp_0(C.b_.c_, C.b_.c_, M);
		CT->SetC(C);
	}
	Fp2 C_Prime [3];
	G2_mulknownSC(C_Prime, s, QsIPrec, PT, precompute.t256, precompute.Half_Zr);
	//if(C_Prime[2]!=1) mie::bn::ecop::Normalize(C_Prime,C_Prime);
	CT->SetC_Prime(C_Prime);
	const int l = mlsss.Mat->rows;
	const int n = mlsss.Mat->cols;
	M_uint vector [n];
	vector [0] = s;

	for (int i = 1; i < n; i++) {
		vector[i] = rg.get();
	}

	std::vector<M_uint> lambda(l);

	for (int j = 0; j < l; j++) {
		for (int i = 0; i < n; i++) {
			const M_uint& tmp2 = M_abs(mlsss.Mat->ptr[j][i]);
			lambda [j] += vector [i] * tmp2;
		}
		lambda[j] %= M_param_r();
	}

	MAP* rho = mlsss.rho;
	//Fp g_a_lambda [3];
	Fp G1tmp [3];
#ifdef UNKNOWN_ATTRIBUTES
	Fp G1tmp2 [3];
#endif
	//Fp tmp3 [3];
	Fp tmp4 [3];

	for (int i = 0; i < l; i++) {
		std::stringstream oss;
		oss << rho[i].atributo << rho[i].ind;
		const std::string cad = oss.str();
		PointG1& puntoG1 = C_Att[cad];
		PointG2& puntoG2 = D_Att[cad];
		puntoG1.setAtt(cad);
		puntoG2.setAtt(cad);
		// @note removed const quantifier
		M_uint rCD = rg.get();
		G1_mulknownSC(G1tmp, lambda[i], precompute.getPsIga(), PT, precompute.t256, precompute.Half_Zr); //this one is not working
#ifdef UNKNOWN_ATTRIBUTES
		hash_and_mapB(G1tmp2[0], G1tmp2[1], rho[i].atributo);
		G1tmp2[2] = Fp(1);
		G1_mul(tmp4, rCD, G1tmp2, WPrec, SBPrec, BetaSPrec);
#else
		const PointG1& test_att = attributes.get(rho[i].atributo);
		G1_mulknownSC(tmp4, rCD, test_att.getPsI(), PT, precompute.t256, precompute.Half_Zr);
#endif
		// if(tmp4[2]!=1) mie::bn::ecop::Normalize(tmp4,tmp4);
		Fp::neg(tmp4[1], tmp4[1]);
		//if(tmp4[2]!=1)  mie::bn::ecop::Normalize(tmp4,tmp4);
		addJJJG1(puntoG1.getG1_W(), G1tmp, tmp4);
		G2_mulknownSC(puntoG2.getG2_W(), rCD, QsIPrec, PT, precompute.t256, precompute.Half_Zr);
	}
}

void Alice::encG2(CypherTextG2* CT, const PublicKeyG2& /*PK*/, const Fp& M, const MatrizType& mlsss, const PrecomputeG2& precompute)
{
	using namespace mie;
	RandInt rg;
	//////// PRECOMP
	const Fp (*PsIPrec)[2] = precompute.getPsI();
	const Fp12 (*fe_alpha) = precompute.getFeAlphaPtr();
	Fp2_map& C_Att = CT->getCAtt_W();
	Fp_map& D_Att = CT->getDAtt_W();
#ifdef UNKNOWN_ATTRIBUTES
	const M_sint* WBPrec = precompute.getWB();
	const M_sint (*BBPrec)[4] = precompute.getBB();
#endif
	const Fp2_map& attributes = precompute.getSetOfAttributes();
	const M_uint s = rg.get();
	Fp12 C_s;
	GT_expoknownSC(C_s, s, fe_alpha, PT, precompute.t256, precompute.Half_Zr);
	{
		Fp12 C(C_s);
		bn::Fp2::mul_Fp_0(C.a_.a_, C.a_.a_, M);
		bn::Fp2::mul_Fp_0(C.a_.b_, C.a_.b_, M);
		bn::Fp2::mul_Fp_0(C.a_.c_, C.a_.c_, M);
		bn::Fp2::mul_Fp_0(C.b_.a_, C.b_.a_, M);
		bn::Fp2::mul_Fp_0(C.b_.b_, C.b_.b_, M);
		bn::Fp2::mul_Fp_0(C.b_.c_, C.b_.c_, M);
		CT->SetC(C);
	}
	Fp C_Prime [3];
	G1_mulknownSC(C_Prime, s, PsIPrec, PT, precompute.t256, precompute.Half_Zr);
	//if(C_Prime[2]!=1) mie::bn::ecop::Normalize(C_Prime,C_Prime);
	CT->SetC_Prime(C_Prime);
	const int l = mlsss.Mat->rows;
	const int n = mlsss.Mat->cols;
	M_uint vector [n];
	vector [0] = s;

	for (int i = 1; i < n; i++) {
		vector[i] = rg.get();
	}

	std::vector<M_uint> lambda(l);

	for (int j = 0; j < l; j++) {
		for (int i = 0; i < n; i++) {
			const M_uint& tmp2 = M_abs(mlsss.Mat->ptr[j][i]);
			lambda [j] += vector [i] * tmp2;
		}
		lambda[j] %= M_param_r();
	}

	MAP* rho = mlsss.rho;
	//Fp g_a_lambda [3];
	Fp2 G2tmp [3];
#ifdef UNKNOWN_ATTRIBUTES
	Fp2 G2tmp2 [3];
#endif
	//Fp tmp3 [3];
	Fp2 tmp4 [3];

	for (int i = 0; i < l; i++) {
		std::stringstream oss;
		oss << rho[i].atributo << rho[i].ind;
		const std::string cad = oss.str();
		PointG2& puntoG2 = C_Att[cad];
		PointG1& puntoG1 = D_Att[cad];
		puntoG2.setAtt(cad);
		puntoG1.setAtt(cad);
		// @note removed const quantifier
		M_uint rCD = rg.get();
		G2_mulknownSC(G2tmp, lambda[i], precompute.getQsIga(), PT, precompute.t256, precompute.Half_Zr);
#ifdef UNKNOWN_ATTRIBUTES
		hash_and_map2(G2tmp2[0], G2tmp2[1], rho[i].atributo);
		HashtoG2(tmp4, G2tmp2); tmp4[2] = 1;
		G2_mul(tmp4, tmp4, rCD, WBPrec, BBPrec);
#else
		const PointG2& test_att = attributes.get(rho[i].atributo);
		G2_mulknownSC(tmp4, rCD, test_att.getQsI(), PT, precompute.t256, precompute.Half_Zr);
#endif
		Fp2::neg(tmp4[1], tmp4[1]);
		//if(tmp4[2]!=1)  mie::bn::ecop::Normalize(tmp4,tmp4);
		addJJJG2(puntoG2.getG2_W(), G2tmp, tmp4);
		G1_mulknownSC(puntoG1.getG1_W(), rCD, PsIPrec, PT, precompute.t256, precompute.Half_Zr);
	}
}

/***
    Local Variables:
    c-basic-offset: 4
    indent-tabs-mode: t
    tab-width: 4
    End:
*/
