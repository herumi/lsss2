/**
	@file
	@brief Benchmark and verification helpers: ./bn | grep clk
    @author Zavattoni et al.
	@license modified new BSD license
	http://www.opensource.org/licenses/BSD-3-Clause
*/
#include <stdio.h>
#include <memory.h>
#include "xbyak/xbyak_util.h"
extern Xbyak::util::Clock sclk;
#include "bn.h"
#include <iostream>
#include "util.h"
#include "opti.h"
#include "bn-multi.h"
#include "random.h"
extern uint64_t PT[8][256];

using namespace bn;
using namespace ecop;

static int s_errNum = 0;
static int s_testNum = 0;

#define TEST_EQUAL(x, y) { s_testNum++; if (x != y) { s_errNum++; printf("%s(%d): err %s != %s\n", __FILE__, __LINE__, #x, #y); std::cout << "lhs=" << (x) << "\nrhs=" << (y) << std::endl; } }
#define TEST_ASSERT(x) { s_testNum++; if (!(x)) { s_errNum++; printf("%s(%d): err assert %s\n", __FILE__, __LINE__, #x); } }

#define FATAL_EXIT(msg) { printf("%s(%d): err %s\n", __FILE__, __LINE__, msg); exit(1); }

const M_uint& p = M_param_p();
const M_uint& r = M_param_r();

#define P_INIT Fp(2), Fp("16740896641879863340107777353588575149660814923656713498672603551465628253431")

#define P2_INIT Fp("13438486984812665827952643313785387127911349697151255879146972340858203091778"), Fp("12741389316352206200828479361093127917015298445269456291074734498988157668221")

M_uint pm1o4;
int pow2i[65];
M_uint pow2w_1i[4];
int ipow2w_1i[4];
#if 0
inline int toint(M_uint &tn){
  int l=tn.bitLen();
  int m1=0;
  for (int i=0;i<l;i++) if (tn.testBit(i)) m1+=pow2i[i];//pow(2,i);
  return m1;
}
inline int toint2(M_uint tn){
  int l=tn.bitLen();
  int m1=0;
  for (int i=0;i<l;i++) if (tn.testBit(i)) m1+=pow2i[i];//pow(2,i);
  return m1;
}
#endif

/*
M_uint BigToVuint(Big value){
  char cvalue[178]; // 1+Floor(Log(p))
  cvalue<<value;
  std::string svalue;
  svalue=cvalue;
  M_uint Vvalue; Vvalue.set(cvalue);
  return Vvalue;
}
// */
int main(int argc, char *argv[]) try
{
	argc--, argv++;
	int mode = -1;
	if (argc > 1 && strcmp(*argv, "-m") == 0) {
		mode = atoi(argv[1]);
	}
	Param::init(mode);

  const size_t N = 10000; // QQQ
  const size_t N2= 10000;
  const size_t K = 1000;
  M_uint Vk;
  Fp2 Q[3];Q[2]=1;
  Q[0]=Fp2(Fp("1526569384695140815723561162875597635216481810437138628361482123790755026813"),
           Fp("13703557017160381761049515716217192112749045062540006677094660930222433307406"));
  Q[1]=Fp2(Fp("14544560363942345482135594466035306103982445071895272941955646605233259296374"),
           Fp("10064490002957794680334175161733004840498613464368377518703742085166623589697"));
  Q[2]=Fp2(Fp("1"), Fp("0"));
  Fp2 lQ5[3],lQ5b[3];

  RandInt rg;
  Fp aa=rg.getFp();
  //Bcf=rand(Br);
  //cf=BigToVuint(Bcf);
  //mip->IOBASE=16;
  Fp bb=rg.getFp();
  Fp cc;
  Xbyak::util::Clock clkFpAdd;
  clkFpAdd.begin();
  for (size_t i = 0; i < N; i++) {
    //cc=aa*bb;
    Fp::add(cc,aa,bb);
  }
  clkFpAdd.end();
  printf("FpAdd\t% 10.3fKclk\n", (clkFpAdd.getClock() / K) / double(N));
  Xbyak::util::Clock clkFpSub;
  clkFpSub.begin();
  for (size_t i = 0; i < N; i++) {
    //cc=aa*bb;
    Fp::sub(cc,aa,bb);
  }
  clkFpSub.end();
  printf("FpSub\t% 10.3fKclk\n", (clkFpSub.getClock() / K) / double(N));
  Xbyak::util::Clock clkFp;
  clkFp.begin();
  for (size_t i = 0; i < N; i++) {
    //cc=aa*bb;
    Fp::mul(cc,aa,bb);
  }
  clkFp.end();
  printf("FpMult\t% 10.3fKclk\n", (clkFp.getClock() / K) / double(N));
  Xbyak::util::Clock clkFpSqr;
  clkFpSqr.begin();
  for (size_t i = 0; i < N; i++) {
    //cc=aa*bb;
    Fp::square(cc,aa);
  }
  clkFpSqr.end();
  printf("FpSqr\t% 10.3fKclk\n", (clkFpSqr.getClock() / K) / double(N));
  Fp Inverso;
  const Fp Anverso=Q[0].a_;
  Xbyak::util::Clock clkinv;
  clkinv.begin();
  for (size_t i = 0; i < N; i++) {
    Inverso=Anverso;
    Inverso.inverse();
  }
  clkinv.end();
  printf("FpInv\t% 10.3fKclk\n", (clkinv.getClock() / K) / double(N));
  std::cout<<"Inv*Anv="<<Inverso*Anverso<<std::endl;

  Fp2 cc2,aa2,bb2;
  aa2=Q[0]; bb2=Q[1];
  Xbyak::util::Clock clkFp2Add;
  clkFp2Add.begin();
  for (size_t i = 0; i < N; i++) {
    //cc=aa*bb2;
    Fp2::add(cc2,aa2,bb2);
  }
  clkFp2Add.end();
  printf("Fp2Add\t% 10.3fKclk\n", (clkFp2Add.getClock() / K) / double(N));
  Xbyak::util::Clock clkFp2Mul;
  clkFp2Mul.begin();
  for (size_t i = 0; i < N; i++) {
    //cc=aa*bb2;
    Fp2::mul(cc2,aa2,bb2);
  }
  clkFp2Mul.end();
  printf("Fp2Mult\t% 10.3fKclk\n", (clkFp2Mul.getClock() / K) / double(N));
  Xbyak::util::Clock clkFp2Sqr;
  clkFp2Sqr.begin();
  for (size_t i = 0; i < N; i++) {
    //cc=aa*bb2;
    Fp2::square(cc2,aa2);
  }
  clkFp2Sqr.end();
  printf("Fp2Sqr\t% 10.3fKclk\n", (clkFp2Sqr.getClock() / K) / double(N));
  Fp2 Inverso2;
  Fp2 Anverso2; Anverso2=Q[0];
  Xbyak::util::Clock clkFp2Inv;
  clkFp2Inv.begin();
  for (size_t i = 0; i < N; i++) {
    Inverso2=Anverso2;
    Inverso2.inverse();
  }
  clkFp2Inv.end();
  printf("Fp2Inv\t% 10.3fKclk\n", (clkFp2Inv.getClock() / K) / double(N));
  std::cout<<"Inv*Anv="<<Inverso2*Anverso2<<std::endl;
  {
  Xbyak::util::Clock clkFp2Inv;
  clkFp2Inv.begin();
  for (size_t i = 0; i < N; i++) {
    Fp2::neg(cc2,aa2);
  }
  clkFp2Inv.end();
  printf("Fp2neg\t% 10.3fKclk\n", (clkFp2Inv.getClock() / K) / double(N));
  }


  Fp6 cc6,aa6,bb6;
  aa6.a_=Q[0]; aa6.b_=Q[0]; aa6.c_=Q[0];
  bb6.a_=Q[1]; bb6.b_=Q[1]; bb6.c_=Q[1];
  Xbyak::util::Clock clkFp6Add;
  clkFp6Add.begin();
  for (size_t i = 0; i < N; i++) {
    //cc=aa*bb2;
    Fp6::add(cc6,aa6,bb6);
  }
  clkFp6Add.end();
  printf("Fp6Add\t% 10.3fKclk\n", (clkFp6Add.getClock() / K) / double(N));
  Xbyak::util::Clock clkFp6Mul;
  clkFp6Mul.begin();
  for (size_t i = 0; i < N; i++) {
    //cc=aa*bb2;
    Fp6::mul(cc6,aa6,bb6);
  }
  clkFp6Mul.end();
  printf("Fp6Mult\t% 10.3fKclk\n", (clkFp6Mul.getClock() / K) / double(N));
  Xbyak::util::Clock clkFp6Sqr;
  clkFp6Sqr.begin();
  for (size_t i = 0; i < N; i++) {
    //cc=aa*bb2;
    Fp6::square(cc6,aa6);
  }
  clkFp6Sqr.end();
  printf("Fp62Sqr\t% 10.3fKclk\n", (clkFp6Sqr.getClock() / K) / double(N));
  Fp6 Inverso6;
  Fp6 Anverso6; Anverso6.a_=Q[0]; Anverso6.b_=Q[0]; Anverso6.c_=Q[0];
  Xbyak::util::Clock clkFp6Inv;
  clkFp6Inv.begin();
  for (size_t i = 0; i < N; i++) {
    Inverso6=Anverso6;
    Inverso6.inverse();
  }
  clkFp6Inv.end();
  printf("Fp6Inv\t% 10.3fKclk\n", (clkFp6Inv.getClock() / K) / double(N));
  std::cout<<"Inv*Anv="<<Inverso6*Anverso6<<std::endl;


  Fp12 t12e1,t12e2,t12e3;
  t12e2.a_.a_=Q[0];
  t12e2.a_.b_=Q[0];
  t12e2.a_.c_=Q[0];
  t12e2.b_.a_=Q[1];
  t12e2.b_.b_=Q[1];
  t12e2.b_.c_=Q[1];
  Xbyak::util::Clock clkFp12Add;
  clkFp12Add.begin();
  for (size_t i = 0; i < N; i++) {
    //cc=aa*bb2;
    Fp12::add(t12e1,t12e2,t12e3);
  }
  clkFp12Add.end();
  printf("Fp12Add\t% 10.3fKclk\n", (clkFp12Add.getClock() / K) / double(N));
  Xbyak::util::Clock clkFp12Mult;
  clkFp12Mult.begin();
  for (size_t i = 0; i < N; i++) {
    //cc=aa*bb2;
    Fp12::mul(t12e1,t12e2,t12e3);
  }
  clkFp12Mult.end();
  printf("Fp12Mult\t% 10.3fKclk\n", (clkFp12Mult.getClock() / K) / double(N));
  Xbyak::util::Clock clkFp12SQR;
  clkFp12SQR.begin();
  for (size_t i = 0; i < N; i++) {
    t12e1=t12e2;
    Fp12::square(t12e1);
  }
  clkFp12SQR.end();
  printf("Fp12Sqr\t% 10.3fKclk\n", (clkFp12SQR.getClock() / K) / double(N));

  Xbyak::util::Clock clkInverseFp12;
  clkInverseFp12.begin();
  for (unsigned int i = 0; i < N; i++) {
    t12e3=t12e2;
    t12e3.inverse();
  }
  clkInverseFp12.end();
  printf("Inverse Fp12\t% 10.2fKclk\n", (clkInverseFp12.getClock() / K) / double(N));
  std::cout<<"Inv*Anv="<<t12e3*t12e2<<std::endl;

  Xbyak::util::Clock clkSpecialGS;
  clkSpecialGS.begin();
  for (unsigned int i = 0; i < N; i++) {
    t12e3.sqru();
  }
  clkSpecialGS.end();
  printf("Special Sqr GS\t% 10.2fKclk\n", (clkSpecialGS.getClock() / K) / double(N));

  Xbyak::util::Clock clkSpecialK;
  clkSpecialK.begin();
  for (unsigned int i = 0; i < N; i++) {
    Compress::fixed_power(t12e3, t12e2);
  }
  clkSpecialK.end();
  printf("Special Sqr K\t% 10.2fKclk\n", (clkSpecialK.getClock() / K) / double(N));






















  const int64_t t = -0x4080000000000001LL;//0x3fc0100000000000ULL;
  int i;
  int w=3;
  int m=4;
  int twotow_1=pow(2,w-1);

  const M_uint& p = M_param_p();
  const M_uint& r = M_param_r();
  M_uint tr; M_setStr(tr, "129607518034317099905336561907183648775");//tr.set("126611883464401272108868536818127077377");
  //M_uint cf=(p+1-tr)/r; // eq to 1, but left for compatibility // I mean, this is the BN curves.
  PUT(p);PUT(r);PUT(tr);PUT(t);//PUT(bn::ParamT<Fp2>::r_1);
  M_setStr(pm1o4, "12598581548261874213705603106673800432416890341079302386700286569554565398542"); //pm1o4.set("4007642258600782069439172071874662378909209525296084374944598245029055561728");

  for (i=0;i<256/m+1;i++) pow2i[i]=pow(2,i);
  for (i=0;i<m;i++) std::cout<<pow2i[i]<<","; std::cout<<std::endl;
  std::cout<<twotow_1<<std::endl;
  for (i=0;i<m;i++) pow2w_1i[i]=ipow2w_1i[i]=pow(twotow_1,i);
  for (i=0;i<m;i++) std::cout<<pow2w_1i[i]<<","; std::cout<<std::endl;

  //Get m-dimmensional expansion
  //Make precompute for Fp2
  //Precompute wNAF for every element in the expansion

  M_sint WB[4];
  M_sint BB[4][4];
  M_sint SB[2][2];
  M_sint W[2];
  M_sint BetaS;
  M_sint z; M_setStr(z, "-4647714815446351873");//z.set("4593689212103950336");
  //M_uint One_r; One_r.set("0x9366C48000000004F393800000000010A100000000000016");

  W[0]=6*z*z+4*z+1;      // This is first column of inverse of SB (without division by determinant)
	W[1]=-(2*z+1);
	SB[0][0]=6*z*z+2*z;
	SB[0][1]=-(2*z+1);
	SB[1][0]=-(2*z+1);
	SB[1][1]=-(6*z*z+4*z+1);
  BetaS=-(18*z*z*z+18*z*z+9*z+2);//.set("-1744846929583747973039745380539802239475828258251116380162");//18*z*z*z+18*z*z+9*z+1;
  PUT(BetaS);

  WB[0]=-(6*z*z+6*z+2);
  WB[1]=-(1);
  WB[2]=z;
  WB[3]=1+3*z+6*z*z+6*z*z*z;
  //std::cout<<"WB0=("<<WB[0]<<","<<WB[1]<<","<<WB[2]<<","<<WB[3]<<")"<<std::endl;

  BB[0][0]=2*z;   BB[0][1]=1+z;    BB[0][2]=-z;   BB[0][3]=z;
  BB[1][0]=1+z;   BB[1][1]=-1-3*z; BB[1][2]=-1+z; BB[1][3]=1;
  BB[2][0]=-1;    BB[2][1]=2+6*z;  BB[2][2]=2;    BB[2][3]=-1;
  BB[3][0]=2+6*z; BB[3][1]=1;      BB[3][2]=-1;   BB[3][3]=1;
  //std::cout<<"WB1=("<<WB[0]<<","<<WB[1]<<","<<WB[2]<<","<<WB[3]<<")"<<std::endl;

  //int i;
	for (i=0; i < 4; i++)
	{
		WB[i] <<= 256;
		WB[i] /= M_param_r();
	}
	for (i=0; i < 2; i++)
	{
		W[i] <<= 256;
		W[i] /= M_param_r();
	}

  Fp2 half=2; half.inverse();

  const Fp2 Q5[3] = {
    Fp2(Fp("11704862095684563340633177014105692338896570212191553344841646079297773588350"), Fp("8109660419091099176077386016058664786484443690836266670000859598224113683590")),
    Fp2(Fp("13675004707765291840926741134330657174325775842323897203730447415462283202661"), Fp("6686821593302402098300348923000142044418144174058402521315042066378362212321")),
    Fp2(Fp(1), Fp(0)),
  };
  PUT(bn::ecop::isOnTwistECJac3(Q5));
  Fp2 Q5A[2];Q5A[0]=Q5[0];Q5A[1]=Q5[1];
  Fp2 QsI[4][PRECOMPUTEknown][2];
  precomputeG2known(QsI,Q5A);


  //*
  /////////////////////////////
  // G1 tests
  /////////////////////////////
  Fp R1[3],R2[3],R3[3]; R1[2]=R2[2]=1;
  Fp tmp[3];
#if 1
  Fp P1[3];
  std::string stServer="Bob esponja.";
  const char *cServer=stServer.c_str();

  Fp x0; H1(x0,cServer);
  x0=x0*x0*x0; x0=x0+2;
  
  M_uint v0;
  v0 = M_fromFp(x0);
  M_uint p0 = M_param_p();
  PUT(jack(v0,p0));
  PUT(x0); PUT(p0);
  hash_and_mapB(P1[0],P1[1],cServer); P1[2]=1;
  PUT(bn::ecop::isOnECJac3(P1));



  tmp[0]=P1[0]; tmp[1]=P1[1]; tmp[2]=P1[2];
  Vk=rg.get();//.set(rand(Br).getbig()->w,4);

  bn::ecop::ScalarMult(R1,P1,Vk);
  std::cout<<"[k]P:"<<R1[0]<<","<<R1[1]<<","<<R1[2]<<std::endl;

  { // QQQ
    M_uint tmp = r - Vk;
    bn::ecop::ScalarMult(R2,P1,tmp);
  }
  std::cout<<"[r-k]P:"<<R2[0]<<","<<R2[1]<<","<<R2[2]<<std::endl;

  bn::ecop::ECAdd(R3,R1,R2);
  std::cout<<"[k]P+[r-k]P:"<<R3[0]<<","<<R3[1]<<","<<R3[2]<<std::endl;



  std::cout<<"Quietly testing G1"<<std::endl;
  tmp[0]=P1[0]; tmp[1]=P1[1]; tmp[2]=P1[2];
  std::cout<<"G1 U"<<std::endl;
  int acumG1=0;
  for (i=0;i<1000;i++){
    //std::cout<<i<<std::endl;
    Vk=rg.get();//.set(rand(Br).getbig()->w,4);
    G1_mul(R1,Vk,tmp,W,SB,BetaS);
    bn::ecop::NormalizeJac(R1,R1);
    bn::ecop::ScalarMult(R2,tmp,Vk);
    bn::ecop::NormalizeJac(R2,R2);
    TEST_EQUAL(R1[0],R2[0]);
    if (R1[0]!=R2[0]) {
      acumG1++;
      M_sint u[2];
      DecompG1(u,Vk,W,SB);
      for (int ii=0;ii<2;ii++) std::cout<<u[ii]<<","; std::cout<<std::endl; std::cout<<Vk<<std::endl;
      std::cout<<"P("<<P1[0]<<","<<P1[1]<<")"<<std::endl;
    }
    //TEST_EQUAL(R1[1],R2[1]);
    //TEST_EQUAL(R1[2],R2[2]);
  }
  std::cout<<"acum="<<acumG1<<std::endl;
  std::cout<<"G1_mul.1Core:\n"<<R1[0]<<","<<R1[1]<<","<<R1[2]<<std::endl;
  std::cout<<"ScalarMult.1Core.Vk2:\n"<<R2[0]<<","<<R2[1]<<","<<R2[2]<<std::endl;


  //*/


  PUT(bn::ecop::isOnTwistECJac3(Q5));
  Fp2 S1[3],S2[3],S3[3];
  Vk=rg.get();//.set(rand(Br).getbig()->w,4);

  bn::ecop::ScalarMult(S1,Q5,Vk);
  std::cout<<"[k]P:"<<S1[0]<<","<<S1[1]<<","<<S1[2]<<std::endl;

  { // QQQ
    M_uint tmp = r - Vk;
    bn::ecop::ScalarMult(S2,Q5,tmp);
  }
  std::cout<<"[r-k]P:"<<S2[0]<<","<<S2[1]<<","<<S2[2]<<std::endl;

  bn::ecop::ECAdd(S3,S1,S2);
  std::cout<<"[k]P+[r-k]P:"<<S3[0]<<","<<S3[1]<<","<<S3[2]<<std::endl;

  /*
  /////////////////////////////
  // G2 tests
  /////////////////////////////
  std::cout<<"Quietly testing G2"<<std::endl;
  int acumG2=0; int flag1=-1; int flag2=-1;
  for (i=0;i<1000;i++){
    Vk=rg.get();//.set(rand(Br).getbig()->w,4);
    //Vk=r1;
    G2_mulknown(lQ5,Vk,WB,BB,QsI);
    bn::ecop::NormalizeJac(lQ5,lQ5);

    bn::ecop::ScalarMult(lQ5b,Q5,Vk);
    bn::ecop::NormalizeJac(lQ5b,lQ5b);
    //std::cout<<lQ5[0]-lQ5b[0]<<std::endl;
    TEST_EQUAL(lQ5[0],lQ5b[0]);
    //TEST_EQUAL(lQ5[1],lQ5b[1]);
    //TEST_EQUAL(lQ5[2],lQ5b[2]);
    if ((lQ5[0]-lQ5b[0])!=0) {std::cout<<"Known:"<<i<<"."<<Vk<<" :("<<std::endl; flag1=i;}

    G2_mul(lQ5b,Q5A,Vk,WB,BB);
    bn::ecop::NormalizeJac(lQ5b,lQ5b);
    TEST_EQUAL(lQ5[0],lQ5b[0]);
    //TEST_EQUAL(lQ5[1],lQ5b[1]);
    //TEST_EQUAL(lQ5[2],lQ5b[2]);
    if ((lQ5[0]-lQ5b[0])!=0) {std::cout<<"Unknown:"<<i<<"."<<Vk<<" :("<<std::endl; flag2=i;
      PUT(lQ5[1]+lQ5b[1]);
    }
    if (flag1==i) {acumG2++;} if (flag2==i) {acumG2++;} if ((flag1==flag2)&&(flag1==i)) acumG2--;
  }
  std::cout<<"acum="<<acumG2<<std::endl;
  std::cout<<"end of test"<<std::endl;
  std::cout<<std::endl;
  //*/

  Fp kP1[3];
  Fp12 e3;
  Fp12 e3a;
  //Fp2 Q6A[2],kQ6A[2];
  M_setStr(Vk, "0x10FF39ACDF87D66262C0B47C92273083E91FCDF811230B65F1BF6003DFB46368");



  //*
  Fp12 e1,e2,e4;

  Fp2 P6;
  Fp2 kP6;
  Fp2 Q6[3];
  Fp P6b[2];
  Fp kP6b[2];
  Fp2 kQ6[3];

  Fp2 k2Q6[3];
  Fp2 k3Q6[3];
  /////////////////////////////
  // GT tests
  /////////////////////////////
  std::cout<<"Quietly testing GT"<<std::endl;
  int acum;
  M_uint Vk2;
  Fp12 feB[4][PRECOMPUTEknown]; Fp12 feB2[4][PRECOMPUTE];
  Fp12 fB;
  for (i=0,acum=0;i<1;i++) {
    Vk=rg.get();//.set(rand(Br).getbig()->w,4);
    //P1[0]=1; P1[1].set("6002591013793082186349685741340126608890177833573736884572962620906403581104");P1[2]=1;
    ScalarMult(kP1,P1,Vk); bn::ecop::NormalizeJac(kP1,kP1);
    P6.a_=P1[0];P6.b_=P1[1]; kP6.a_=kP1[0]; kP6.b_=kP1[1];
    Q6[0]=Q5[0];Q6[1]=Q5[1];Q6[2]=1;
    ScalarMult(kQ6,Q6,Vk); bn::ecop::NormalizeJac(kQ6,kQ6);

    kP6b[0]=kP6.a_; kP6b[1]=kP6.b_;
    P6b[0]=P6.a_; P6b[1]=P6.b_;
    bn::opt_atePairing<Fp>(e1, Q6, kP6b);
    bn::opt_atePairing<Fp>(e2, kQ6, P6b);
    bn::opt_atePairing<Fp>(e3a, Q6, P6b);
    GT_expo(e3,e3a,Vk,WB,BB);
    e4=power(e3a,Vk);
    TEST_EQUAL(e1.a_.a_.a_,e2.a_.a_.a_);//TEST_EQUAL(e2.a_.a_.a_,e3.a_.a_.a_);
    TEST_EQUAL(e3.a_.a_.a_,e4.a_.a_.a_);
    //*
    if (e3.a_.a_.a_-e4.a_.a_.a_!=0) {
      precomputeGTknown(feB,e3a);
      GT_expoknown(fB,feB,Vk,WB,BB);
      PUT(fB.a_.a_.a_);
      PUT(e1-e3);PUT(fB-e2);
      std::cout<<"e[1]:=Fp!"<<e3a.a_.a_.a_<<";"<<std::endl;
      std::cout<<"e[2]:=Fp!"<<e3a.a_.a_.b_<<";"<<std::endl;

      std::cout<<"e[3]:=Fp!"<<e3a.a_.b_.a_<<";"<<std::endl;
      std::cout<<"e[4]:=Fp!"<<e3a.a_.b_.b_<<";"<<std::endl;

      std::cout<<"e[5]:=Fp!"<<e3a.a_.c_.a_<<";"<<std::endl;
      std::cout<<"e[6]:=Fp!"<<e3a.a_.c_.b_<<";"<<std::endl;

      std::cout<<"e[7]:=Fp!"<<e3a.b_.a_.a_<<";"<<std::endl;
      std::cout<<"e[8]:=Fp!"<<e3a.b_.a_.b_<<";"<<std::endl;

      std::cout<<"e[9]:=Fp!"<<e3a.b_.b_.a_<<";"<<std::endl;
      std::cout<<"e[10]:=Fp!"<<e3a.b_.b_.b_<<";"<<std::endl;

      std::cout<<"e[11]:=Fp!"<<e3a.b_.c_.a_<<";"<<std::endl;
      std::cout<<"e[12]:=Fp!"<<e3a.b_.c_.b_<<";"<<std::endl;
      e4=power(e3a,r);
      PUT(e4);
      PUT(p);
      PUT(r);
      PUT(Vk);
      M_sint u[4];
      DecompG2(u,Vk,WB,BB);for(int ii=0;ii<4;ii++) std::cout<<u[ii]<<","; std::cout<<std::endl;
      int v[8][70];
      int tlen[4],tlent=0;
      NAFw2(v[0],v[1],tlen[0],M_abs(u[0]),WNAF); if (tlen[0]>tlent) tlent=tlen[0];
      NAFw2(v[2],v[3],tlen[1],M_abs(u[1]),WNAF); if (tlen[1]>tlent) tlent=tlen[1];
      NAFw2(v[4],v[5],tlen[2],M_abs(u[2]),WNAF); if (tlen[2]>tlent) tlent=tlen[2];
      NAFw2(v[6],v[7],tlen[3],M_abs(u[3]),WNAF); if (tlen[3]>tlent) tlent=tlen[3];
      for (i=0;i<8;i++) {std::cout<<"v_["<<i+1<<"]:=[";for (int j=0;j<tlen[i/2];j++) {std::cout<<v[i][j]; if (j+1<tlen[i/2]) std::cout<<",";}std::cout<<"];\n";}
      precomputeGT(feB2,e3a);
      for (int ii=0;ii<4;ii++)
        for (int jj=0;jj<2;jj++) {
          std::cout<<ii<<","<<jj<<":"<<feB[ii][jj]-feB2[ii][jj]<<std::endl;
        }
      GT_expo(e3,e3a,Vk,WB,BB);
      TEST_EQUAL(e3.a_.a_.a_,e1.a_.a_.a_);
      NAFw2(v[0],v[1],tlen[0],M_abs(u[0]),WNAFknown); if (tlen[0]>tlent) tlent=tlen[0];
      NAFw2(v[2],v[3],tlen[1],M_abs(u[1]),WNAFknown); if (tlen[1]>tlent) tlent=tlen[1];
      NAFw2(v[4],v[5],tlen[2],M_abs(u[2]),WNAFknown); if (tlen[2]>tlent) tlent=tlen[2];
      NAFw2(v[6],v[7],tlen[3],M_abs(u[3]),WNAFknown); if (tlen[3]>tlent) tlent=tlen[3];
      for (i=0;i<8;i++) {std::cout<<"v_["<<i+1<<"]:=[";for (int j=0;j<tlen[i/2];j++) {std::cout<<v[i][j]; if (j+1<tlen[i/2]) std::cout<<",";}std::cout<<"];\n";}
      acum++; //return 0;
      }
    //*/
    Vk2=rg.get();//.set(rand(Br).getbig()->w,4);
    ScalarMult(k2Q6,Q6,Vk2); bn::ecop::NormalizeJac(k2Q6,k2Q6);
    bn::opt_atePairing<Fp>(e1, kQ6, P6b);
    bn::opt_atePairing<Fp>(e2, k2Q6, P6b);
    Fp12::mul(e3,e1,e2);
    ECAdd(k3Q6,kQ6,k2Q6); bn::ecop::NormalizeJac(k3Q6,k3Q6);
    bn::opt_atePairing<Fp>(e4, k3Q6, P6b);
    TEST_EQUAL(e3.a_.a_.a_,e4.a_.a_.a_);
  }
  PUT(Fp12(e3-e4).isZero());
  PUT(e3.a_.a_.a_);
  PUT(e4.a_.a_.a_);
  PUT(Vk);
  PUT(acum);
  //*/

  Fp12 fe1,fe2,fe3;
  Vk=rg.get();//.set(rand(Br).getbig()->w,4);

  fe1=power(e4,Vk);
  std::cout<<"e^(k):"<<fe1.a_.a_.a_<<std::endl;
  M_uint r_k; r_k=r-Vk;
  fe2=power(e4,r_k);
  std::cout<<"e^(r-k):"<<fe2.a_.a_.a_<<std::endl;

  Fp12::mul(fe3,fe1,fe2);
  std::cout<<"e^(k).e^(r-k):"<<fe3<<std::endl;
//  return 0;


  Fp P[3];
  Fp PsI[2][PRECOMPUTEknown][2];
  Fp Pa[2];
  Fp Pb[2];
  P[0].set("13444485882265322272857095018562747159513775856924555608551532122942502696033");
  P[1].set("11811997307285544251176681325629039017467625014708607213193666012921830535998");P[2]=1;

  /////////////////////////////
  // Some Benchmarks
  /////////////////////////////
  //*
  Xbyak::util::Clock clkprecomputeG1;
  //Fp PsI[2][PRECOMPUTEknown][2];
  //Fp Pa[2];
  //Fp Pb[2];
  bn::ecop::NormalizeJac(P,P);
  Pa[0]=P[0];Pa[1]=P[1];
  Pb[0]=P[0];Pb[1]=P[1];
  Fp::mul(Pb[0],Pb[0],M_toFp(M_abs(BetaS))); Fp::neg(Pb[0],Pb[0]);//Beta is negative
  clkprecomputeG1.begin();
  for (size_t ii = 0; ii < N2; ii++) {
    precomputeG1known(PsI,Pa,BetaS);
  }
  clkprecomputeG1.end();
  printf("precomputeG1\t% 10.2fKclk\n", (clkprecomputeG1.getClock() / K) / double(N2));

  M_sint u1[2];
  Fp PsJ[4][PRECOMPUTE][2];
  Xbyak::util::Clock clkG1precompute;
  clkG1precompute.begin();
  for (size_t i = 0; i < N; i++) {
    precomputeG1(PsJ,kP1,kP1,u1);
  }
  clkG1precompute.end();
  printf("G1precompute\t% 10.2fKclk\n", (clkG1precompute.getClock() / K) / double(N));
  Xbyak::util::Clock clkG1precomputeb;
  clkG1precomputeb.begin();
  for (size_t i = 0; i < N; i++) {
    precomputeG1b(PsJ,kP1,BetaS);
  }
  clkG1precomputeb.end();
  printf("G1precomputeb\t% 10.2fKclk\n", (clkG1precomputeb.getClock() / K) / double(N));

  Vk=rg.get();//.set(rand(Br).getbig()->w,4);
  Xbyak::util::Clock clkG1mulK_1core;
  clkG1mulK_1core.begin();
  for (size_t ii = 0; ii < N2; ii++) {
    G1_mulknown(R1,Vk,PsI,W,SB);
  }
  clkG1mulK_1core.end();
  printf("G1mulK_1core\t% 10.2fKclk\n", (clkG1mulK_1core.getClock() / K) / double(N2));
  Xbyak::util::Clock clkG1mulU_1core;
  clkG1mulU_1core.begin();
  for (size_t ii = 0; ii < N2; ii++) {
    G1_mul(R1,Vk,P,W,SB,BetaS);
  }
  clkG1mulU_1core.end();
  printf("G1mulU_1core\t% 10.2fKclk\n", (clkG1mulU_1core.getClock() / K) / double(N2));

  Xbyak::util::Clock clkG2mulK_1core;
  clkG2mulK_1core.begin();
  for (size_t ii = 0; ii < N2; ii++) {
    G2_mulknown(lQ5,Vk,WB,BB,QsI);
  }
  clkG2mulK_1core.end();
  printf("G2mulK_1core\t% 10.2fKclk\n", (clkG2mulK_1core.getClock() / K) / double(N2));

  Xbyak::util::Clock clkG2mulU_1core;
  clkG2mulU_1core.begin();
  for (size_t ii = 0; ii < N2; ii++) {
    G2_mul(lQ5b,Q5A,Vk,WB,BB);
  }
  clkG2mulU_1core.end();
  printf("G2mulU_1core\t% 10.2fKclk\n", (clkG2mulU_1core.getClock() / K) / double(N2));


  //Fp12 feB[4][PRECOMPUTEknown];
  //Fp12 fB;

  M_setStr(Vk, "9460739465784705992616833317629855351139894408403962129930038371011463863840");
  Xbyak::util::Clock clkGTexpoK_1core;
  clkGTexpoK_1core.begin();
  for (size_t ii = 0; ii < N2; ii++) {
    GT_expoknown(fB,feB,Vk,WB,BB);
  }
  clkGTexpoK_1core.end();

  precomputeGTknown(feB,e3a);
  printf("GTexpoK_1core\t% 10.2fKclk\n", (clkGTexpoK_1core.getClock() / K) / double(N2));
  GT_expoknown(fB,feB,Vk,WB,BB);
  GT_expo(e3,e3a,Vk,WB,BB);
  e4=power(e3a,Vk);
  TEST_EQUAL(fB.a_.a_.a_,e3.a_.a_.a_);
  TEST_EQUAL(e3.a_.a_.a_,e4.a_.a_.a_);
  PUT(fB.a_.a_.a_-e4.a_.a_.a_);

  M_sint u4[4];
  DecompG2(u4,Vk,WB,BB);
  for (i=0;i<4;i++) std::cout<<u4[i]<<","; std::cout<<std::endl;

#if 0
  int v[8][70];
  int tlen[4],tlent=0;

  precomputeGTknown(feB,e3a);
  Fp12 feC[4][PRECOMPUTE];
  precomputeGT(feC,e3a);
  std::cout<<"<<";
  for (i=0;i<4;i++)
    for (int j=0;j<PRECOMPUTE;j++)
      std::cout<<feB[i][j].a_.a_.a_-feC[i][j].a_.a_.a_;
  std::cout<<">>"<<std::endl;

  NAFw2(v[0],v[1],tlen[0],u4[0].get(),WNAFknown); if (tlen[0]>tlent) tlent=tlen[0];
  NAFw2(v[2],v[3],tlen[1],u4[1].get(),WNAFknown); if (tlen[1]>tlent) tlent=tlen[1];
  NAFw2(v[4],v[5],tlen[2],u4[2].get(),WNAFknown); if (tlen[2]>tlent) tlent=tlen[2];
  NAFw2(v[6],v[7],tlen[3],u4[3].get(),WNAFknown); if (tlen[3]>tlent) tlent=tlen[3];
  for (i=0;i<8;i++) {for (int j=0;j<70;j++) std::cout<<v[i][j]<<",";std::cout<<"\n";}
  for (i=0;i<8;i++)for (int j=0;j<70;j++) v[i][j]=0;

  NAFw2(v[0],v[1],tlen[0],u4[0].get(),WNAF); if (tlen[0]>tlent) tlent=tlen[0];
  NAFw2(v[2],v[3],tlen[1],u4[1].get(),WNAF); if (tlen[1]>tlent) tlent=tlen[1];
  NAFw2(v[4],v[5],tlen[2],u4[2].get(),WNAF); if (tlen[2]>tlent) tlent=tlen[2];
  NAFw2(v[6],v[7],tlen[3],u4[3].get(),WNAF); if (tlen[3]>tlent) tlent=tlen[3];
  for (i=0;i<8;i++) {for (int j=0;j<70;j++) std::cout<<v[i][j]<<",";std::cout<<"\n";}
#endif

  Xbyak::util::Clock clkGTexpo_1core;
  clkGTexpo_1core.begin();
  for (size_t ii = 0; ii < N2; ii++) {
    GT_expo(e3,e3a,Vk,WB,BB);
  }
  clkGTexpo_1core.end();
  printf("GTexpoU_1core\t% 10.2fKclk\n", (clkGTexpo_1core.getClock() / K) / double(N2));

  std::cout<<std::endl;
  //*/


  Xbyak::util::Clock clkDecompG1;
  M_sint u[2];
  clkDecompG1.begin();
  for (size_t ii = 0; ii < N2; ii++) {
    DecompG1(u,Vk,W,SB);
  }
  clkDecompG1.end();
  printf("DecompG1\t% 10.2fKclk\n", (clkDecompG1.getClock() / K) / double(N2));
  Xbyak::util::Clock clkDecompG2;
  M_sint u0[4];
  clkDecompG2.begin();
  for (size_t ii = 0; ii < N2; ii++) {
    DecompG2(u0,Vk,WB,BB);
  }
  clkDecompG2.end();
  printf("DecompG2\t% 10.2fKclk\n", (clkDecompG2.getClock() / K) / double(N2));
  Xbyak::util::Clock clkmad;
  clkmad.begin();
  for (size_t ii = 0; ii < N2; ii++) {
    mad(u[0],W[0],Vk);
  }
  clkmad.end();
  printf("mad\t% 10.2fKclk\n", (clkmad.getClock() / K) / double(N2));
  clkmad.begin();
  for (size_t ii = 0; ii < N2; ii++) {
    mad(u[0],WB[0],Vk);
  }
  clkmad.end();
  printf("mad\t% 10.2fKclk\n", (clkmad.getClock() / K) / double(N2));


  int ikp[70],ikn[70],itlen=256;
  /*
  Vk.set(rand(Br).getbig()->w,4);
  Xbyak::util::Clock clkNAFw;
  clkNAFw.begin();
  for (size_t i = 0; i < N; i++) {
    NAFw2(ikp,ikn,itlen,Vk,WNAF);
  }
  clkNAFw.end();
  printf("NAFw=%d,256-bits\t% 10.2fKclk\n", WNAF, (clkNAFw.getClock() / K) / double(N));
  Xbyak::util::Clock clkNAFwb;
  clkNAFwb.begin();
  for (size_t i = 0; i < N; i++) {
    NAFw2(ikp,ikn,itlen,Vk,WNAFknown);
  }
  clkNAFwb.end();
  printf("NAFw=%d,256-bits\t% 10.2fKclk\n", WNAFknown, (clkNAFwb.getClock() / K) / double(N));
  //*/

  itlen=70;
  //std::string s="18446744073709551616";
  //char *cstr64 = new char [s.size()+1];
  //strcpy(cstr64,s.c_str());
  //Big Dos_64(cstr64);
  //delete [] cstr64;
  //Vk.set(rand(Dos_64).getbig()->w,4);
  Vk2=rg.get(); Vk = M_block(Vk2, 0);
  Xbyak::util::Clock clkNAFw2;
  clkNAFw2.begin();
  for (size_t i = 0; i < N; i++) {
    NAFw64(ikp,ikn,itlen,Vk,WNAF);
  }
  clkNAFw2.end();
  printf("NAFw=%d,64-bits\t% 10.2fKclk\n", WNAF, (clkNAFw2.getClock() / K) / double(N));
  //std::cout<<"loop="<<itlen<<std::endl;
  //for (int ii=0;ii<itlen;ii++) std::cout<<ikp[ii]<<","; std::cout<<std::endl; for (int ii=0;ii<itlen;ii++) std::cout<<ikn[ii]<<","; std::cout<<std::endl;
  Xbyak::util::Clock clkNAFw2b;
  clkNAFw2b.begin();
  for (size_t i = 0; i < N; i++) {
    NAFw64(ikp,ikn,itlen,Vk,WNAFknown);
  }
  clkNAFw2b.end();
  printf("NAFw=%d,64-bits\t% 10.2fKclk\n", WNAFknown, (clkNAFw2b.getClock() / K) / double(N));
  //for (int ii=0;ii<itlen;ii++) std::cout<<ikp[ii]<<","; std::cout<<std::endl; for (int ii=0;ii<itlen;ii++) std::cout<<ikn[ii]<<","; std::cout<<std::endl;
  /*
  int ikp3[66],ikn3[66];
  Xbyak::util::Clock clkNAFw3;
  clkNAFw3.begin();
  for (size_t i = 0; i < N; i++) {
    NAFw3(ikp3,ikn3,itlen,Vk,WNAF);
  }
  clkNAFw3.end();
  printf("NAFw3=%d,64-bits\t% 10.2fKclk\n", WNAF, (clkNAFw3.getClock() / K) / double(N));
  //*/

  //s="340282366920938463463374607431768211456";
  //char *cstr128 = new char [s.size()+1];
  //strcpy(cstr128,s.c_str());
  //Big Dos_128=cstr128;
  //delete [] cstr128;
  //Vk.set(rand(Dos_128).getbig()->w,4);
  Vk=rg.get(); Vk >>= 128; //M_uint::shrUnit(Vk,Vk,2);
  itlen=130;
  Xbyak::util::Clock clkNAFw2128;
  clkNAFw2128.begin();
  for (size_t i = 0; i < N; i++) {
    NAFw128(ikp,ikn,itlen,Vk,WNAFknown);
  }
  clkNAFw2128.end();
  printf("NAFw=%d,128-bits\t% 10.2fKclk\n", WNAFknown,(clkNAFw2128.getClock() / K) / double(N));

  itlen=130;
  Xbyak::util::Clock clkNAFw2128b;
  clkNAFw2128b.begin();
  for (size_t i = 0; i < N; i++) {
    NAFw128(ikp,ikn,itlen,Vk,WNAF);
  }
  clkNAFw2128b.end();
  printf("NAFw=%d,128-bits\t% 10.2fKclk\n", WNAF,(clkNAFw2128b.getClock() / K) / double(N));

  /*
  Xbyak::util::Clock clkG1JJA;
  clkG1JJA.begin();
  for (size_t i = 0; i < N; i++) {
    addJJAG1(R1,kP1,Pa);
  }
  clkG1JJA.end();
  printf("G1 Add JJA\t% 10.2fKclk\n", (clkG1JJA.getClock() / K) / double(N));
  //*/

  Xbyak::util::Clock clkG1JJJ;
  clkG1JJJ.begin();
  for (size_t i = 0; i < N; i++) {
        addJJAG1b(R1,kP1,Pa);
  }
  clkG1JJJ.end();
  printf("G1 Add JJA\t% 10.2fKclk\n", (clkG1JJJ.getClock() / K) / double(N));

  Xbyak::util::Clock clkG1dbl;
  clkG1dbl.begin();
  for (size_t i = 0; i < N; i++) {
    bn::ecop::ECDouble(R1,kP1);
  }
  clkG1dbl.end();
  printf("G1 Dbl A\t% 10.2fKclk\n", (clkG1dbl.getClock() / K) / double(N));

  Xbyak::util::Clock clkG1dblJ;
  clkG1dblJ.begin();
  for (size_t i = 0; i < N; i++) {
    dblJG1(kP1,kP1);
  }
  clkG1dblJ.end();
  printf("G1 Dbl J\t% 10.2fKclk\n", (clkG1dblJ.getClock() / K) / double(N));

  Xbyak::util::Clock clkNormaliseG1;
  clkNormaliseG1.begin();
  for (unsigned int i = 0; i < N; i++) {
    bn::ecop::NormalizeJac(P1,kP1);
  }
  clkNormaliseG1.end();
  printf("NormaliseG1\t% 10.2fKclk\n", (clkNormaliseG1.getClock() / K) / double(N));

  //*

  Fp2 QsJ[4][PRECOMPUTE][2];
  Xbyak::util::Clock clkG2precompute;
  clkG2precompute.begin();
  for (size_t i = 0; i < N; i++) {
    precomputeG2(QsJ,Q5A,u0);
  }
  clkG2precompute.end();
  printf("G2precompute\t% 10.2fKclk\n", (clkG2precompute.getClock() / K) / double(N));
  {
  Xbyak::util::Clock clkG2precompute;
  clkG2precompute.begin();
  for (size_t i = 0; i < N; i++) {
    precomputeG2b(QsJ,Q5A,u0);
  }
  clkG2precompute.end();
  printf("G2precomputeb\t% 10.2fKclk\n", (clkG2precompute.getClock() / K) / double(N));
  }



  Fp2 Q0[3];Q0[0]=Q[0];Q0[1]=Q[1];Q0[2]=Q[2];
  Q0[0]=Fp2(Fp("6223258573959374160388148138515068413463808374382904056166846479839437283179"),Fp("1333677181606561741263147618884774023879777504350792897443644297132133116760"));
  Q0[1]=Fp2(Fp("15448069458242091293797671350465853383868332221085553043065407654696213272343"),Fp("5988972321581320818364157621890995344611715545763882542498818720594534755038"));
  Q0[2]=Fp2(Fp("1"),Fp("0"));

  Fp2 Q3[]= {Fp2(Fp("16078745797517204416536047345108963426180929352523687780879944136677887850920"), Fp("13741732381062839667309601883674719699992528991173099693690893730898121669377")),
             Fp2(Fp("1000111743260271299562572635045641989113444435201231117183696796510031419626"), Fp("8709084883629411557146433485681162012476118870656559311612969796519071901272")),
             Fp2(Fp("1"), Fp("0"))};

  Fp2 Q2[3] = {Fp2(Fp("0"), Fp("0")), Fp2(Fp("0") , Fp("0")), Fp2(Fp("1"),Fp("0"))};
  Fp2 Q3p[] = {Q3[0], Q3[1], Q3[2]};

  Xbyak::util::Clock clkAddJJAG2;
  clkAddJJAG2.begin();
  for (size_t i = 0; i < N; i++) {
    addJJAG2c(Q2,Q0,Q3p);
  }
  clkAddJJAG2.end();
  printf("G2 Add JJA\t% 10.2fKclk\n", (clkAddJJAG2.getClock() / K) / double(N));
  TEST_ASSERT(bn::ecop::isOnTwistECJac3(Q2));

  Xbyak::util::Clock clkAddJJJG2;
  clkAddJJJG2.begin();
  for (size_t i = 0; i < N; i++) {
    addJJJG2(Q2,Q0,Q3p);
  }
  clkAddJJJG2.end();
  printf("G2 Add JJJ\t% 10.2fKclk\n", (clkAddJJJG2.getClock() / K) / double(N));
  TEST_ASSERT(bn::ecop::isOnTwistECJac3(Q2));

  Fp2 Q1[3],Q4[3]; Fp2 Q4_[3],Q0_[3];
  Q4[0]=Q3[0]; Q4[1]=Q3[1]; Q4[2]=1;

  Vk=rg.get();//.set(rand(Br).getbig()->w,4);
  ScalarMult(Q0_,Q0,Vk);
  std::cout<<"Q0_:"<<Q0_[0]<<","<<Q0_[1]<<","<<Q0_[2]<<std::endl;
  Vk=rg.get();//.set(rand(Br).getbig()->w,4);
  ScalarMult(Q4_,Q4,Vk);
  std::cout<<"Q4_:"<<Q4_[0]<<","<<Q4_[1]<<","<<Q4_[2]<<std::endl;

  PUT(bn::ecop::isOnTwistECJac3(Q0_));
  PUT(bn::ecop::isOnTwistECJac3(Q4_));

  addJJJG2(Q2,Q0_,Q4_);
  //bn::ecop::NormalizeJac(Q2,Q2);
  std::cout<<Q2[0]<<","<<Q2[1]<<","<<Q2[2]<<std::endl;
  bn::ecop::ECAdd(Q1,Q0_,Q4_);
  //bn::ecop::NormalizeJac(Q1,Q1);
  std::cout<<Q1[0]<<","<<Q1[1]<<","<<Q1[2]<<std::endl;

  /*
  Xbyak::util::Clock clkdblG2;
  clkdblG2.begin();
  for (size_t i = 0; i < N; i++) {
    dblJG2(Q2,Q2);
  }
  clkdblG2.end();
  printf("G2 Dbl J\t% 10.2fKclk\n", (clkdblG2.getClock() / K) / double(N));
  */

  Xbyak::util::Clock clkdblAG2;
  clkdblAG2.begin();
  for (size_t i = 0; i < N; i++) {
    bn::ecop::ECDouble(Q2,Q0);
  }
  clkdblAG2.end();
  printf("G2 Dbl A\t% 10.2fKclk\n", (clkdblAG2.getClock() / K) / double(N));
  TEST_ASSERT(bn::ecop::isOnTwistECJac3(Q2));


  Fp12 feA[4][PRECOMPUTE];
  Xbyak::util::Clock clkGTprecompute;
  clkGTprecompute.begin();
  for (size_t i = 0; i < N; i++) {
    precomputeGT(feA,e3a);
  }
  clkGTprecompute.end();
  printf("GTprecompute\t% 10.2fKclk\n", (clkGTprecompute.getClock() / K) / double(N));







/////////////////////////////
// Multipairing
/////////////////////////////

	Fp2 Qm[8][2];
	Fp2 Qn[8][3];
	Fp2 Qo[3];
	Fp2 Qp[8][3];
	Fp Pm[8][2];
	Fp Pn[8][3];
	Fp Po[8][2];
	Fp Pp[8][3];
	Fp Poo[3];
	M_uint ai[8],bi[8];
	std::string stPunto="";
	const char *cPunto;

	for (int j=0;j<8;j++) {ai[j]=bi[j]=1;}
	std::cout<<std::endl;
	std::cout<<"Generating points in G2"<<std::endl;

	Fp2 Fp2tmp[2], Fp2tmp3[3];

	for (i = 0; i < 8; i++)
	{
		Vk=rg.get();
		stPunto=M_toStr(Vk);
		cPunto=stPunto.c_str();
		hash_and_map2(Fp2tmp[0], Fp2tmp[1], cPunto);
		HashtoG2(Fp2tmp3, Fp2tmp);

		Qp[i][0] = Fp2tmp3[0];
		Qp[i][1] = Fp2tmp3[1];
		Qp[i][2] = Fp2tmp3[2];

		Vk = rg.get();
		ai[i] = Vk;
		//bn::ecop::ScalarMult(Qn[i], Qp[i], Vk);
		G2_mul(Qn[i], Qp[i], Vk, WB, BB);
	}

 	std::cout<<"isOnTwistECJac3:";
	for (i = 0; i < 8; i++)
		std::cout<<bn::ecop::isOnTwistECJac3(Qn[i]);
	std::cout<<std::endl;

	std::cout<<"Testing MultiNormalizeFp2:";
	bn::MultiNormalizeFp2(Qm,Qn,8);
	for (i = 0; i < 8; i++)
	{
		bn::ecop::NormalizeJac(Qo,Qn[i]);
		std::cout<<Fp2(Qm[i][0]-Qo[0]).isZero();
		std::cout<<Fp2(Qm[i][1]-Qo[1]).isZero();
	}
	std::cout<<std::endl;


	std::cout<<std::endl;
	std::cout<<"Generating points in G1"<<std::endl;
	std::cout<<"isOnECJac3:";
	for (i = 0; i < 8; i++)
	{
		Vk=rg.get();
		stPunto=M_toStr(Vk);
		cPunto=stPunto.c_str();
		hash_and_mapB(Pn[i][0],Pn[i][1],cPunto);
		Pn[i][2] = Fp(1);
		
		Vk=rg.get();
		bi[i]=Vk;
		Pp[i][0] = Pn[i][0];
		Pp[i][1] = Pn[i][1];
		Pp[i][2] = Pn[i][2];

		bn::ecop::ScalarMult(Poo, Pn[i], Vk);
		Pn[i][0] = Poo[0];
		Pn[i][1] = Poo[1];
		Pn[i][2] = Poo[2];
		std::cout<<bn::ecop::isOnECJac3(Pn[i]);
	}
	std::cout<<std::endl;

	std::cout<<"Testing MultiNormalizeFp:";
	bn::MultiNormalizeFp(Pm, Pn, 8);
	for (i = 0; i < 8; i++)
	{
		bn::ecop::NormalizeJac(Po[i],Pn[i]);
		std::cout<<Fp(Pm[i][0]-Po[i][0]).isZero();
		std::cout<<Fp(Pm[i][1]-Po[i][1]).isZero();
	}
	std::cout<<std::endl;


	std::cout<<std::endl;
	std::cout<<"Testing Multipairing"<<std::endl;
	e3 = Fp12(1);
	for (i = 0; i < 8; i++)
	{
		bn::opt_atePairing<Fp>(e1, Qm[i], Pm[i]);
		Fp12::mul(e3, e3, e1);
	}
	
	opt_ateMultiPairingJ(e4, 8, Qn, Pn);
	std::cout<<"e3.a_.a_.a_:"<<e3.a_.a_.a_<<std::endl;
	std::cout<<"e4.a_.a_.a_:"<<e4.a_.a_.a_<<std::endl;
	PUT(Fp12(e3-e4).isZero());
	std::cout<<std::endl;
//*
  for (i=0;i<8;i++) {
    bn::ecop::ScalarMult(Qo,Qp[i],ai[i]);
    bn::ecop::ScalarMult(Qn[i],Qo,bi[i]);
  }

  opt_ateMultiPairingJ(e4, 8, Qn, Pp);
  std::cout<<"e4.a_.a_.a_:"<<e4.a_.a_.a_<<std::endl;
  PUT(Fp12(e3-e4).isZero());
// */
/*
  for (i=0;i<8;i++) {
    Vk=(ai[i]*bi[i]);
    bn::ecop::ScalarMult(Pn[i],Pp[i],Vk);
  }

  opt_ateMultiPairingJ(e4, 8, Qp, Pn);
  std::cout<<"e4.a_.a_.a_:"<<e4.a_.a_.a_<<std::endl;
  TEST_EQUAL(e3.a_.a_.a_,e4.a_.a_.a_);
//*/
  /*
  opt_ateMultiPairingJ(e1, 8, Qp, Pp);
  Vk=0;
  for (i=0;i<8;i++) {Vk+=ai[i]*bi[i];}
  e4=power(e1,Vk);
  //GT_expo(e4,e1,ab,WB,BB);
  std::cout<<"e4.a_.a_.a_:"<<e4.a_.a_.a_<<std::endl;
  TEST_EQUAL(e3.a_.a_.a_,e4.a_.a_.a_);
  //*/

  /*
  for (i=0;i<8;i++) std::cout<<"Pp:"<<Pp[i][0]<<","<<Pp[i][1]<<","<<Pp[i][2]<<std::endl;
  bn::MultiNormalizeJacFp(Pn,Pp,8);
  for (i=0;i<8;i++) std::cout<<"Pn:"<<Pn[i][0]<<","<<Pn[i][1]<<","<<Pn[i][2]<<std::endl;
  //*/


//*
	Xbyak::util::Clock clkMultiPairingJ;
	clkMultiPairingJ.begin();
	for (size_t ii = 0; ii < N2; ii++)
		opt_ateMultiPairingJ(e4, 8, Qn, Pn);
	clkMultiPairingJ.end();
	printf("MultiPairingJ U8\t% 10.2fKclk\n", (clkMultiPairingJ.getClock() / K) / double(N2));
	TEST_EQUAL(e3.a_.a_.a_,e4.a_.a_.a_);

	Xbyak::util::Clock clkMultiPairingJ2;
	clkMultiPairingJ2.begin();
  	for (size_t ii = 0; ii < N2; ii++)
		opt_ateMultiPairingJ(e4, 7, Qn, Pn);
	clkMultiPairingJ2.end();
	printf("MultiPairingJ U7\t% 10.2fKclk\n", (clkMultiPairingJ2.getClock() / K) / double(N2));
	opt_ateMultiPairingJ(e4, 8, Qn, Pn);
//*/


Fp g1[]={Fp("0x1C2F70188B5D340DD39DA84D4EEC78D7D0BFB74815BCBF7AEEF163A8D50B1CC1"),Fp("0x20AEF0FBCFC4AED595CB7BF85C8C804C31A8E28B0ACAFAF2851B55502C0899E2"),Fp("1")};
std::cout<<bn::ecop::isOnECJac3(g1)<<std::endl;


Fp2 g2[]={
Fp2(Fp("0x78FE8DD1A89BD7E108262D14B47897592CECE88A123347F6DC12116BEC63250"),Fp("0x77B913FCE39AD736F90B45D920E3B4156367DF0BB9B225A9F7DB696331CBC11")),
Fp2(Fp("0x155119705330AE57F338EC84C28A7072B302B22EE1777674E82B10069561EA2B"),Fp("0xD0168FB2786FB107AEDB50BCD65999AB33F25AD0C9A0046A78029EE60B82CE4")),
Fp2(Fp("1"),Fp("0")),
        };
std::cout<<bn::ecop::isOnTwistECJac3(g2)<<std::endl;


  addJJJG1(Pn[1], g1, Pn[0]);
  NormalizeJac(Pn[1],Pn[1]);
  //std::cout<<"Pn[1]:"<<Pn[1][0]<<","<<Pn[1][1]<<","<<Pn[1][2]<<std::endl;

  ECAdd(Pn[2],g1,Pn[0]);
  NormalizeJac(Pn[2],Pn[2]);
  //std::cout<<"Pn[2]:"<<Pn[2][0]<<","<<Pn[2][1]<<","<<Pn[2][2]<<std::endl;
  TEST_EQUAL(Pn[1][0],Pn[2][0]);
  std::cout<<Pn[1][0]-Pn[2][0]<<std::endl;
  std::cout<<Pn[1][1]-Pn[2][1]<<std::endl;
  //std::cout<<Pn[1][2]-Pn[2][2]<<std::endl;

  //Vk=BigToVuint(rand(Big("18446744073709551616")));

  GT_expo(e3,e1,Vk,WB,BB);
  e4=power(e1,Vk);
  PUT(Fp12(e3-e4).isZero());

  std::cout<<"Testing G1known..."<<std::endl;
  Pn[0][0]=Pm[0][0]; Pn[0][1]=Pm[0][1]; Pn[0][2]=1;
  precomputeG1known(PsI,Pm[0],BetaS);
  Vk=rg.get();//.set(rand(Br).getbig()->w,4);
  G1_mulknown(Pn[1],Vk,PsI,W,SB);
  bn::ecop::NormalizeJac(Pn[1],Pn[1]);
  bn::ecop::ScalarMult(Pn[2],Pn[0],Vk);
  bn::ecop::NormalizeJac(Pn[2],Pn[2]);
  TEST_EQUAL(Pn[1][0],Pn[2][0]);
  G1_mul(Pn[3],Vk,Pn[0],W,SB,BetaS);
  bn::ecop::NormalizeJac(Pn[3],Pn[3]);
  TEST_EQUAL(Pn[2][0],Pn[3][0]);

  std::cout<<"Testing G2known..."<<std::endl;
  Qo[0]=g2[0]; Qo[1]=g2[1]; Qo[2]=g2[2];
  precomputeG2known(QsI,Qo);
  Vk=rg.get();//.set(rand(Br).getbig()->w,4);
  G2_mulknown(lQ5,Vk,WB,BB,QsI);
  bn::ecop::NormalizeJac(lQ5,lQ5);
  bn::ecop::ScalarMult(lQ5b,Qo,Vk);
  bn::ecop::NormalizeJac(lQ5b,lQ5b);
  TEST_EQUAL(lQ5[0],lQ5b[0]);

  Fp12 fe[4][PRECOMPUTEknown];
  Fp12 f,g,h;
  std::cout<<"Testing GTknown..."<<std::endl;
  precomputeGTknown(fe,e3);
  Vk=rg.get();//.set(rand(Br).getbig()->w,4);
  GT_expoknown(f,fe,Vk,WB,BB);
  g=power(e3,Vk);
  GT_expo(h,e3,Vk,WB,BB);
  TEST_EQUAL(f,g);TEST_EQUAL(g,h);



  Fp6 d;

  Xbyak::util::Clock clkLineDbl;
  clkLineDbl.begin();
  for (size_t i = 0; i < N; i++) {
    Fp6::pointDblLineEval(d, Qn[0], g1);
  }
  clkLineDbl.end();
  printf("LineDbl\t% 10.3fKclk\n", (clkLineDbl.getClock() / K) / double(N));

  Xbyak::util::Clock clkLineAdd;
  clkLineAdd.begin();
  for (size_t i = 0; i < N; i++) {
    Fp6::pointAddLineEval(d, Qn[1], Qn[0], g1);
  }
  clkLineAdd.end();
  printf("LineAdd\t% 10.3fKclk\n", (clkLineAdd.getClock() / K) / double(N));

  Fp6 l0[8][70];
  {
    Xbyak::util::Clock clkClock;
    clkClock.begin();
    for (size_t i = 0; i < N; i++) {
      for (int ii=0;ii<8;ii++) for (int jj=0;jj<70;jj++) l0[ii][jj]=0;
      PrecomputePairingKnownG2(l0, 8, Qn);
    }
    clkClock.end();
    printf("PrecomputePairingKnownG2b 8\t% 10.3fKclk\n", (clkClock.getClock() / K) / double(N));
  }
  //std::cout<<Qn[0][0]<<","<<Qn[0][1]<<","<<Qn[0][2]<<std::endl;
  NormalizeJac(Qo,Qn[0]);
  //std::cout<<Qo[0]<<","<<Qo[1]<<","<<Qo[2]<<std::endl;
  Qm[0][0]=Qo[0]; Qm[0][1]=Qo[1]; //Qm[0][2]=Qo[2];
  //std::cout<<Qm[0][0]<<","<<Qm[0][1]<<std::endl;
  bn::opt_atePairing<Fp>(e1, Qm[0], Pm[0]);
  PrecomputePairingKnownG2(l0, 1, Qn);
  opt_atePairingKnownG2(e2, l0[0], Pm[0]);
  TEST_EQUAL(e1,e2);
  {
    Xbyak::util::Clock clkClock;
    clkClock.begin();
    for (size_t i = 0; i < N; i++) {
      //for (int ii=0;ii<8;ii++) for (int jj=0;jj<70;jj++) l0[ii][jj]=0;
      bn::opt_atePairing<Fp>(e1, Qm[0], Pm[0]);
    }
    clkClock.end();
    printf("Regular Pairing \t% 10.3fKclk\n", (clkClock.getClock() / K) / double(N));
  }
  {
    Xbyak::util::Clock clkClock;
    clkClock.begin();
    for (size_t i = 0; i < N; i++) {
      //for (int ii=0;ii<8;ii++) for (int jj=0;jj<70;jj++) l0[ii][jj]=0;
      opt_atePairingKnownG2(e2, l0[0], Pm[0]);
    }
    clkClock.end();
    printf("KnownG2 Pairing \t% 10.3fKclk\n", (clkClock.getClock() / K) / double(N));
  }
  //*
  {
    int n=8;
    PrecomputePairingKnownG2(l0, n, Qn);
    Xbyak::util::Clock clkClock;
    clkClock.begin();
    for (size_t i = 0; i < N2; i++) {
      //for (int ii=0;ii<8;ii++) for (int jj=0;jj<70;jj++) l0[ii][jj]=0;
      opt_ateMultiPairingJKnownG2(e2, n, l0, Pn);
    }
    clkClock.end();
    printf("KnownG2 Multi Pairing %2d \t%10.3fKclk\n", n, (clkClock.getClock() / K) / double(N2));
  }
  {
    int n=7;
    PrecomputePairingKnownG2(l0, n, Qn);
    Xbyak::util::Clock clkClock;
    clkClock.begin();
    for (size_t i = 0; i < N2; i++) {
      //for (int ii=0;ii<8;ii++) for (int jj=0;jj<70;jj++) l0[ii][jj]=0;
      opt_ateMultiPairingJKnownG2(e2, n, l0, Pn);
    }
    clkClock.end();
    printf("KnownG2 Multi Pairing %2d \t%10.3fKclk\n", n, (clkClock.getClock() / K) / double(N2));
  }
  {
    int n=6;
    PrecomputePairingKnownG2(l0, n, Qn);
    Xbyak::util::Clock clkClock;
    clkClock.begin();
    for (size_t i = 0; i < N2; i++) {
      //for (int ii=0;ii<8;ii++) for (int jj=0;jj<70;jj++) l0[ii][jj]=0;
      opt_ateMultiPairingJKnownG2(e2, n, l0, Pn);
    }
    clkClock.end();
    printf("KnownG2 Multi Pairing %2d \t%10.3fKclk\n", n, (clkClock.getClock() / K) / double(N2));
  }
  //*/

  //for (int i=1;i<256;i++) std::cout<<i<<":"<<(int)log2((unsigned)i)<<"\t"; std::cout<<std::endl;
#endif
  Fp PC[256][2];
  Fp G1[]={Fp("0x1C2F70188B5D340DD39DA84D4EEC78D7D0BFB74815BCBF7AEEF163A8D50B1CC1"),Fp("0x20AEF0FBCFC4AED595CB7BF85C8C804C31A8E28B0ACAFAF2851B55502C0899E2"),Fp("1")};
  precomputeG1knownC(PC,G1,W,SB,BetaS);
  M_uint td=pow(2,32);
  Fp PA[3],PB[3];
  G1_mul(PA,td,G1,W,SB,BetaS);
  bn::ecop::NormalizeJac(PA,PA);

  std::cout<<"Quietly testing powers for precomputation"<<std::endl;
//PUT(PA[0]);
//PUT(PC[2][0]);
//exit(1);
  TEST_EQUAL(PA[0],PC[2][0]); TEST_EQUAL(PA[1],PC[2][1]);

  PB[0]=PC[2][0]; PB[1]=PC[2][1]; PB[2]=1;
  G1_mul(PA,td,PB,W,SB,BetaS);
  bn::ecop::NormalizeJac(PA,PA);
  TEST_EQUAL(PA[0],PC[4][0]); TEST_EQUAL(PA[1],PC[4][1]);

  PB[0]=PC[4][0]; PB[1]=PC[4][1]; PB[2]=1;
  G1_mul(PA,td,PB,W,SB,BetaS);
  bn::ecop::NormalizeJac(PA,PA);
  TEST_EQUAL(PA[0],PC[8][0]); TEST_EQUAL(PA[1],PC[8][1]);

  PB[0]=PC[8][0]; PB[1]=PC[8][1]; PB[2]=1;
  G1_mul(PA,td,PB,W,SB,BetaS);
  bn::ecop::NormalizeJac(PA,PA);
  TEST_EQUAL(PA[0],PC[16][0]); TEST_EQUAL(PA[1],PC[16][1]);

  PB[0]=PC[16][0]; PB[1]=PC[16][1]; PB[2]=1;
  G1_mul(PA,td,PB,W,SB,BetaS);
  bn::ecop::NormalizeJac(PA,PA);
  TEST_EQUAL(PA[0],PC[32][0]); TEST_EQUAL(PA[1],PC[32][1]);

  PB[0]=PC[32][0]; PB[1]=PC[32][1]; PB[2]=1;
  G1_mul(PA,td,PB,W,SB,BetaS);
  bn::ecop::NormalizeJac(PA,PA);
  TEST_EQUAL(PA[0],PC[64][0]); TEST_EQUAL(PA[1],PC[64][1]);

  PB[0]=PC[64][0]; PB[1]=PC[64][1]; PB[2]=1;
  G1_mul(PA,td,PB,W,SB,BetaS);
  bn::ecop::NormalizeJac(PA,PA);
  TEST_EQUAL(PA[0],PC[128][0]); TEST_EQUAL(PA[1],PC[128][1]);

  M_uint Vtd[256];
  Vtd[0]=0;
  Vtd[1]=1;
  Vtd[2]=power((M_uint)2,(M_uint)32);
  int ip,ip2; ip=2;
  for (int i=3;i<256;i++) {
    if (i==(1<<ip)) {
      Vtd[i]=power((M_uint)2,(M_uint)(ip*32));
      ip++;
      continue;
    } else {
      Vtd[i]=Vtd[(1<<(ip-1))];
      ip2=i;
      for (int j=0;j<(int)log2((unsigned)i);j++){
        if (ip2&1) {
          Vtd[i]=Vtd[i]+Vtd[(1<<j)];
        }
        ip2>>=1;
      }
    }
  }
  //for (i=0;i<256;i++) std::cout<<"i="<<i<<":"<<hex<<Vtd[i]<<std::endl;

  std::cout<<"Quietly re-testing combinations for precomputation"<<std::endl;
  int iacump,iacumn; iacump=iacumn=0;
  for (int i=0;i<256;i++){
    G1_mul(PA,Vtd[i],G1,W,SB,BetaS); bn::ecop::NormalizeJac(PA,PA);
    if ((PA[0]==PC[i][0])&&(PA[1]==PC[i][1])) iacump++; else {iacumn++; PUT(i);}
  }
  std::cout<<"+:"<<iacump<<",-:"<<iacumn<<std::endl;
  PUT(PC[0][0]); PUT(PC[0][1]);
  G1_mul(PA,Vtd[0],G1,W,SB,BetaS); bn::ecop::NormalizeJac(PA,PA);
  PUT(PA[0]); PUT(PA[1]);


  int nn;
  std::cout<<"Quietly testing combinations for precomputation"<<std::endl;

  PA[0]=PC[1][0]; PA[1]=PC[1][1]; PA[2]=1;
  PB[0]=PC[2][0]; PB[1]=PC[2][1]; PB[2]=1;
  bn::ecop::ECAdd(PA,PA,PB); bn::ecop::NormalizeJac(PA,PA);
  nn=3; TEST_EQUAL(PA[0],PC[nn][0]); TEST_EQUAL(PA[1],PC[nn][1]);

  PA[0]=PC[1][0]; PA[1]=PC[1][1]; PA[2]=1;
  PB[0]=PC[4][0]; PB[1]=PC[4][1]; PB[2]=1;
  bn::ecop::ECAdd(PA,PA,PB); bn::ecop::NormalizeJac(PA,PA);
  nn=5; TEST_EQUAL(PA[0],PC[nn][0]); TEST_EQUAL(PA[1],PC[nn][1]);

  PA[0]=PC[4][0]; PA[1]=PC[4][1]; PA[2]=1;
  PB[0]=PC[2][0]; PB[1]=PC[2][1]; PB[2]=1;
  bn::ecop::ECAdd(PA,PA,PB); bn::ecop::NormalizeJac(PA,PA);
  nn=6; TEST_EQUAL(PA[0],PC[nn][0]); TEST_EQUAL(PA[1],PC[nn][1]);
  //PA[0]=PC[6][0]; PA[1]=PC[6][1]; PA[2]=1;
  PB[0]=PC[1][0]; PB[1]=PC[1][1]; PB[2]=1;
  bn::ecop::ECAdd(PA,PA,PB); bn::ecop::NormalizeJac(PA,PA);
  nn=7; TEST_EQUAL(PA[0],PC[nn][0]); TEST_EQUAL(PA[1],PC[nn][1]);

  PA[0]=PC[8][0]; PA[1]=PC[8][1]; PA[2]=1;
  PB[0]=PC[2][0]; PB[1]=PC[2][1];  PB[2]=1;
  bn::ecop::ECAdd(PA,PA,PB);
  PB[0]=PC[1][0]; PB[1]=PC[1][1];  PB[2]=1;
  bn::ecop::ECAdd(PA,PA,PB); bn::ecop::NormalizeJac(PA,PA);
  nn=11; TEST_EQUAL(PA[0],PC[nn][0]); TEST_EQUAL(PA[1],PC[nn][1]);

  PA[0]=PC[8][0]; PA[1]=PC[8][1]; PA[2]=1;
  PB[0]=PC[4][0]; PB[1]=PC[4][1];  PB[2]=1;
  bn::ecop::ECAdd(PA,PA,PB); bn::ecop::NormalizeJac(PA,PA);
  nn=12; TEST_EQUAL(PA[0],PC[nn][0]); TEST_EQUAL(PA[1],PC[nn][1]);

  PA[0]=PC[14][0]; PA[1]=PC[14][1]; PA[2]=1;
  PB[0]=PC[1][0];  PB[1]=PC[1][1];  PB[2]=1;
  bn::ecop::ECAdd(PA,PA,PB); bn::ecop::NormalizeJac(PA,PA);
  nn=15; TEST_EQUAL(PA[0],PC[nn][0]); TEST_EQUAL(PA[1],PC[nn][1]);

  PA[0]=PC[12][0]; PA[1]=PC[12][1]; PA[2]=1;
  PB[0]=PC[1][0];  PB[1]=PC[1][1];  PB[2]=1;
  bn::ecop::ECAdd(PA,PA,PB);
  PB[0]=PC[2][0];  PB[1]=PC[2][1];  PB[2]=1;
  bn::ecop::ECAdd(PA,PA,PB); bn::ecop::NormalizeJac(PA,PA);
  nn=15; TEST_EQUAL(PA[0],PC[nn][0]); TEST_EQUAL(PA[1],PC[nn][1]);

  PA[0]=PC[16][0]; PA[1]=PC[16][1]; PA[2]=1;
  PB[0]=PC[1][0];  PB[1]=PC[1][1];  PB[2]=1;
  bn::ecop::ECAdd(PA,PA,PB); bn::ecop::NormalizeJac(PA,PA);
  nn=17; TEST_EQUAL(PA[0],PC[nn][0]); TEST_EQUAL(PA[1],PC[nn][1]);

  PA[0]=PC[16][0]; PA[1]=PC[16][1]; PA[2]=1;
  PB[0]=PC[2][0];  PB[1]=PC[2][1];  PB[2]=1;
  bn::ecop::ECAdd(PA,PA,PB); bn::ecop::NormalizeJac(PA,PA);
  nn=18; TEST_EQUAL(PA[0],PC[nn][0]); TEST_EQUAL(PA[1],PC[nn][1]);

  PA[0]=PC[16][0]; PA[1]=PC[16][1]; PA[2]=1;
  PB[0]=PC[4][0];  PB[1]=PC[4][1];  PB[2]=1;
  bn::ecop::ECAdd(PA,PA,PB); bn::ecop::NormalizeJac(PA,PA);
  nn=20; TEST_EQUAL(PA[0],PC[nn][0]); TEST_EQUAL(PA[1],PC[nn][1]);

  PA[0]=PC[16][0]; PA[1]=PC[16][1]; PA[2]=1;
  PB[0]=PC[4][0];  PB[1]=PC[4][1];  PB[2]=1;
  bn::ecop::ECAdd(PA,PA,PB);
  PB[0]=PC[1][0];  PB[1]=PC[1][1];  PB[2]=1;
  bn::ecop::ECAdd(PA,PA,PB); bn::ecop::NormalizeJac(PA,PA);
  nn=21; TEST_EQUAL(PA[0],PC[nn][0]); TEST_EQUAL(PA[1],PC[nn][1]);

  PA[0]=PC[16][0]; PA[1]=PC[16][1]; PA[2]=1;
  PB[0]=PC[4][0];  PB[1]=PC[4][1];  PB[2]=1;
  bn::ecop::ECAdd(PA,PA,PB);
  PB[0]=PC[2][0];  PB[1]=PC[2][1];  PB[2]=1;
  bn::ecop::ECAdd(PA,PA,PB); bn::ecop::NormalizeJac(PA,PA);
  nn=22; TEST_EQUAL(PA[0],PC[nn][0]); TEST_EQUAL(PA[1],PC[nn][1]);

  PA[0]=PC[128][0]; PA[1]=PC[128][1]; PA[2]=1;
  PB[0]=PC[2][0];   PB[1]=PC[2][1];  PB[2]=1;
  bn::ecop::ECAdd(PA,PA,PB); bn::ecop::NormalizeJac(PA,PA);
  nn=130; TEST_EQUAL(PA[0],PC[nn][0]); TEST_EQUAL(PA[1],PC[nn][1]);

  PB[0]=PC[4][0];   PB[1]=PC[4][1];  PB[2]=1;
  bn::ecop::ECAdd(PA,PA,PB); bn::ecop::NormalizeJac(PA,PA);
  nn=134; TEST_EQUAL(PA[0],PC[nn][0]); TEST_EQUAL(PA[1],PC[nn][1]);

  PA[0]=PC[128][0]; PA[1]=PC[128][1]; PA[2]=1;
  PB[0]=PC[4][0];   PB[1]=PC[4][1];  PB[2]=1;
  bn::ecop::ECAdd(PA,PA,PB); bn::ecop::NormalizeJac(PA,PA);
  nn=132; TEST_EQUAL(PA[0],PC[nn][0]); TEST_EQUAL(PA[1],PC[nn][1]);

  PA[0]=PC[128][0]; PA[1]=PC[128][1]; PA[2]=1;
  PB[0]=PC[16][0];  PB[1]=PC[16][1];  PB[2]=1;
  bn::ecop::ECAdd(PA,PA,PB); bn::ecop::NormalizeJac(PA,PA);
  nn=144; TEST_EQUAL(PA[0],PC[nn][0]); TEST_EQUAL(PA[1],PC[nn][1]);

  PB[0]=PC[8][0];  PB[1]=PC[8][1]; PB[2]=1;
  bn::ecop::ECAdd(PA,PA,PB); bn::ecop::NormalizeJac(PA,PA);
  PB[0]=PC[2][0];  PB[1]=PC[2][1]; PB[2]=1;
  bn::ecop::ECAdd(PA,PA,PB); bn::ecop::NormalizeJac(PA,PA);
  nn=154; TEST_EQUAL(PA[0],PC[nn][0]); TEST_EQUAL(PA[1],PC[nn][1]);

  std::cout<<"Test2"<<std::endl;
  //Vk[0]=Vk[1]=Vk[2]=Vk[3]=0;
  //Vk.set("0xCAFE201219803344CAFE201219803344CAFE201219803344CAFE201219803344");
  //Vk.set("0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF");
  //Vk.set("0xAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
  //Vk.set("0xF");
  Vk=rg.get();//Vk.set(rand(Br).getbig()->w,4);
  precomputeG1knownC(PC,G1,W,SB,BetaS);
  G1_mulknownC(R1,Vk,PC,PT); bn::ecop::NormalizeJac(R1,R1);
  G1_mul(R2,Vk,G1,W,SB,BetaS); bn::ecop::NormalizeJac(R2,R2);
  TEST_EQUAL(R1[0],R2[0]); TEST_EQUAL(R1[1],R2[1]);
  bn::ecop::ScalarMult(R2,G1,Vk); bn::ecop::NormalizeJac(R2,R2);
  PUT(R1[0]-R2[0]); PUT(R1[1]-R2[1]);

  std::cout<<"Quietly testing G1 knownC"<<std::endl;
  tmp[0]=G1[0]; tmp[1]=G1[1]; tmp[2]=1;
  std::cout<<"G1 KC"<<std::endl;
  int acumG1C=0;
  for (i=0;i<1000;i++){
    //std::cout<<i<<std::endl;
    Vk=rg.get();//.set(rand(Br).getbig()->w,4);
    G1_mulknownC(R1,Vk,PC,PT);
    bn::ecop::NormalizeJac(R1,R1);
    bn::ecop::ScalarMult(R2,tmp,Vk);
    bn::ecop::NormalizeJac(R2,R2);
    TEST_EQUAL(R1[0],R2[0]);
    if (R1[0]!=R2[0]) {
      acumG1C++;
      M_sint u[2];
      DecompG1(u,Vk,W,SB);
      for (int ii=0;ii<2;ii++) std::cout<<u[ii]<<","; std::cout<<std::endl; std::cout<<Vk<<std::endl;
      std::cout<<"P("<<G1[0]<<","<<G1[1]<<")"<<std::endl;
    }
    //TEST_EQUAL(R1[1],R2[1]);
    //TEST_EQUAL(R1[2],R2[2]);
  }
  std::cout<<"acum="<<acumG1C<<std::endl;
  //std::cout<<"G1_mulKC.1Core:\n"<<R1[0]<<","<<R1[1]<<","<<R1[2]<<std::endl;
  //std::cout<<"ScalarMult.1Core.Vk2:\n"<<R2[0]<<","<<R2[1]<<","<<R2[2]<<std::endl;

  {
    Xbyak::util::Clock clkClock;
    clkClock.begin();
    for (size_t i = 0; i < N; i++) {
      G1_mulknownC(R1,Vk,PC,PT);
    }
    clkClock.end();
    printf("G1_mulknownC \t% 10.3fKclk\n", (clkClock.getClock() / K) / double(N));
  }
  //*/

  /*
  M_uint lk;
  {
    unsigned char acum[32];
    Xbyak::util::Clock clkClock;
    clkClock.begin();
    for (size_t i = 0; i < N; i++) {
      RecodeForPrecompC2(acum,Vk,TC);
    }
    clkClock.end();
    printf("RecodeForPrecompC \t% 10.3fKclk\n", (clkClock.getClock() / K) / double(N));
  }

  {
    Xbyak::util::Clock clkClock;
    clkClock.begin();
    for (size_t i = 0; i < N; i++) {
      precomputeG1knownC(PC,G1,W,SB,BetaS);
    }
    clkClock.end();
    printf("precomputeG1knownC \t% 10.3fKclk\n", (clkClock.getClock() / K) / double(N));
  }
  //*/

  Fp2 PC2[256][2];
  Fp2 G2[]={
        Fp2(Fp("12723517038133731887338407189719511622662176727675373276651903807414909099441"),Fp("4168783608814932154536427934509895782246573715297911553964171371032945126671")),
        Fp2(Fp("13891744915211034074451795021214165905772212241412891944830863846330766296736"),Fp("7937318970632701341203597196594272556916396164729705624521405069090520231616")),
        Fp2(Fp("1"),Fp("0"))
        };
  Fp2 R12[3],R22[3];
  Vk=rg.get();//.set(rand(Br).getbig()->w,4);
  precomputeG2knownC(PC2,G2,WB,BB);
  G2_mulknownC(R12,Vk,PC2,PT); bn::ecop::NormalizeJac(R12,R12);
  G2_mul(R22,G2,Vk,WB,BB); bn::ecop::NormalizeJac(R22,R22);
  TEST_EQUAL(R12[0],R22[0]); TEST_EQUAL(R12[1],R22[1]);
  bn::ecop::ScalarMult(R22,G2,Vk); bn::ecop::NormalizeJac(R22,R22);
  PUT(R12[0]-R22[0]); PUT(R12[1]-R22[1]);

  {
    Xbyak::util::Clock clkClock;
    clkClock.begin();
    for (size_t i = 0; i < N; i++) {
      G2_mulknownC(R12,Vk,PC2,PT);
    }
    clkClock.end();
    printf("G2_mulknownC \t% 10.3fKclk\n", (clkClock.getClock() / K) / double(N));
  }

  std::cout<<"Quietly testing G2 knownC"<<std::endl;
  Fp2 tmp2[3];
  tmp2[0]=G2[0]; tmp2[1]=G2[1]; tmp2[2]=1;
  std::cout<<"G2 KC"<<std::endl;
  int acumG2C=0;
  precomputeG2knownC(PC2,G2,WB,BB);
  for (i=0;i<100;i++){
    //std::cout<<i<<std::endl;
    Vk=rg.get();//.set(rand(Br).getbig()->w,4);
    G2_mulknownC(R12,Vk,PC2,PT);
    bn::ecop::NormalizeJac(R12,R12);
    bn::ecop::ScalarMult(R22,tmp2,Vk);
    bn::ecop::NormalizeJac(R22,R22);
    //TEST_EQUAL(R12[0],R22[0]);
    if (R12[0]!=R22[0]) {
      acumG2C++;
      //PUT(Vk);
      //std::cout<<"P("<<G2[0]<<","<<G2[1]<<")"<<std::endl;
    }
    //TEST_EQUAL(R1[1],R2[1]);
    //TEST_EQUAL(R1[2],R2[2]);
  }
  std::cout<<"acum="<<acumG2C<<std::endl;

  std::cout<<"Quietly re-testing combinations for precomputation G2"<<std::endl;
  iacump=iacumn=0;
  for (int i=0;i<256;i++){
    G2_mul(R12,G2,Vtd[i],WB,BB); bn::ecop::NormalizeJac(R12,R12);
    if ((R12[0]==PC2[i][0])&&(R12[1]==PC2[i][1])) iacump++; else {iacumn++; PUT(i);}
  }
  std::cout<<"+:"<<iacump<<",-:"<<iacumn<<std::endl;


  /*
  Vk.set("14282855421176169383435467954704162388791070254816859802788558881137450783236");
  precomputeG2knownC(PC2,G2,WB,BB);
  G2_mulknownC(R12,Vk,PC2,TC); bn::ecop::NormalizeJac(R12,R12);
  bn::ecop::ScalarMult(R22,tmp2,Vk); bn::ecop::NormalizeJac(R22,R22);
  TEST_EQUAL(R12[0],R22[0]);
  G2_mul(R22,G2,Vk,WB,BB); bn::ecop::NormalizeJac(R22,R22);
  TEST_EQUAL(R12[0],R22[0]);
  precomputeG2known(QsI,G2);
  G2_mulknown(R22,Vk,WB,BB,QsI); bn::ecop::NormalizeJac(R22,R22);
  TEST_EQUAL(R12[0],R22[0]);
  //*/

  //Fp12 e1;
  //Fp12 e2;
  //Fp12 e3;
  bn::opt_atePairing<Fp>(e1, G2, G1);
  Fp12 PC3[256];
  Vk=rg.get();//.set(rand(Br).getbig()->w,4);
  precomputeGTknownC(PC3,e1,WB,BB);
  GT_expoknownC(e2,Vk,PC3,PT);
  GT_expo(e3,e1,Vk,WB,BB);
  TEST_EQUAL(e2,e3);

  {
    Xbyak::util::Clock clkClock;
    clkClock.begin();
    for (size_t i = 0; i < N; i++) {
      GT_expoknownC(e2,Vk,PC3,PT);
    }
    clkClock.end();
    printf("GT_mulknownC \t% 10.3fKclk\n", (clkClock.getClock() / K) / double(N));
  }

  std::cout<<"Quietly testing GT knownC"<<std::endl;
  std::cout<<"GT KC"<<std::endl;
  int acumGTC=0;
  precomputeGTknownC(PC3,e1,WB,BB);
  for (i=0;i<100;i++){
    Vk=rg.get();//.set(rand(Br).getbig()->w,4);
    GT_expoknownC(e2,Vk,PC3,PT);
    GT_expo(e3,e1,Vk,WB,BB);
    //TEST_EQUAL(R12[0],R22[0]);
    if (e2!=e3) {
      acumGTC++;
      //PUT(Vk);
      //std::cout<<"P("<<G2[0]<<","<<G2[1]<<")"<<std::endl;
    }
    //TEST_EQUAL(R1[1],R2[1]);
    //TEST_EQUAL(R1[2],R2[2]);
  }
  std::cout<<"acum="<<acumGTC<<std::endl;

  {
    Xbyak::util::Clock clkClock;
    clkClock.begin();
    for (size_t i = 0; i < N; i++) {
      p_power_Frobenius(Q5A, Q5A);
    }
    clkClock.end();
    printf("p_power_Frobenius \t% 10.3fKclk\n", (clkClock.getClock() / K) / double(N));
  }
 {
    Xbyak::util::Clock clkClock;
    clkClock.begin();
    for (size_t i = 0; i < N; i++) {
      bn::ecop::NormalizeJac(Q5A, Q5A);
    }
    clkClock.end();
    printf("bn::ecop::NormalizeJac \t% 10.3fKclk\n", (clkClock.getClock() / K) / double(N));
  }
  {
  unsigned char acum[32];
  M_uint t256; M_setStr(t256, "0x10000000000000000000000000000000000000000000000000000000000000000");
  M_uint Half_Zr; M_setStr(Half_Zr, "0x1291B24120000000DD1A26C000000003FFCFC000000000085080000000000007");

      Xbyak::util::Clock clkClock;
    clkClock.begin();
    for (size_t i = 0; i < N; i++) {
      RecodeForPrecompSC(acum, Vk, PT, t256, Half_Zr);
    }
    clkClock.end();
    printf("RecodeForPrecompSC \t% 10.3fKclk\n", (clkClock.getClock() / K) / double(N));
  }

  {
    M_uint t256;
     M_uint Half_Zr;

      Xbyak::util::Clock clkClock;
    clkClock.begin();
    for (size_t i = 0; i < N; i++) {
        M_setStr(t256, "0x10000000000000000000000000000000000000000000000000000000000000000");
  		M_setStr(Half_Zr, "0x1291B24120000000DD1A26C000000003FFCFC000000000085080000000000007");
    }
    clkClock.end();
    printf("Setting consts for RecodingSC \t% 10.3fKclk\n", (clkClock.getClock() / K) / double(N));
  }

  {
    Xbyak::util::Clock clkClock;
    clkClock.begin();
    for (size_t i = 0; i < N; i++) {
      bn::MultiNormalizeFp2(Qm,Qn,6);
    }
    clkClock.end();
    printf("MultiNormalizeFp2,6 \t% 10.3fKclk\n", (clkClock.getClock() / K) / double(N));
  }

 {
    Xbyak::util::Clock clkClock;
    clkClock.begin();
    for (size_t i = 0; i < N; i++) {
      bn::MultiNormalizeFp(Po,Pn,6);
    }
    clkClock.end();
    printf("MultiNormalizeFp,6 \t% 10.3fKclk\n", (clkClock.getClock() / K) / double(N));
  }
  {
    Xbyak::util::Clock clkClock;
    clkClock.begin();
    for (size_t i = 0; i < N; i++) {
      opt_ateMultiPairingJ(e4, 8, Qn, Pn);
    }
    clkClock.end();
    printf("Multipairing U,8 \t% 10.3fKclk\n", (clkClock.getClock() / K) / double(N));
  }
  
	{
		using namespace mie;
	        Xbyak::util::Clock clkClock;
        	clkClock.begin();
		for (size_t ii = 0; ii < N; ii++) {
			if (M_bitLen(Vk) < 31) {
				//G1_mulSmall(R, Vk, P1);
				return 1;
		    }
			M_sint u[2];
			Fp P[2]; P[0] = PA[0]; P[1] = PA[1];
			Fp PsI[2][PRECOMPUTE][2];
			int i, j;
			int v[4][130];

			for (i = 0; i < 4; i++)for (j = 0; j < 130; j++) v[i][j] = 0;

			int tlen[2], tlent = 0;
			DecompG1(u, Vk, W, SB);
			for (i = 0; i < 2; i++) tlen[i] = 130;

			NAFw2(v[0], v[1], tlen[0], M_abs(u[0]), WNAF); if (tlen[0] > tlent) tlent = tlen[0];
			NAFw2(v[2], v[3], tlen[1], M_abs(u[1]), WNAF); if (tlen[1] > tlent) tlent = tlen[1];

			precomputeG1b(PsI, P, BetaS);
			for (i = 0; i < 2; i++) if (u[i] < 0) for (j = 0; j < PRECOMPUTE; j++) PsI[i][j][1] = -PsI[i][j][1];
		

		}
		clkClock.end();
		printf("G1 preloop \t% 10.3fKclk\n", (clkClock.getClock() / K) / double(N));
	}
  
    {
		using namespace mie;
		M_sint u[2];
    		Fp P[2]; P[0] = PA[0]; P[1] = PA[1];
		Fp PsI[2][PRECOMPUTE][2];
		int i, j;
		int v[4][130];

		for (i = 0; i < 4; i++)for (j = 0; j < 130; j++) v[i][j] = 0;

		int tlen[2], tlent = 0;
		DecompG1(u, Vk, W, SB);
		for (i = 0; i < 2; i++) tlen[i] = 130;

		NAFw2(v[0], v[1], tlen[0], M_abs(u[0]), WNAF); if (tlen[0] > tlent) tlent = tlen[0];
		NAFw2(v[2], v[3], tlen[1], M_abs(u[1]), WNAF); if (tlen[1] > tlent) tlent = tlen[1];

		precomputeG1b(PsI, P, BetaS);
		for (i = 0; i < 2; i++) if (u[i] < 0) for (j = 0; j < PRECOMPUTE; j++) PsI[i][j][1] = -PsI[i][j][1];
		
		Fp S[2]; Fp S2[3], R2[3];
		
		Fp R[3];
		
        	Xbyak::util::Clock clkClock;
	        clkClock.begin();
		for (size_t ii = 0; ii < N; ii++) {
			
			R[0] = 1; R[1] = 1; R[2] = 0;

			for (i = tlent - 1; i >= 0; i--) {
				dblJG1(R2, R); //bn::ecop::ECDouble(R2,R);
				R[0] = R2[0]; R[1] = R2[1]; R[2] = R2[2];

				for (j = 0; j <= 3; j += 2) {
					if (i <= tlen[j / 2] - 1) {
						if (v[j][i] > 0) {
							S[0] = PsI[j / 2][(v[j][i] - 1) >> 1][0];
							S[1] = PsI[j / 2][(v[j][i] - 1) >> 1][1];
							S2[0] = S[0]; S2[1] = S[1]; S2[2] = 1;
							addJJAG1b(R, R, S);
						}

						if (v[j + 1][i] > 0) {
							S[0] = PsI[j / 2][(v[j + 1][i] - 1) >> 1][0];
							S[1] = -PsI[j / 2][(v[j + 1][i] - 1) >> 1][1];
							S2[0] = S[0]; S2[1] = S[1]; S2[2] = 1;
							addJJAG1b(R, R, S);
						}
					}
				}
			}
        }
		clkClock.end();
		printf("G1 loop \t% 10.3fKclk\n", (clkClock.getClock() / K) / double(N));
	} 
	
	{
		Xbyak::util::Clock clkClock;
		clkClock.begin();
		for (size_t ii = 0; ii < N; ii++) {
			if (M_bitLen(Vk) < 31) {
				G2_mulSmall(Q, Vk, Q5A);
				return 1;
			}
			int i, j;
			Fp2 QsI[4][PRECOMPUTE][2];
			M_sint u[4];
			int v[8][70];

			for (i = 0; i < 8; i++)for (j = 0; j < 70; j++) v[i][j] = 0;

			int tlen[4], tlent = 0;
			DecompG2(u, Vk, WB, BB); //for(i=0;i<4;i++) std::cout<<u[i]<<","; std::cout<<std::endl;

			for (i = 0; i < 4; i++) tlen[i] = 70;

			NAFw2(v[0], v[1], tlen[0], M_abs(u[0]), WNAF); if (tlen[0] > tlent) tlent = tlen[0];
			NAFw2(v[2], v[3], tlen[1], M_abs(u[1]), WNAF); if (tlen[1] > tlent) tlent = tlen[1];
			NAFw2(v[4], v[5], tlen[2], M_abs(u[2]), WNAF); if (tlen[2] > tlent) tlent = tlen[2];
			NAFw2(v[6], v[7], tlen[3], M_abs(u[3]), WNAF); if (tlen[3] > tlent) tlent = tlen[3];

			precomputeG2b(QsI, Q, u);
		}
		clkClock.end();
		printf("G2 preloop \t% 10.3fKclk\n", (clkClock.getClock() / K) / double(N));
	}
	{
		int i, j;
		Fp2 QsI[4][PRECOMPUTE][2];
		Fp2 Q2[3];

		M_sint u[4];
		int v[8][70];

		for (i = 0; i < 8; i++)for (j = 0; j < 70; j++) v[i][j] = 0;

		int tlen[4], tlent = 0;
		DecompG2(u, Vk, WB, BB); //for(i=0;i<4;i++) std::cout<<u[i]<<","; std::cout<<std::endl;

		for (i = 0; i < 4; i++) tlen[i] = 70;

		NAFw2(v[0], v[1], tlen[0], M_abs(u[0]), WNAF); if (tlen[0] > tlent) tlent = tlen[0];
		NAFw2(v[2], v[3], tlen[1], M_abs(u[1]), WNAF); if (tlen[1] > tlent) tlent = tlen[1];
		NAFw2(v[4], v[5], tlen[2], M_abs(u[2]), WNAF); if (tlen[2] > tlent) tlent = tlen[2];
		NAFw2(v[6], v[7], tlen[3], M_abs(u[3]), WNAF); if (tlen[3] > tlent) tlent = tlen[3];

		precomputeG2b(QsI, Q, u);
		Fp2 S[2];
		
		Xbyak::util::Clock clkClock;
		clkClock.begin();
		for (size_t ii = 0; ii < N; ii++) {
			Q[0] = 1; Q[1] = 1; Q[2] = 0;

			for (i = tlent - 1; i >= 0; i--) {
				dblJG2(Q2, Q); //bn::ecop::ECDouble(Q2,Q);
				Q[0] = Q2[0]; Q[1] = Q2[1]; Q[2] = Q2[2];

				for (j = 0; j <= 7; j += 2) {
					if (i <= tlen[j / 2] - 1) {
						//std::cout<<i<<":"<<j/2<<":"<<v[j][i]<<","<<v[j+1][i]<<std::endl;
						if (v[j][i] > 0) {
							S[0] = QsI[(j >> 1)][(v[j][i] - 1) >> 1][0];
							S[1] = QsI[(j >> 1)][(v[j][i] - 1) >> 1][1];
							addJJAG2c(Q, Q, S); //,half);
						}

						if (v[j + 1][i] > 0) {
							S[0] = QsI[(j >> 1)][(v[j + 1][i] - 1) >> 1][0];
							Fp2::neg(S[1], QsI[(j >> 1)][(v[j + 1][i] - 1) >> 1][1]);
							addJJAG2c(Q, Q, S); //,half);
						}
					}
				}
			}
		}
		clkClock.end();
		printf("G2 loop \t% 10.3fKclk\n", (clkClock.getClock() / K) / double(N));
	}
	{
		Fp12 z;
		Xbyak::util::Clock clkClock;
		clkClock.begin();
		for (size_t ii = 0; ii < N; ii++) {
			if (M_blockSize(Vk) < 2) {
				z = power(e1, Vk);
				return 1;
			}
			int i, j;
			M_sint u[4];
			int v[8][70];

			for (i = 0; i < 8; i++)for (j = 0; j < 70; j++) v[i][j] = 0;

			int tlen[4], tlent = 0;
			Fp12 S;
			Fp12 fe[4][PRECOMPUTE];
			DecompG2(u, Vk, WB, BB);

			for (i = 0; i < 4; i++) tlen[i] = 70;

			NAFw2(v[0], v[1], tlen[0], M_abs(u[0]), WNAF); if (tlen[0] > tlent) tlent = tlen[0];
			NAFw2(v[2], v[3], tlen[1], M_abs(u[1]), WNAF); if (tlen[1] > tlent) tlent = tlen[1];
			NAFw2(v[4], v[5], tlen[2], M_abs(u[2]), WNAF); if (tlen[2] > tlent) tlent = tlen[2];
			NAFw2(v[6], v[7], tlen[3], M_abs(u[3]), WNAF); if (tlen[3] > tlent) tlent = tlen[3];

			precomputeGT(fe, e1);

			for (i = 0; i < 4; i++)
				if (u[i] < 0)
					for (j = 0; j < PRECOMPUTE; j++)
						Fp6::neg(fe[i][j].b_, fe[i][j].b_);

		}
		clkClock.end();
		printf("GT preloop \t% 10.3fKclk\n", (clkClock.getClock() / K) / double(N));
	}
	{
		int i, j;
		M_sint u[4];
		int v[8][70];

		for (i = 0; i < 8; i++)for (j = 0; j < 70; j++) v[i][j] = 0;

		int tlen[4], tlent = 0;
		Fp12 S;
		Fp12 fe[4][PRECOMPUTE];
		DecompG2(u, Vk, WB, BB);

		for (i = 0; i < 4; i++) tlen[i] = 70;

		NAFw2(v[0], v[1], tlen[0], M_abs(u[0]), WNAF); if (tlen[0] > tlent) tlent = tlen[0];
		NAFw2(v[2], v[3], tlen[1], M_abs(u[1]), WNAF); if (tlen[1] > tlent) tlent = tlen[1];
		NAFw2(v[4], v[5], tlen[2], M_abs(u[2]), WNAF); if (tlen[2] > tlent) tlent = tlen[2];
		NAFw2(v[6], v[7], tlen[3], M_abs(u[3]), WNAF); if (tlen[3] > tlent) tlent = tlen[3];

		precomputeGT(fe, e1);

		for (i = 0; i < 4; i++)
			if (u[i] < 0)
				for (j = 0; j < PRECOMPUTE; j++)
					Fp6::neg(fe[i][j].b_, fe[i][j].b_);

		Xbyak::util::Clock clkClock;
		clkClock.begin();
		for (size_t ii = 0; ii < N; ii++) {
			e2 = 1;
				for (i = tlent - 1; i >= 0; i--) {
					e2.sqru();
					for (j = 0; j <= 7; j += 2) {
						if (i <= tlen[j / 2] - 1) {
							if (v[j][i] > 0) {
								Fp12::mul(e2, e2, fe[j / 2][(v[j][i]) / 2]);
							}
							if (v[j + 1][i] > 0) {
								S.a_ = fe[j / 2][(v[j + 1][i]) / 2].a_;
								Fp6::neg(S.b_, fe[j / 2][(v[j + 1][i]) / 2].b_);
								Fp12::mul(e2, e2, S);
							}
						}
					}
				}
		}
		clkClock.end();
		printf("GT preloop \t% 10.3fKclk\n", (clkClock.getClock() / K) / double(N));
	}
	{
		Xbyak::util::Clock clkClock;
		clkClock.begin();
		Vk=rg.get();//.set(rand(Br).getbig()->w,4);
		stPunto=M_toStr(Vk);
		cPunto=stPunto.c_str();
		for (size_t i = 0; i < N; i++) {
			hash_and_mapB(Pn[i][0],Pn[i][1],cPunto);
		}
		clkClock.end();
		printf("Hash to G1 \t% 10.3fKclk\n", (clkClock.getClock() / K) / double(N));
	}
	{
		Vk=rg.get();//.set(rand(Br).getbig()->w,4);
		stPunto=M_toStr(Vk);
		cPunto=stPunto.c_str();
		Fp T[3];
		T[2] = Fp(1);
		T[0] = T[1] = Fp(0);

		Xbyak::util::Clock clkClock;
		clkClock.begin();
		for (size_t i = 0; i < N; i++) {
			hash_and_mapDetG1(T[0], T[1], cPunto);
		}
		clkClock.end();
		printf("Hash to G1 Det\t% 10.3fKclk\n", (clkClock.getClock() / K) / double(N));
	}
	{
		Vk=rg.get();
		stPunto=M_toStr(Vk);
		cPunto=stPunto.c_str();
		Fp2 tmp[3], tmp3[3];
		tmp[2] = Fp2(1);

		Xbyak::util::Clock clkClock;
		clkClock.begin();
		for (size_t i = 0; i < N; i++) {
			hash_and_map2(tmp[0], tmp[1], cPunto);
			HashtoG2(tmp3, tmp);
		}
		clkClock.end();
		printf("Map and Hash to G2 \t% 10.3fKclk\n", (clkClock.getClock() / K) / double(N));
	}
	{
		Vk=rg.get();
		stPunto=M_toStr(Vk);
		cPunto=stPunto.c_str();
		Fp2 tmp[3], tmp3[3];
		tmp[2] = Fp2(1);

		Xbyak::util::Clock clkClock;
		clkClock.begin();
		for (size_t i = 0; i < N; i++) {
			hash_and_mapDetG2(tmp[0], tmp[1], cPunto);
			HashtoG2(tmp3, tmp);
		}
		clkClock.end();
		printf("Map and Hash to G2 Det\t% 10.3fKclk\n", (clkClock.getClock() / K) / double(N));
        }
	{
		Xbyak::util::Clock clkClock;
		clkClock.begin();
		Vk=rg.get();//.set(rand(Br).getbig()->w,4);
		stPunto=M_toStr(Vk);
		cPunto=stPunto.c_str();
		Fp2 tmp[3];
		tmp[2] = Fp2(1);
		tmp[0] = tmp[1] = Fp2(0);
		for (size_t i = 0; i < N; i++) {
			hash_and_map2(tmp[0], tmp[1], cPunto);
		}
		clkClock.end();
		printf("Map-to-Point E' \t% 10.3fKclk\n", (clkClock.getClock() / K) / double(N));
	}
	{
		Xbyak::util::Clock clkClock;
		clkClock.begin();
		Vk=rg.get();//.set(rand(Br).getbig()->w,4);
		stPunto=M_toStr(Vk);
		cPunto=stPunto.c_str();
		Fp2 tmp[3], tmp3[3];
		tmp[2] = Fp2(1);
		hash_and_map2(tmp[0], tmp[1], cPunto);
		//const mie::Vsint* WBPrec = precompute.getWB();
		//const mie::Vsint (*BBPrec)[4] = precompute.getBB();
		for (size_t i = 0; i < N; i++) {
			HashtoG2(tmp3, tmp);//, WB, BB);
		}
		clkClock.end();
		printf("Hash to G2 \t% 10.3fKclk\n", (clkClock.getClock() / K) / double(N));
	}
	M_uint k;
	{
		Xbyak::util::Clock clkClock;
		clkClock.begin();
		Vk=rg.get();//.set(rand(Br).getbig()->w,4);
		k = M_low32bit(Vk);
		k >>= 10;
		Fp P[2]; P[0] = PA[0]; P[1] = PA[1];
		//Fp Q[3];
		for (size_t i = 0; i < 10*N; i++) {
			G1_mulSmall(tmp, k, P);
		}
		clkClock.end();
		printf("G1 mul Small 1\t% 10.3fKclk\n", (clkClock.getClock() / K) / double(10*N));
	}
	std::cout<<M_low32bit(Vk)<<std::endl;
	std::cout<<k<<std::endl;
	G1_mulSmall(tmp, k, P);
	bn::ecop::NormalizeJac(tmp,tmp);
	std::cout<<tmp[0]<<","<<tmp[1]<<","<<tmp[2]<<std::endl;
	G1_mulSmall2(tmp, k, P);
	bn::ecop::NormalizeJac(tmp,tmp);
	std::cout<<tmp[0]<<","<<tmp[1]<<","<<tmp[2]<<std::endl;
	{
		Xbyak::util::Clock clkClock;
		clkClock.begin();
		Vk=rg.get();//.set(rand(Br).getbig()->w,4);
		//M_uint k;
		k = M_low32bit(Vk);
		k >>= 10;
		Fp P[2]; P[0] = PA[0]; P[1] = PA[1];
		//Fp Q[3];
		for (size_t i = 0; i < 10*N; i++) {
			G1_mulSmall2(tmp, k, P);
		}
		clkClock.end();
		printf("G1 mul Small 2\t% 10.3fKclk\n", (clkClock.getClock() / K) / double(10*N));
		
	}
	{
		Xbyak::util::Clock clkClock;
		clkClock.begin();
		Vk=rg.get();//.set(rand(Br).getbig()->w,4);
		stPunto=M_toStr(Vk);
		cPunto=stPunto.c_str();
		Fp2 tmp[3];
		tmp[2] = Fp2(1);
		tmp[0] = tmp[1] = Fp2(0);
		hash_and_map2(tmp[0], tmp[1], cPunto);

		//M_uint k;
		k = M_low32bit(Vk);
		k >>= 10;
		//Fp2 Q[3];
		for (size_t i = 0; i < 10*N; i++) {
			G2_mulSmall(tmp2, k, tmp);
		}
		clkClock.end();
		printf("G2 mul Small 1\t% 10.3fKclk\n", (clkClock.getClock() / K) / double(10*N));
	}
	std::cout<<k<<std::endl;
	G2_mulSmall(tmp2, k, Q);
	bn::ecop::NormalizeJac(tmp2,tmp2);
	std::cout<<tmp2[0]<<","<<tmp2[1]<<","<<tmp2[2]<<std::endl;
	G2_mulSmall2(tmp2, k, Q);
	bn::ecop::NormalizeJac(tmp2,tmp2);
	std::cout<<tmp2[0]<<","<<tmp2[1]<<","<<tmp2[2]<<std::endl;
	{
		Xbyak::util::Clock clkClock;
		clkClock.begin();
		Vk=rg.get();//.set(rand(Br).getbig()->w,4);
		stPunto=M_toStr(Vk);
		cPunto=stPunto.c_str();
		Fp2 tmp[3];
		tmp[2] = Fp2(1);
		tmp[0] = tmp[1] = Fp2(0);
		hash_and_map2(tmp[0], tmp[1], cPunto);

		//M_uint k;
		k = M_low32bit(Vk);
		k >>= 10;
		//Fp2 Q[3];
		for (size_t i = 0; i < 10*N; i++) {
			G2_mulSmall2(tmp2, k, tmp);
		}
		clkClock.end();
		printf("G2 mul Small 2\t% 10.3fKclk\n", (clkClock.getClock() / K) / double(10*N));
	}
	{
		Xbyak::util::Clock clkClock;
		clkClock.begin();
		Vk=rg.get();
		stPunto=M_toStr(Vk);
		cPunto=stPunto.c_str();
		Fp tmp;
		for (size_t i = 0; i < N; i++) {
			H1(tmp, cPunto);
		}
		clkClock.end();
		printf("H1 \t% 10.3fKclk\n", (clkClock.getClock() / K) / double(N));
	}
	{
		M_uint Vt;
		Vt=rg.get();
                std::string stPunto2 = "";
                const char *cPunto2;
                stPunto2 = M_toStr(Vt);
                cPunto2 = stPunto2.c_str();
                Fp T[3];
                T[2] = Fp(1);
                T[0] = T[1] = Fp(0);
                hash_and_mapDetG1(T[0], T[1], cPunto2);
                PUT(bn::ecop::isOnECJac3(T));
                int acumD = 100;
                for (int i = 0; i < 100; i++)
                {
                        Vt=rg.get();
                        stPunto2=M_toStr(Vt);
                        cPunto2=stPunto2.c_str();
                        hash_and_mapDetG1(T[0], T[1], cPunto2); T[2] = Fp(1);
                        std::cout<<bn::ecop::isOnECJac3(T);
                        if (bn::ecop::isOnECJac3(T)) acumD--;
                } std::cout<<std::endl;
		PUT(acumD);
	}
	{
		Fp2 T2[3], T2h[3];
		T2[2] = Fp2(1);
		T2[0] = T2[1] = Fp2(0);
		std::string stPunto2 = "";
		const char *cPunto2;
		M_uint Vt;
		Vt=rg.get();		
		stPunto2=M_toStr(Vt);
		cPunto2=stPunto2.c_str();
                hash_and_mapDetG2(T2[0], T2[1], cPunto2);
                HashtoG2(T2h, T2);
                PUT(bn::ecop::isOnTwistECJac3(T2h));

                int acumD = 100;
                for (int i = 0; i < 100; i++)
                {
                        Vt=rg.get();
                        stPunto2=M_toStr(Vt);
                        cPunto2=stPunto2.c_str();
                        hash_and_mapDetG2(T2[0], T2[1], cPunto2); T2[2] = 1;
                        HashtoG2(T2h, T2);
                        std::cout<<bn::ecop::isOnTwistECJac3(T2h);
                        if (bn::ecop::isOnTwistECJac3(T2h)) acumD--;
                } std::cout<<std::endl;
                PUT(acumD);
	}
	
    printf("num of test = %d (err = %d)\n", s_testNum, s_errNum);
    if (s_errNum == 0) {
        return 0;
    } else {
        return 1;
    }

} catch (std::exception& e) {
	fprintf(stderr, "ExpCode ERR:%s\n", e.what());
	return 1;
}
