/**
	@file
	@brief SHA-2 functions
    @author Zavattoni et al.
	@license modified new BSD license
	http://www.opensource.org/licenses/BSD-3-Clause
*/
#include "sha256.h"

typedef unsigned int uint32_t;
typedef sha256_st sha_st;

void sha256_init(sha_st* sha)
{
	int i;

	for (i = 0; i < 80; i++) sha->w[i] = 0L;

	sha->length[0] = sha->length[1] = 0L;

	for (i = 0; i < 8; i++) sha->h[i] = sha256_h[i];
}

void sha256_loop512(sha_st* sha)
{
	uint32_t t0, t1;
	uint32_t h[8];
	int i;

	for (i = 16; i < 64; i++)
		sha->w[i] = sha->w[i - 16] + S0(sha->w[i - 15]) + sha->w[i - 7] + S1(sha->w[i - 2]);

	for (i = 0; i < 8; i++) h[i] = sha->h[i];

	for (i = 0; i < 64; i++) {
		t0 = sha->h[7] + Sigma1(h[4]) + Che(h[4], h[5], h[6]) + sha256_k[i] + sha->w[i];
		t1 = Sigma0(h[0]) + Ma(h[0], h[1], h[2]);
		h[7] = h[6];
		h[6] = h[5];
		h[5] = h[4];
		h[4] = h[3] + t0;
		h[3] = h[2];
		h[2] = h[1];
		h[1] = h[0];
		h[0] = t0 + t1;
	}

	for (i = 0; i < 8; i++) sha->h[i] += h[i];
}

void sha256_fillchunks(sha_st* sha, int byte)
{
	int l;
	l = (int)((sha->length[0] / 32) % 16); //32-bit reg, 16 regs
	sha->w[l] <<= 8; //1 byte //all zeroes on the left.
	sha->w[l] |= (uint32_t)(byte & 0xFF);
	sha->length[0] += 8;

	if (sha->length[0] == 0L) sha->length[1]++; // carry

	if ((sha->length[0] % 512) == 0L) sha256_loop512(sha); //512-bit chunk.
}

void sha256_hash(sha_st* sha, char hash[32])
{
	int i;
	uint32_t l0, l1;
	l0 = sha->length[0];
	l1 = sha->length[1];
	sha256_fillchunks(sha, ONE_ZEROS);

	while ((sha->length[0] % 512) != 448) sha256_fillchunks(sha, ZERO);

	//so, these ones are empty, fill them with length
	sha->w[14] = l1;
	sha->w[15] = l0;
	sha256_loop512(sha);

	for (i = 0; i < 32; i++) {
		hash[i] = (char)((sha->h[i / 4] >> (8 * (3 - i % 4))) & 0xFF);
	}

	sha256_init(sha);
}
