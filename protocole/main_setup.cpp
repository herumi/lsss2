/**
	@file
	@brief Launch Setup and Offline Precomputation
    @author Zavattoni et al.
	@license modified new BSD license
	http://www.opensource.org/licenses/BSD-3-Clause
*/
#include <fstream>
#include <sstream>

#include "Alice.h"
#include "Server.h"
#include "Bob.h"
#include "bench.h"
#include "opti.h"
#include "Precompute.h"
#include "PublicKey.h"

const char* usage =
    "Usage: ./LSSS-setup [OPTION ...]\n"
    "\n"
    "Generate system parameters, a public key, and a master secret key\n"
    "for use with ./LSSS-keygen, ./LSSS-enc, and ./LSSS-dec.\n"
    "\n"
    "Output will be written to the files \"public_key\" and \"master_key\"\n"
    "\n"
    "";

void parse_args(int argc, char** argv)
{
	for (int i = 1; i < argc; i++) {
		if (!strcmp(argv[i], "-h") || !strcmp(argv[i], "--help")) {
			printf("%s", usage);
			exit(0);
		}
	}
}

std::vector<std::string> createAttributeStrings()
{
	std::vector<std::string> vec;
	std::stringstream sst;
	const std::string prefix = "att";
	const size_t numOfAttrs = 20;

	for (size_t i = 1; i <= numOfAttrs; i++) {
		sst << i;
		const std::string attr = prefix + sst.str();
		vec.push_back(attr);
		sst.clear();
		sst.str("");
	}

	return vec;
}

void setupAndGenerateData()
{
	Precompute prec;
	const std::vector<std::string> s = createAttributeStrings();
	prec.initByVector(s);
	Server server;
	/*
	  @note
	  The below section, for benchmarking, is enabled, then
	  random number generator will be updated.
	*/
	BENCH_BEGIN("step setup", BENCH_TIMES) {
		BENCH_ADD( PublicKey PK; server.Setup(PK, prec)
		           , BENCH_TIMES);
	}
	BENCH_END(BENCH_TIMES);
	/*Function Setup made by the server*/
	PublicKey PK;
	server.Setup(PK, prec);
	server.save("master_key");
	PK.save("public_key");
	prec.save("Precomputation");
}

int main(int argc, char** argv)
{
	/* initializes the BN curve */
	bn::Param::init(0);
	parse_args(argc, argv);
	cpabe::Param::init();
	/*
	  System initialization, setup external libraries,
	  is done until here.
	*/
	setupAndGenerateData();
}


/***
    Local Variables:
    c-basic-offset: 4
    indent-tabs-mode: t
    tab-width: 4
    End:
*/
