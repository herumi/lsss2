/* -*- mode: c++ -*- */
/**
	@file
	@brief Launch Decryption (G2 version)
    @author Zavattoni et al.
	@license modified new BSD license
	http://www.opensource.org/licenses/BSD-3-Clause
*/
#include <iostream>
#include <sstream>
#include <fstream>

#include "Alice.h"
#include "ServerG2.h"
#include "Bob.h"
#include "bench.h"
#include "MatrizLSSS.h"
#define BENCH_TIMES	50
    #include "random.h"
const char* usage =
    "Usage: ./LSSS-decG2 [OPTION ...]\n"
    "\n"
    "Decrypt FILE using private key UserKey and assuming public key\n"
    "public_key.\n\n"
    "\n"
    "Mandatory arguments to long options are mandatory for short options too.\n\n"
    " -h, --help               print this message\n\n"
    "\n"
    "";

using namespace std;

void parse_args(int argc, char** argv)
{
	for (int i = 1; i < argc; i++ ) {
		if (!strcmp(argv[i], "-h") || !strcmp(argv[i], "--help")) {
			printf("%s", usage);
			exit(0);
		}
	}
}

int main(int argc, char** argv)
{
	parse_args(argc, argv);
	/* initializes the BN curve */
	bn::ParamT<Fp2>::init(0);
	ifstream fCt, fSk;
	fCt.open("CypherText");
	CypherTextG2 CT;
	fCt >> CT;
	fCt.close();
	MatrizLSSS M;
	MatrizType mlsss;
	ifstream fMatriz;
	fMatriz.open("matriz");
	fMatriz >> M;
	fMatriz.close();
	mlsss = M.getMatrixType();
	fSk.open (argv[1]);
	
	if (fSk.is_open()) {
		KeyG2 UserKey;
		fSk >> UserKey;
		fSk.close();
		Bob bob;
		ifstream fPrec;
		PrecomputeG2 prec;
		fPrec.open("Precomputation");
		fPrec >> prec;
		fPrec.close();
		Fp Key = 0;
		Key = bob.decG2(&CT, mlsss, UserKey, prec, &M);

		if (Key == 0) {
			printf("attributes don't pass the tree\n");
			return 1;

		} else {
			PUT(Key);
			// @note work around checking
			std::string s = "112233445566778899";
			std::stringstream sst;
			sst << Key;

			if (s != sst.str()) {
				puts("result is not equal to expected value");
				return 1;
			}
		}

//*
		BENCH_BEGIN("step decrypt", BENCH_TIMES) {
			BENCH_ADD( bob.decG2(&CT, mlsss, UserKey, prec, &M);
			           , BENCH_TIMES);
		}
		BENCH_END(BENCH_TIMES);
// */

	} else {
		std::cout << "Error opening file" << std::endl;
	}

	return 0;
}

/***
    Local Variables:
    c-basic-offset: 4
    indent-tabs-mode: t
    tab-width: 4
    End:
*/

