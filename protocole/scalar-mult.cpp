/**
	@file
	@brief Test G2 scalar-point multiplication
    @author Zavattoni et al.
	@license modified new BSD license
	http://www.opensource.org/licenses/BSD-3-Clause
*/
#include <stdio.h>
#include <memory.h>
#include "xbyak/xbyak_util.h"
extern Xbyak::util::Clock sclk;
#include "bn.h"
#include <iostream>
#include "util.h"
#include "opti.h"

#include "bn-multi.h"
#include "random.h"
extern uint64_t PT[8][256];

using namespace bn;
using namespace ecop;

static int s_errNum = 0;
static int s_testNum = 0;

#define TEST_EQUAL(x, y) { s_testNum++; if (x != y) { s_errNum++; printf("%s(%d): err %s != %s\n", __FILE__, __LINE__, #x, #y); std::cout << "lhs=" << (x) << "\nrhs=" << (y) << std::endl; } }
#define TEST_ASSERT(x) { s_testNum++; if (!(x)) { s_errNum++; printf("%s(%d): err assert %s\n", __FILE__, __LINE__, #x); } }

#define FATAL_EXIT(msg) { printf("%s(%d): err %s\n", __FILE__, __LINE__, msg); exit(1); }

const M_uint& p = M_param_p();
const M_uint& r = M_param_r();

#define P_INIT Fp(2), Fp("16740896641879863340107777353588575149660814923656713498672603551465628253431")

#define P2_INIT Fp("13438486984812665827952643313785387127911349697151255879146972340858203091778"), Fp("12741389316352206200828479361093127917015298445269456291074734498988157668221")

M_uint pm1o4;
int pow2i[65];
M_uint pow2w_1i[4];
int ipow2w_1i[4];

void G2Operations()
{
  const size_t N = 10000;
  const size_t K = 1000;

  Fp2 Q0[3];
  Q0[0]=Fp2(Fp("6223258573959374160388148138515068413463808374382904056166846479839437283179"),Fp("1333677181606561741263147618884774023879777504350792897443644297132133116760"));
  Q0[1]=Fp2(Fp("15448069458242091293797671350465853383868332221085553043065407654696213272343"),Fp("5988972321581320818364157621890995344611715545763882542498818720594534755038"));
  Q0[2]=Fp2(Fp("1"),Fp("0"));

  Fp2 Q3[]= {Fp2(Fp("16078745797517204416536047345108963426180929352523687780879944136677887850920"), Fp("13741732381062839667309601883674719699992528991173099693690893730898121669377")),
             Fp2(Fp("1000111743260271299562572635045641989113444435201231117183696796510031419626"), Fp("8709084883629411557146433485681162012476118870656559311612969796519071901272")),
             Fp2(Fp("1"), Fp("0"))};

  Fp2 Q2[3] = {Fp2(Fp("0"), Fp("0")), Fp2(Fp("0") , Fp("0")), Fp2(Fp("1"),Fp("0"))};
  Fp2 Q3p[] = {Q3[0], Q3[1], Q3[2]};

  Xbyak::util::Clock clkAddJJAG2;
  clkAddJJAG2.begin();
  for (size_t i = 0; i < N; i++) {
    addJJAG2c(Q2,Q0,Q3p);
  }
  clkAddJJAG2.end();
  printf("G2 Add JJA\t% 10.2fKclk\n", (clkAddJJAG2.getClock() / K) / double(N));
  TEST_ASSERT(bn::ecop::isOnTwistECJac3(Q2));

  Xbyak::util::Clock clkAddJJJG2;
  clkAddJJJG2.begin();
  for (size_t i = 0; i < N; i++) {
    addJJJG2(Q2,Q0,Q3p);
  }
  clkAddJJJG2.end();
  printf("G2 Add JJJ\t% 10.2fKclk\n", (clkAddJJJG2.getClock() / K) / double(N));
  TEST_ASSERT(bn::ecop::isOnTwistECJac3(Q2));

  Fp2 Q1[3],Q4[3]; Fp2 Q4_[3],Q0_[3];
  Q4[0]=Q3[0]; Q4[1]=Q3[1]; Q4[2]=1;

  {
    M_uint Vk;
    RandInt rg;

    Vk=rg.get();
    ScalarMult(Q0_,Q0,Vk);
    Vk=rg.get();
    ScalarMult(Q4_,Q4,Vk);

    TEST_ASSERT(bn::ecop::isOnTwistECJac3(Q0_));
    TEST_ASSERT(bn::ecop::isOnTwistECJac3(Q4_));

    addJJJG2(Q2,Q0_,Q4_);
    bn::ecop::ECAdd(Q1,Q0_,Q4_);
  }

  Xbyak::util::Clock clkdblG2;
  clkdblG2.begin();
  for (size_t i = 0; i < N; i++) {
    dblJG2(Q2,Q0);
  }
  clkdblG2.end();
  printf("G2 Dbl J\t% 10.2fKclk\n", (clkdblG2.getClock() / K) / double(N));
  TEST_ASSERT(bn::ecop::isOnTwistECJac3(Q2));

  Xbyak::util::Clock clkdblAG2;
  clkdblAG2.begin();
  for (size_t i = 0; i < N; i++) {
    bn::ecop::ECDouble(Q2,Q0);
  }
  clkdblAG2.end();
  printf("G2 Dbl A\t% 10.2fKclk\n", (clkdblAG2.getClock() / K) / double(N));
  TEST_ASSERT(bn::ecop::isOnTwistECJac3(Q2));
}

int main(int argc, char *argv[])
{
	argc--, argv++;
	int mode = -1;
	if (argc > 1 && strcmp(*argv, "-m") == 0) {
		mode = atoi(argv[1]);
	}
	Param::init(mode);

    G2Operations();

  return 0;
}
