/**
	@file
	@brief Miscelaneous functions
    @author Zavattoni et al.
	@license modified new BSD license
	http://www.opensource.org/licenses/BSD-3-Clause
*/
#include "opti.h"


int myPow(int x, int p)
{
	if (p == 0) return 1;
	if (p == 1) return x;

	int tmp = myPow(x, p / 2);

	if (p % 2 == 0) return tmp * tmp;
	else return x * tmp * tmp;
}

////
// Jacobians<- Jacobians + Jacobians
//http://hyperelliptic.org/EFD/g1p/auto-shortw-jacobian-0.html#addition-add-2007-bl
void addJJJG2(Fp2* c, const Fp2* a, const Fp2* b)
{
    if (a[2].isZero()) {
		if ((a[0] == 1) && (a[1] == 1)) {
			if (b[2].isZero()) {
				if ((b[0] == 1) && (b[1] == 1)) {
					c[0] = 1; c[1] = 1; c[2] = 0; return;

				} else {
					c[0] = b[0];
					c[1] = b[1];
					c[2] = b[2];
					return;
				}

			} else {
				c[0] = b[0];
				c[1] = b[1];
				c[2] = b[2];
				return;
			}
		}

	} else if (b[2].isZero())
		if ((b[0] == 1) && (b[1] == 1)) {
			c[0] = a[0]; c[1] = a[1]; c[2] = a[2]; return;
		}
	bn::ecop::ECAdd(c, a, b);
}
void dblJG1(mie::Fp* c, const mie::Fp* a)
{
	using namespace mie;
#if 1

	if (a[2].isZero())
		if ((a[0] == 1) && (a[1] == 1)) {
			c[0] = a[0];
			c[1] = a[1];
			c[2] = a[2];
			return;
		}

	bn::ecop::ECDouble(c, a);
	return;
#endif
	Fp t0, t1, t2, t3, t4;

	//if ((a[0]==1)&&(a[1]==1)&&(a[2]==0)) {c[0]=a[0];c[1]=a[1];c[2]=a[2]; return;}
	if (a[2].isZero())
		if ((a[0] == 1) && (a[1] == 1)) {
			c[0] = a[0];
			c[1] = a[1];
			c[2] = a[2];
			return;
		}

	//Fp t0,t1,t2,t3,t4;
	Fp::square(t2, a[0]);
	Fp::square(t0, a[1]);
	Fp::square(t1, t0);
	Fp::add(t0, a[0], t0);
	Fp::square(t0, t0);
	Fp::sub(t0, t0, t2);
	Fp::sub(t0, t0, t1);
	Fp::add(t0, t0, t0); //2*t3;
	//Fp::mul3(t2,t2);
	//t2=t2+t2+t2;
	Fp::add(t4, t2, t2);
	Fp::add(t2, t4, t2);
	Fp::square(t3, t2);
	Fp::add(t4, t0, t0); //2*D
	Fp::sub(c[0], t3, t4);
	Fp::sub(t0, t0, c[0]);
	//Fp::mul8(t1,t1);
	//t1=8*t1;
	Fp::add(t4, t1, t1);
	Fp::add(t4, t4, t4);
	Fp::add(t1, t4, t4);
	Fp::mul(t0, t2, t0);
	Fp::sub(c[1], t0, t1);
	Fp::mul(t0, a[1], a[2]);
	Fp::add(c[2], t0, t0); //2*t8
}
void addJJJG1(mie::Fp* c, const mie::Fp* a, const mie::Fp* b)
{
	using namespace mie;
	bn::ecop::ECAdd(c, a, b);
}
void G1_mulSmallJ(mie::Fp* R, const M_uint& k, const mie::Fp* PA)
{
	using namespace mie;
	Fp PsI[PRECOMPUTE][3];
	Fp P[3]; P[0] = PA[0]; P[1] = PA[1]; P[2] = PA[2];
	int i, j;
	int v[2][70];

	for (i = 0; i < 2; i++)for (j = 0; j < 70; j++) v[i][j] = 0;

	int tlent = 0;
	NAFw64(v[0], v[1], tlent, k, WNAF);
	precomputeG1SmallJ(PsI, P, k);
	Fp S[3]; Fp R2[3];
	R2[0] = 1; R2[1] = 1; R2[2] = 0;
	R[0] = P[0]; R[1] = P[1]; R[2] = P[2];

	for (i = tlent - 2; i >= 0; i--) {
		dblJG1(R2, R); //bn::ecop::ECDouble(R2,R);

		if (R2[0] == 0) std::cout << ":(" << std::endl;

		R[0] = R2[0]; R[1] = R2[1]; R[2] = R2[2];

		if (v[0][i] > 0) {
			S[0] = PsI[(v[0][i] - 1) >> 1][0];
			S[1] = PsI[(v[0][i] - 1) >> 1][1];
			S[2] = PsI[(v[0][i] - 1) >> 1][2];
			addJJJG1(R, R, S);
		}

		if (v[1][i] > 0) {
			S[0] = PsI[(v[1][i] - 1) >> 1][0];
			S[1] = -PsI[(v[1][i] - 1) >> 1][1];
			S[2] = PsI[(v[0][i] - 1) >> 1][2];
			addJJJG1(R, R, S);
		}
	}
}
void G1_mulSmall(mie::Fp* R, const M_uint& k, const mie::Fp* PA)
{
	using namespace mie;
	bn::ecop::NormalizeJac(const_cast<Fp*>(PA), PA); // QQQ
//std::cout<<k<<std::endl;
	Fp PsI[PRECOMPUTE][2];
	Fp P[2]; P[0] = PA[0]; P[1] = PA[1];
	int i, j;
	int v[2][70];

	for (i = 0; i < 2; i++)for (j = 0; j < 70; j++) v[i][j] = 0;

	int tlent = 0;
	NAFw64(v[0], v[1], tlent, k, WNAF);
	//for (i=0;i<2;i++){for (j=0;j<tlent;j++) std::cout<<v[i][j]<<","; std::cout<<endl;}
	precomputeG1Small(PsI, P, k);
	Fp S[2]; Fp R2[3];
	R[0] = 1; R[1] = 1; R[2] = 0;

	for (i = tlent - 1; i >= 0; i--) {
		dblJG1(R2, R); //bn::ecop::ECDouble(R2,R);
		R[0] = R2[0]; R[1] = R2[1]; R[2] = R2[2];

		if (v[0][i] > 0) {
			S[0] = PsI[(v[0][i] - 1) >> 1][0];
			S[1] = PsI[(v[0][i] - 1) >> 1][1];
			addJJAG1b(R, R, S);
		}

		if (v[1][i] > 0) {
			S[0] = PsI[(v[1][i] - 1) >> 1][0];
			S[1] = -PsI[(v[1][i] - 1) >> 1][1];
			addJJAG1b(R, R, S);
		}
	}
}
void G1_mulSmall2(mie::Fp* R, const M_uint& k, const mie::Fp* PA)
{
	using namespace mie;
	bn::ecop::NormalizeJac(const_cast<Fp*>(PA), PA); // QQQ

	Fp P[2]; P[0] = PA[0]; P[1] = PA[1];
	int i, j;
	int v[2][70];

	for (i = 0; i < 2; i++)for (j = 0; j < 70; j++) v[i][j] = 0;

	int tlent = 0;
	NAFw64(v[0], v[1], tlent, k, 2);//WNAF);

	Fp S[2]; Fp R2[3];
	R[0] = 1; R[1] = 1; R[2] = 0;

	for (i = tlent - 1; i >= 0; i--) {
		dblJG1(R2, R);
		R[0] = R2[0]; R[1] = R2[1]; R[2] = R2[2];

		if (v[0][i] > 0) {
			addJJAG1b(R, R, P);
		}

		if (v[1][i] > 0) {
			S[0] = P[0];
			S[1] = -P[1];
			addJJAG1b(R, R, S);
		}
	}
}
void G1_mul(mie::Fp* R, const M_uint& Vk, const mie::Fp* PA, const M_sint* WB, const M_sint (*BB)[2], const M_sint& Beta)
{
	using namespace mie;
#if 0
	bn::ecop::ScalarMult(R, PA, Vk);
	return;
#else
  if (M_bitLen(Vk) < 31) {
      G1_mulSmall2(R, Vk, PA);
	  return;
  }
//  } else {
		M_sint u[2];
		if (PA[2]!=1)
		    bn::ecop::NormalizeJac(const_cast<Fp*>(PA), PA); // QQQ
		Fp P[2]; P[0] = PA[0]; P[1] = PA[1];
		Fp PsI[2][PRECOMPUTE][2];
		int i, j;
		int v[4][130];

		for (i = 0; i < 4; i++)for (j = 0; j < 130; j++) v[i][j] = 0;

		int tlen[2], tlent = 0;
		DecompG1(u, Vk, WB, BB);

//for (int i=0;i<2;i++) std::cout<<u[i]<<","; std::cout<<std::endl; std::cout<<Vk<<std::endl;
		for (i = 0; i < 2; i++) tlen[i] = 130;

		NAFw2(v[0], v[1], tlen[0], M_abs(u[0]), WNAF); if (tlen[0] > tlent) tlent = tlen[0];

		NAFw2(v[2], v[3], tlen[1], M_abs(u[1]), WNAF); if (tlen[1] > tlent) tlent = tlen[1];

#if 0
		Fp Q[2];
		/*if (Beta<0)*/ //Fp::neg(Q[0],Q[0]);//Beta is negative //non it's not for this curve, the conditional is required...
		Q[0] = P[0]; Q[1] = P[1];
		M_uint BetaU = Beta.get();
		Fp::mul(Q[0], Q[0], BetaU);
		precomputeG1(PsI, P, Q, u);
#else
		precomputeG1b(PsI, P, Beta);

		for (i = 0; i < 2; i++) if (u[i] < 0) for (j = 0; j < PRECOMPUTE; j++) PsI[i][j][1] = -PsI[i][j][1];

#endif
		//for (j=0;j<PRECOMPUTE;j++) Fp::neg(PsI[1][j][1],PsI[1][j][1]);
		Fp S[2]; Fp S2[3], R2[3];
		R[0] = 1; R[1] = 1; R[2] = 0;

		for (i = tlent - 1; i >= 0; i--) {
			dblJG1(R2, R); //bn::ecop::ECDouble(R2,R);
			R[0] = R2[0]; R[1] = R2[1]; R[2] = R2[2];

			for (j = 0; j <= 3; j += 2) {
				if (i <= tlen[j / 2] - 1) {
					if (v[j][i] > 0) {
						S[0] = PsI[j / 2][(v[j][i] - 1) >> 1][0];
						S[1] = PsI[j / 2][(v[j][i] - 1) >> 1][1];
						S2[0] = S[0]; S2[1] = S[1]; S2[2] = 1;
						addJJAG1b(R, R, S);
					}

					if (v[j + 1][i] > 0) {
						S[0] = PsI[j / 2][(v[j + 1][i] - 1) >> 1][0];
						S[1] = -PsI[j / 2][(v[j + 1][i] - 1) >> 1][1];
						S2[0] = S[0]; S2[1] = S[1]; S2[2] = 1;
						addJJAG1b(R, R, S);
					}
				}
			}
		}
//	}

#endif
}

void G1_mulknown(mie::Fp* R, const M_uint& Vk, const mie::Fp PsI[2][PRECOMPUTEknown][2], const M_sint* WB, const M_sint (*BB)[2])
{
	using namespace mie;
#if 0
	Fp PA[3]; PA[0] = PsI[0][0][0]; PA[1] = PsI[0][0][1]; PA[2] = 1;
	bn::ecop::ScalarMult(R, PA, Vk);
	return;
#else
	M_sint u[2];
	int i, j;
	//std::cout<<"Vk="<<Vk<<")\n";
	int v[4][130];

	for (i = 0; i < 4; i++)for (j = 0; j < 130; j++) v[i][j] = 0;

	int tlen[2], tlent = 0;
	DecompG1(u, Vk, WB, BB); // std::cout<<"("<<u[0]<<","<<u[1]<<")\n";

	for (i = 0; i < 2; i++) tlen[i] = 130;

	NAFw128(v[0], v[1], tlen[0], M_abs(u[0]), WNAFknown); if (tlen[0] > tlent) tlent = tlen[0];

	NAFw128(v[2], v[3], tlen[1], M_abs(u[1]), WNAFknown); if (tlen[1] > tlent) tlent = tlen[1];

	//or (i=0;i<4;i++) {for (j=0;j<tlen[i/2];j++) std::cout<<v[i][j]<<","; std::cout<<":"<<tlen[i/2]<<std::endl;}
	//precomputeG1(PsI,P,Q,u);

	// recover Psi at the end of function
	for (j = 0; j < PRECOMPUTEknown; j++)
		Fp::neg(const_cast<Fp&>(PsI[1][j][1]), PsI[1][j][1]);

	//for (i=0;i<2;i++) {for (j=0;j<PRECOMPUTEknown;j++) std::cout<<PsI[i][j][0]<<","<<PsI[i][j][1]<<std::endl; std::cout<<"\n";}
	Fp S[2]; Fp S2[3], R2[3];
	R[0] = 1; R[1] = 1; R[2] = 0;

	for (i = tlent - 1; i >= 0; i--) {
		dblJG1(R2, R); //bn::ecop::ECDouble(R2,R);
		R[0] = R2[0]; R[1] = R2[1]; R[2] = R2[2];

		for (j = 0; j <= 3; j += 2) {
			if (i <= tlen[j / 2] - 1) {
				if (v[j][i] > 0) {
					S[0] = PsI[j / 2][(v[j][i] - 1) >> 1][0];
					S[1] = PsI[j / 2][(v[j][i] - 1) >> 1][1];
					S2[0] = S[0]; S2[1] = S[1]; S2[2] = 1;
					addJJAG1b(R, R, S);
				}

				if (v[j + 1][i] > 0) {
					S[0] = PsI[j / 2][(v[j + 1][i] - 1) >> 1][0];
					S[1] = -PsI[j / 2][(v[j + 1][i] - 1) >> 1][1];
					S2[0] = S[0]; S2[1] = S[1]; S2[2] = 1;
					addJJAG1b(R, R, S);
				}
			}
		}
	}

	for (j = 0; j < PRECOMPUTEknown; j++)
		Fp::neg(const_cast<Fp&>(PsI[1][j][1]), PsI[1][j][1]);

#endif
}



void DecompG1(M_sint* u, const M_uint& Vk, const M_sint* WB, const M_sint (*BB)[2])
{
	using namespace mie;
	int i, j;
	M_sint v0[2];
	//for (i=0;i<2;i++) mad(v0[i],WB[i],Vk);
	mad(v0[0], WB[0], Vk);
	mad(v0[1], WB[1], Vk);
	u[0] = Vk; u[1] = 0;

	for (i = 0; i < 2; i++)
		for (j = 0; j < 2; j++)
			u[i] = u[i] - v0[j] * BB[j][i];
}

void precomputeG1SmallJ(mie::Fp QsI[][3], const mie::Fp* P, const M_sint& u)
{
	using namespace mie;
	//int i;
	Fp Qo[3];
	//Fp ux[PRECOMPUTE],uy[PRECOMPUTE]; for (i=0;i<PRECOMPUTE;i++) ux[i]=uy[i]=0;
	QsI[0][0] = P[0]; QsI[0][1] = P[1]; QsI[0][2] = P[2];

	if (u < 0) {
		QsI[0][1] = -QsI[0][1]; /*u=-u;*/
	}

	Qo[0] = QsI[0][0]; Qo[1] = QsI[0][1]; Qo[2] = QsI[0][2];
	bn::ecop::ECDouble(Qo, QsI[0]);
	bn::ecop::ECAdd(QsI[1], Qo, QsI[0]);
}

void precomputeG1Small(mie::Fp QsI[][2], const mie::Fp* P, const M_sint& u)
{
	using namespace mie;
	int i, j;
	Fp Qo[2];

	Fp ux[PRECOMPUTE], uy[PRECOMPUTE]; for (i = 0; i < PRECOMPUTE; i++) ux[i] = uy[i] = 0;

	QsI[0][0] = P[0]; QsI[0][1] = P[1];

	if (u < 0) {
		QsI[0][1] = -QsI[0][1]; /*u=-u;*/
	}

	Qo[0] = QsI[0][0]; Qo[1] = QsI[0][1];
	precompute(ux, uy, Qo, PRECOMPUTE);

	for (j = 1; j < PRECOMPUTE; j++) {
		QsI[j][0] = ux[j - 1]; QsI[j][1] = uy[j - 1];
	}
}

void precomputeG1(mie::Fp QsI[2][PRECOMPUTE][2], const mie::Fp P[2], const mie::Fp Q[2], M_sint u[2])
{
	using namespace mie;
	int i, j; int m = 2;
	Fp Qo[2];

	Fp ux[PRECOMPUTE], uy[PRECOMPUTE]; for (i = 0; i < PRECOMPUTE; i++) ux[i] = uy[i] = 0;

	QsI[0][0][0] = P[0]; QsI[0][0][1] = P[1];
	QsI[1][0][0] = Q[0]; QsI[1][0][1] = Q[1];

	for (i = 0; i < m; i++) if (u[i] < 0) {
			QsI[i][0][1] = -QsI[i][0][1];
			u[i] = -u[i];
		}

	for (i = 0; i < m; i++) {
		Qo[0] = QsI[i][0][0]; Qo[1] = QsI[i][0][1];
		precompute(ux, uy, Qo, PRECOMPUTE);

		for (j = 1; j <= PRECOMPUTE - 1; j++) {
			QsI[i][j][0] = ux[j - 1]; QsI[i][j][1] = uy[j - 1];
		}
	}
}

void precomputeG1b(mie::Fp QsI[2][PRECOMPUTE][2], const mie::Fp* P, const M_sint& Beta)
{
	int j;
	QsI[0][0][0] = P[0]; QsI[0][0][1] = P[1];
#if 0
	mie::Fp Qo[2];

	mie::Fp ux[PRECOMPUTE], uy[PRECOMPUTE]; for (j = 0; j < PRECOMPUTE; j++) ux[j] = uy[j] = 0;

	Qo[0] = P[0]; Qo[1] = P[1];
	precompute(ux, uy, Qo, PRECOMPUTE);

	for (j = 1; j < PRECOMPUTE; j++) {
		QsI[0][j][0] = ux[j - 1]; QsI[0][j][1] = uy[j - 1];
	}

#else
	mie::Fp Qi[3], Qs[3];
	Qs[0] = QsI[0][0][0]; Qs[1] = QsI[0][0][1]; Qs[2] = 1;
	bn::ecop::ECDouble(Qi, Qs);
	bn::ecop::ECAdd(Qi, Qi, Qs);
	bn::ecop::NormalizeJac(Qi, Qi);
	QsI[0][1][0] = Qi[0]; QsI[0][1][1] = Qi[1];
#endif

	Fp abs_Beta = M_toFp(M_abs(Beta));
	for (j = 0; j < PRECOMPUTE; j++) {
		mie::Fp::mul(QsI[1][j][0], QsI[0][j][0], abs_Beta);
		//mie::Fp::neg(QsI[1][j][0],QsI[1][j][0]);
		QsI[1][j][1] = QsI[0][j][1];
		//mie::Fp::neg(QsI[1][j][1],QsI[0][j][1]);
	}
}

void precomputeG1known(mie::Fp QsI[2][PRECOMPUTEknown][2], const mie::Fp* P, const M_sint& Beta)
{
	mie::Fp Qo[2];
	mie::Fp ux[PRECOMPUTEknown], uy[PRECOMPUTEknown];
	QsI[0][0][0] = P[0];
	QsI[0][0][1] = P[1];
	Qo[0] = P[0];
	Qo[1] = P[1];
	precompute(ux, uy, Qo, PRECOMPUTEknown);

	for (int j = 1; j < PRECOMPUTEknown; j++) {
		QsI[0][j][0] = ux[j - 1];
		QsI[0][j][1] = uy[j - 1];
	}

	const Fp abs_Beta = M_toFp(M_abs(Beta));
	for (int j = 0; j < PRECOMPUTEknown; j++) {
		mie::Fp::mul(QsI[1][j][0], QsI[0][j][0], abs_Beta);
		//mie::Fp::neg(QsI[1][j][0],QsI[1][j][0]);
		QsI[1][j][1] = QsI[0][j][1];
	}
}



////////////////////////////////////
// New precomputation
////////////////////////////////////

//Alg. 3.44
// t=Log(2,r), w=8, d=t/w=32

//This one was cobbled for a single purpose. I don't warranty it works for you.
/*
static inline uint32_t log2(const uint32_t x)
{
	uint32_t y;
	asm ( "\tbsr %1, %0\n"
	      : "=r"(y)
	      : "r" (x)
	    );
	return y;
}
// */

void precomputeG1knownC(mie::Fp PP[256][2],
                        const mie::Fp* P /*  @note TODO: check that it can be constant, or not */,
                        const M_sint* WB, const M_sint (*BB)[2], M_sint Beta)
{
	int t = 256, d = 32;
	int i, j;
	M_uint td;
	int p, p2;
	mie::Fp PA[3];
	mie::Fp PQ[256][3];
	PQ[0][0] = 1;    PQ[0][1] = 1;    PQ[0][2] = 1;
	PQ[1][0] = P[0]; PQ[1][1] = P[1]; PQ[1][2] = 1;
	td = pow(2, d);
	G1_mul(PA, td, PQ[1], WB, BB, Beta);
	PQ[2][0] = PA[0]; PQ[2][1] = PA[1]; PQ[2][2] = PA[2];
	p = 2;

	for (i = 3; i < t; i++) {
		if (i == (1 << p)) {
			G1_mul(PA, td, PQ[(1 << (p - 1))], WB, BB, Beta);
			PQ[i][0] = PA[0];
			PQ[i][1] = PA[1];
			PQ[i][2] = PA[2];
			p++;
			continue;

		} else {
			PQ[i][0] = PQ[(1 << (p - 1))][0]; PQ[i][1] = PQ[(1 << (p - 1))][1]; PQ[i][2] = PQ[(1 << (p - 1))][2];
			p2 = i;

			const int log2_i = (int)log2((unsigned)i);
			for (j = 0; j < log2_i; j++) {
				if (p2 & 1) {
					addJJJG1(PA, PQ[i], PQ[(1 << j)]);
					PQ[i][0] = PA[0]; PQ[i][1] = PA[1]; PQ[i][2] = PA[2];
				}

				p2 >>= 1;
			}
		}
	}

	bn::MultiNormalizeFp(PP, PQ, 256);
	PP[0][0] = 0; PP[0][1] = 1;
}

void precomputeG1knownSC(mie::Fp PP[PRECOMPUTEknownS][2],
						const mie::Fp* P /*  @note TODO: check that it can be constant, or not */,
						const M_sint *WB, const M_sint (*BB)[2], M_sint Beta){
  int t=256,d=32;
  int i,j;
  M_uint td;
  int p,p2;
  mie::Fp PA[3];
  mie::Fp PQ[t][3];

  PQ[0][0]=1;    PQ[0][1]=1;    PQ[0][2]=1;
  PQ[1][0]=P[0]; PQ[1][1]=P[1]; PQ[1][2]=1;

  td=pow(2,d);
  G1_mul(PA,td,PQ[1],WB,BB,Beta);
  PQ[2][0]=PA[0]; PQ[2][1]=PA[1]; PQ[2][2]=PA[2];
  p=2;
  for (i=3;i<t;i++) {
    if (i==(1<<p)) {
      G1_mul(PA,td,PQ[(1<<(p-1))],WB,BB,Beta);
      PQ[i][0]=PA[0];
      PQ[i][1]=PA[1];
      PQ[i][2]=PA[2];
      p++;
      continue;
    } else {
      PQ[i][0]=PQ[(1<<(p-1))][0]; PQ[i][1]=PQ[(1<<(p-1))][1]; PQ[i][2]=PQ[(1<<(p-1))][2];
      p2=i;
      const int log2_i = (int)log2((unsigned)i);
      for (j=0;j<log2_i;j++){
        if (p2&1) {
          addJJJG1(PA,PQ[i],PQ[(1<<j)]);
          PQ[i][0]=PA[0]; PQ[i][1]=PA[1]; PQ[i][2]=PA[2];
        }
        p2>>=1;
      }
    }
  }

  for (i=0;i<PRECOMPUTEknownS;i++){
    PA[0]=PQ[t-1-i][0];
    mie::Fp::neg(PA[1],PQ[t-1-i][1]);
    PA[2]=PQ[t-1-i][2];
    addJJJG1(PQ[i],PQ[i],PA);
  }

  bn::MultiNormalizeFp(PP,PQ,PRECOMPUTEknownS);
  PA[0]=PQ[255][0];PA[1]=PQ[255][1];PA[2]=PQ[255][2];
  bn::ecop::NormalizeJac(PA,PA);
  PP[0][0]=PA[0]; mie::Fp::neg(PP[0][1],PA[1]);
  //PP[0][0]=0; PP[0][1]=1;
}

void RecodeForPrecompC2(unsigned char out[32], const M_uint& Vk, const uint64_t PT[8][256])
{
	//d=32,w=8
	uint64_t acum[4];
	unsigned char t0[8][4];//, tsalida[32];
	int i, j;
	M_uint Vl = Vk;

	//Break into w elements of d-bits. Note: M_uint is 4 WORDS of 64-bits
	for (i = 0; i < 8; i++) {
		for (j = 0; j < 4; j++) {
			t0[i][j] = M_block(Vl, 0) & 0xFF;
			Vl >>= 8;
		}
	}

	for (j = 0; j < 4; j++) {
		acum[j] = 0;

		for (i = 0; i < 8; i++) {
			acum[j] ^= PT[i][t0[i][j]];
		}
	}

	//rc.set(acum,4); <-we should be storing into a more elegant M_uint...
	for (i = 0; i < 32; i++) {
		out[i] = acum[i / 8] & 0xFF;
		acum[i / 8] >>= 8;
	}

	return;
}

void RecodeForPrecompSC(unsigned char out[32], const M_uint &Vk, const uint64_t PT[8][256], const M_uint & t256, const M_uint &Half_Zr){
  //d=32,w=8

  M_uint Vl=Vk;

  //Vl+=t256; Vl-=1; Vl*=Half_Zr; Vl%=M_param_r();
  Vl=((Vk+t256-1)*Half_Zr); Vl = Vl % M_param_r();
  RecodeForPrecompC2(out,Vl,PT);
}


void G1_mulknownC(mie::Fp* R, const M_uint& Vk, const mie::Fp PP[256][2], const uint64_t PT[8][256])
{
	//d=32;
	mie::Fp Q[3];
	R[0] = R[1] = 1; R[2] = 0;
	int i;
	unsigned char acum[32];
	RecodeForPrecompC2(acum, Vk, PT);

	for (i = 31; i >= 0; i--) {
		dblJG1(Q, R);
		addJJAG1b(R, Q, PP[acum[i]]);
	}
}

void G1_mulknownSC(mie::Fp *R, const M_uint &Vk, const mie::Fp PP[PRECOMPUTEknownS][2], const uint64_t PT[8][256], const M_uint & t256, const M_uint &Half_Zr)
{
    //d=32;
    mie::Fp Q[3];
    mie::Fp QA[2];
    R[0] = R[1] = 1; R[2] = 0;

    int i;
    unsigned char acum[32];
    RecodeForPrecompSC(acum, Vk, PT, t256, Half_Zr);

    for (i = 31; i >= 0; i--) {
        dblJG1(Q,R);
        if ((acum[i] & 0x80) == 0x80) {
            QA[0]=PP[acum[i]^0xFF][0];
            QA[1]=PP[acum[i]^0xFF][1];
            mie::Fp::neg(QA[1],QA[1]);
            addJJAG1b(R,Q,QA);
        } else {
            addJJAG1b(R,Q,PP[acum[i]]);
        }
    }
    //mie::Fp::neg(R[1],R[1]);
}

////////////////////////////////////////////////////////////
// G2 part
////////////////////////////////////////////////////////////
// Copy from the G1 version once it is optimized.
void precomputeG2knownC(Fp2 PQ[256][2], const Fp2 Q[2], const M_sint* WB, const M_sint (*BB)[4])
{
	int t = 256, d = 32;
	int i, j;
	M_uint td;
	int p, p2;
	Fp2 QQ[256][3];
	QQ[0][0] = 1;    QQ[0][1] = 1;    QQ[0][2] = 1;
	QQ[1][0] = Q[0]; QQ[1][1] = Q[1]; QQ[1][2] = 1;
	td = pow(2, d);
	G2_mul(QQ[2], QQ[1], td, WB, BB);
	bn::ecop::NormalizeJac(QQ[2], QQ[2]);
	p = 2;

	for (i = 3; i < t; i++) {
		if (i == (1 << p)) {
			G2_mul(QQ[i], QQ[(1 << (p - 1))], td, WB, BB);
			bn::ecop::NormalizeJac(QQ[i], QQ[i]);
			p++;
			continue;

		} else {
			QQ[i][0] = QQ[(1 << (p - 1))][0]; QQ[i][1] = QQ[(1 << (p - 1))][1]; QQ[i][2] = QQ[(1 << (p - 1))][2];
			p2 = i;

			const int log2_i = (int)log2((unsigned)i);
			for (j = 0; j < log2_i; j++) {
				if (p2 & 1) {
					addJJJG2(QQ[i], QQ[i], QQ[(1 << j)]);
				}

				p2 >>= 1;
			}
		}
	}

	bn::MultiNormalizeFp2(PQ, QQ, 256);
	PQ[0][0] = 0; PQ[0][1] = 1;
}

void precomputeG2knownSC(Fp2 PQ[PRECOMPUTEknownS][2], const Fp2 Q[2], const M_sint* WB, const M_sint (*BB)[4])
{
	int t = 256, d = 32;
	int i, j;
	M_uint td;
	int p, p2;
	Fp2 QA[3];
	Fp2 QQ[t][3];

	QQ[0][0] = 1;    QQ[0][1] = 1;    QQ[0][2] = 1;
	QQ[1][0] = Q[0]; QQ[1][1] = Q[1]; QQ[1][2] = 1;

	td = pow(2, d);
	G2_mul(QQ[2], QQ[1], td, WB, BB);
    bn::ecop::NormalizeJac(QQ[2], QQ[2]);
	p = 2;

	for (i = 3; i < t; i++) {
		if (i == (1 << p)) {
			G2_mul(QQ[i], QQ[(1 << (p - 1))], td, WB, BB);
			bn::ecop::NormalizeJac(QQ[i], QQ[i]);
			p++;
			continue;

		} else {
			QQ[i][0] = QQ[(1 << (p - 1))][0]; QQ[i][1] = QQ[(1 << (p - 1))][1]; QQ[i][2] = QQ[(1 << (p - 1))][2];
			p2 = i;

			const int log2_i = (int)log2((unsigned)i);
			for (j = 0; j < log2_i; j++) {
				if (p2 & 1) {
					addJJJG2(QQ[i], QQ[i], QQ[(1 << j)]);
				}

				p2 >>= 1;
			}
		}
	}

	for (i = 0; i < PRECOMPUTEknownS; i++) {
	    QA[0] = QQ[t - 1 - i][0];
	    Fp2::neg(QA[1], QQ[t - 1 -i][1]);
	    QA[2] = QQ[t - 1 - i][2];
	    addJJJG2(QQ[i], QQ[i], QA);
	}

	bn::MultiNormalizeFp2(PQ, QQ, PRECOMPUTEknownS);
	QA[0] = QQ[255][0]; QA[1] = QQ[255][1]; QA[2] = QQ[255][2];
	bn::ecop::NormalizeJac(QA, QA);
	PQ[0][0] = QA[0]; Fp2::neg(PQ[0][1], QA[1]);
}

void G2_mulknownC(Fp2* R, const M_uint& Vk, const Fp2 PQ[256][2], uint64_t PT[8][256])
{
	//d=32;
	Fp2 Q[3];
	R[0] = R[1] = 1;
	R[2] = 0;
	unsigned char acum[32];
	RecodeForPrecompC2(acum, Vk, PT);

	for (int i = 31; i >= 0; i--) {
		dblJG2(Q, R); //is this one failing?
		addJJAG2c(R, Q, PQ[acum[i]]);
	}
}

void G2_mulknownSC(Fp2* R, const M_uint& Vk, const Fp2 PQ[PRECOMPUTEknownS][2], uint64_t PT[8][256], const M_uint & t256, const M_uint &Half_Zr)
{
	//d=32;
	Fp2 Q[3];
	Fp2 QA[3];
	R[0] = R[1] = 1;
	R[2] = 0;
	unsigned char acum[32];
	RecodeForPrecompSC(acum, Vk, PT, t256, Half_Zr);

	for (int i = 31; i >= 0; i--) {
		dblJG2(Q, R); //is this one failing?
        if ((acum[i] & 0x80) == 0x80) {
            QA[0]=PQ[acum[i]^0xFF][0];
            QA[1]=PQ[acum[i]^0xFF][1];
            Fp2::neg(QA[1],QA[1]);
            addJJAG2c(R, Q, QA);
        } else {
		    addJJAG2c(R, Q, PQ[acum[i]]);
		}
	}
}

////////////////////////////////////////////////////////////
// GT part
////////////////////////////////////////////////////////////
// Copy from the G1 version once it is optimized.
void precomputeGTknownC(Fp12 PF[256], const Fp12& f, const M_sint* WB, const M_sint (*BB)[4])
{
	//int t=256,d=32;
	int i, j;
	M_uint td("0x100000000"); // 2^32
	int p, p2;
	Fp12 FA, FB;
	PF[0] = 1;
	PF[1] = f;
	GT_expo(PF[2], f, td, WB, BB);
	p = 2;

	for (i = 3; i < 256; i++) {
		if (i == (1 << p)) {
			GT_expo(PF[i], PF[(1 << (p - 1))], td, WB, BB);
			p++;
			continue;

		} else {
			PF[i] = PF[(1 << (p - 1))];
			p2 = i;

			const int log2_i = (int)log2((unsigned)i);
			for (j = 0; j < log2_i; j++) {
				if (p2 & 1) {
					Fp12::mul(PF[i], PF[i], PF[(1 << j)]);
				}

				p2 >>= 1;
			}
		}
	}
}

void precomputeGTknownSC(Fp12 PF[PRECOMPUTEknownS], const Fp12& f, const M_sint* WB, const M_sint (*BB)[4])
{
	int t=256,d=32;
	int i, j;
	M_uint td;
	int p, p2;
	Fp12 FA;
	Fp12 PE[t];
	PE[0] = 1;
	PE[1] = f;
	td = pow(2, d);
	GT_expo(PE[2], f, td, WB, BB);
	p = 2;

	for (i = 3; i < t; i++) {
		if (i == (1 << p)) {
			GT_expo(PE[i], PE[(1 << (p - 1))], td, WB, BB);
			p++;
			continue;

		} else {
			PE[i] = PE[(1 << (p - 1))];
			p2 = i;

			const int log2_i = (int)log2((unsigned)i);
			for (j = 0; j < log2_i; j++) {
				if (p2 & 1) {
					Fp12::mul(PE[i], PE[i], PE[(1 << j)]);
				}

				p2 >>= 1;
			}
		}
	}
    for (i = 0; i < PRECOMPUTEknownS; i++) {
        FA.a_ = PE[i^0xFF].a_;
        Fp6::neg(FA.b_, PE[i^0xFF].b_);
        Fp12::mul(PF[i], PE[i], FA);
    }
    PF[0].a_ = PE[255].a_;
    Fp6::neg(PF[0].b_, PE[255].b_);
}

void GT_expoknownC(Fp12& R, const M_uint& Vk, const Fp12 PF[256], uint64_t PT[8][256])
{
	//d=32;
	R = 1;//R.set(Fp2(1),Fp2(0),Fp2(0),Fp2(0),Fp2(0),Fp2(0));
	unsigned char acum[32];
	RecodeForPrecompC2(acum, Vk, PT);

	for (int i = 31; i >= 0; i--) {
		R.sqru();
		Fp12::mul(R, R, PF[acum[i]]);
	}
}

void GT_expoknownSC(Fp12& R, const M_uint& Vk, const Fp12 PF[128], uint64_t PT[8][256], const M_uint & t256, const M_uint &Half_Zr)
{
	//d=32;
	R = 1;//R.set(Fp2(1),Fp2(0),Fp2(0),Fp2(0),Fp2(0),Fp2(0));
	unsigned char acum[32];
	Fp12 FA;
	RecodeForPrecompSC(acum, Vk, PT, t256, Half_Zr);
//for (int i=31;i>=0;i--) std::cout<<(int)acum[i]<<","; std::cout<<std::endl;
	for (int i = 31; i >= 0; i--) {
		R.sqru();
		if ((acum[i] & 0x80) == 0x80) {
		    FA = PF[acum[i]^0xFF];
		    Fp6::neg(FA.b_, FA.b_);
		} else {
		    FA = PF[acum[i]];
		}
		Fp12::mul(R, R, FA);
	}
}


////////////////////////////////////////////////////////////
// G1 precomputation
////////////////////////////////////////////////////////////
void get_di(mie::Fp d[], const mie::Fp P[], int w)
{
//a=0
	//M_uint d[w];
	mie::Fp A, B, C, D, E;
	mie::Fp t0;
	int i;
	d[0] = 2 * P[1];
	C = d[0] * d[0];
	A = 3 * P[0] * P[0];
	B = C * (3 * P[0]);
	d[1] = A * A - B;

	if (w > 2) {
		E = d[1] * d[1];
		B = E * B;
		C = C * C;
		D = E * d[1];
		mie::Fp::neg(t0, d[1]);
		A = t0 * A - C;
		d[2] = A * A - 2 * D - B;
		E = d[2] * d[2];
		B = E * (B + 3 * D);
		C = D * (2 * A + C);
		D = E * d[2];
		mie::Fp::neg(t0, d[2]);
		A = t0 * A - C;

		if (w > 3)
			d[3] = A * A - D - B;

		for (i = 5; i <= w; i++) {
			E = d[i - 2] * d[i - 2];
			B = E * B;
			C = D * C;
			D = E * d[i - 2];
			mie::Fp::neg(t0, d[i - 2]);
			A = (t0) * A - C;
			d[i - 1] = A * A - D - B;
		}
	}
}
// Alias Montgomery's shared inversion
void invert_di(mie::Fp* e, mie::Fp* f, const mie::Fp* d, int w)
{
	int i; mie::Fp t0, t1; e[0] = d[0];

	for (i = 2; i <= w; i++) e[i - 1] = e[i - 2] * d[i - 1];

	t0 = e[w - 1]; t0.inverse();

	for (i = w; i >= 2; i--) {
		t1 = d[i - 1];
		f[i - 1] = e[i - 2] * t0;
		t0 = t0 * t1;
	}

	f[0] = t0;
}
void get_deltai(mie::Fp* l, mie::Fp* e, const mie::Fp* f, int w)
{
	int i; l[0] = f[0];

	for (i = 2; i <= w; i++) l[i - 1] = e[i - 2] * e[i - 2] * f[i - 1];
}
void get_points(mie::Fp x[], mie::Fp y[], const mie::Fp P[], int w, const mie::Fp l[])
{
	//mie::Fp x[w],y[w];
	mie::Fp t0, x1, y1; t0 = x1 = y1 = 0;
	int i;
	t0 = (3 * P[0] * P[0]) * l[0];
	x1 = t0 * t0 - 2 * P[0];
	y1 = t0 * (P[0] - x1) - P[1];
	t0 = (y1 - P[1]) * l[1];
	x[2 - 2] = t0 * t0 - x1 - P[0];
	y[2 - 2] = t0 * (x1 - x[2 - 2]) - y1;

	for (i = 3; i <= w; i++) {
		t0 = (y[i - 1 - 2] - y1) * l[i - 1];
		x[i - 2] = t0 * t0 - x1 - x[i - 1 - 2];
		y[i - 2] = t0 * (x1 - x[i - 2]) - y1;
	}
}

void precompute(mie::Fp* x, mie::Fp* y, const mie::Fp* P, int w)
{
	mie::Fp d[w], e[w], f[w], l[w];
	get_di(d, P, w);
	invert_di(e, f, d, w);
	get_deltai(l, e, f, w);
	get_points(x, y, P, w, l);
}

void addJJAG1b(mie::Fp* c, const mie::Fp* a, const mie::Fp* b)
{
	mie::Fp t1, t2, t3, t4;

	if (a[2].isZero()) {
		if ((a[0] == 1) && (a[1] == 1)) {
			if (b[0].isZero()) {
				if ((b[1] == 1)) {
					c[0] = 1; c[1] = 1; c[2] = 0; return;

				} else {
					c[0] = b[0];
					c[1] = b[1];
					c[2] = 1;
					return;
				}

			} else {
				c[0] = b[0];
				c[1] = b[1];
				c[2] = 1;
				return;
			}
		}

	} else if (b[0].isZero())
		if ((b[1] == 1)) {
			c[0] = a[0]; c[1] = a[1]; c[2] = a[2]; return;
		}

	mie::Fp::square(t1, a[2]);
	mie::Fp::mul(t2, t1, a[2]);
	mie::Fp::mul(t1, t1, b[0]);
	mie::Fp::mul(t2, t2, b[1]);
	mie::Fp::sub(t1, t1, a[0]);
	mie::Fp::sub(t2, t2, a[1]);

	//*
	if (t1.isZero()) {
		if (!t2.isZero()) {
			c[0] = 1; c[1] = 1; c[2] = 0;
			return;

		} else {
			mie::Fp b2[3]; b2[0] = b[0]; b2[1] = b[1]; b2[2] = b[2];
			//std::cout<<"+"<<std::endl;
			bn::ecop::ECDouble(c, b2);
			return;
		}
	}//*/

	mie::Fp::mul(c[2], a[2], t1);
	mie::Fp::square(t3, t1);
	mie::Fp::mul(t4, t3, t1);
	mie::Fp::mul(t3, t3, a[0]);
	mie::Fp::add(t1, t3, t3);
	mie::Fp::square(c[0], t2);
	mie::Fp::sub(c[0], c[0], t1);
	mie::Fp::sub(c[0], c[0], t4);
	mie::Fp::sub(t3, t3, c[0]);
	mie::Fp::mul(t3, t3, t2);
	mie::Fp::mul(t4, t4, a[1]);
	mie::Fp::sub(c[1], t3, t4);
}

////
////////////////////////////////////////////////////////////
// G2 precomputation
////////////////////////////////////////////////////////////
void get_diPUNTOG2(Fp2 d[], const Fp2 P[], int w)
{
//a=0
	//M_uint d[w];
	Fp2 A, B, C, D, E;
	Fp2 t0;
	int i;
	d[0] = P[1] + P[1];
	Fp2::square(C, d[0]);
	Fp2::square(A, P[0]); A = A + A + A;
	B = C * (P[0] + P[0] + P[0]);
	Fp2::square(d[1], A); d[1] = d[1] - B;

	//std::cout<<w;//<<std::endl;
	if (w > 2) {
		Fp2::square(E, d[1]);
		B = E * B;
		Fp2::square(C, C);
		D = E * d[1];
		Fp2::neg(t0, d[1]);
		A = t0 * A - C;
		Fp2::square(d[2], A); d[2] = d[2] - D - D - B;
		Fp2::square(E, d[2]);
		B = E * (B + D + D + D);
		C = D * (A + A + C);
		D = E * d[2];
		Fp2::neg(t0, d[2]);
		A = t0 * A - C;

		if (w > 3) {
			Fp2::square(d[3], A); d[3] = d[3] - D - B;
		}

		for (i = 5; i <= w; i++) {
			Fp2::square(E, d[i - 2]);
			B = E * B;
			C = D * C;
			D = E * d[i - 2];
			Fp2::neg(t0, d[i - 2]);
			A = (t0) * A - C;
			Fp2::square(d[i - 1], A); d[i - 1] = d[i - 1] - D - B;
		}
	}
}

// Alias Montgomery's shared inversion
void invert_diPUNTOG2(Fp2* e, Fp2* f, const Fp2* d, int w)
{
	Fp2 t0, t1;
	e[0] = d[0];

	for (int i = 2; i <= w; i++) {
		e[i - 1] = e[i - 2] * d[i - 1];
	}

	t0 = e[w - 1];
	t0.inverse();

	for (int i = w; i >= 2; i--) {
		t1 = d[i - 1];
		f[i - 1] = e[i - 2] * t0;
		t0 = t0 * t1;
	}

	f[0] = t0;
}

void get_deltaiPUNTOG2(Fp2* l, const Fp2* e, const Fp2* f, int w)
{
	l[0] = f[0];

	for (int i = 2; i <= w; i++) {
		Fp2::square(l[i - 1], e[i - 2]);
		l[i - 1] *= f[i - 1];
	}
}

void get_pointsPUNTOG2(Fp2 x[], Fp2 y[], const Fp2 P[], int w, const Fp2 l[])
{
	Fp2 t0, x1, y1;
	Fp2::square(t0, P[0]);
	t0 = (t0 + t0 + t0) * l[0];
	Fp2::square(x1, t0);
	x1 = x1 - P[0] - P[0];
	y1 = t0 * (P[0] - x1) - P[1];
	t0 = (y1 - P[1]) * l[1];
	Fp2::square(x[2 - 2], t0);
	x[2 - 2] = x[2 - 2] - x1 - P[0];
	y[2 - 2] = t0 * (x1 - x[2 - 2]) - y1;

	for (int i = 3; i <= w; i++) {
		t0 = (y[i - 1 - 2] - y1) * l[i - 1];
		Fp2::square(x[i - 2], t0);
		x[i - 2] = x[i - 2] - x1 - x[i - 1 - 2];
		y[i - 2] = t0 * (x1 - x[i - 2]) - y1;
	}
}

void precomputePUNTOG2(Fp2* x, Fp2* y, const Fp2* P, int w)
{
	Fp2 d[w], e[w], f[w], l[w];
	get_diPUNTOG2(d, P, w);
	invert_diPUNTOG2(e, f, d, w);
	get_deltaiPUNTOG2(l, e, f, w);
	get_pointsPUNTOG2(x, y, P, w, l);
}

////////////////////////////////////////////////////////////
// Point Addition/Doubling
////////////////////////////////////////////////////////////

// Jacobians<- Jacobians + Affine
inline void addJJAG2c(Fp2* c, const Fp2* a, const Fp2* b)
{
	if (a[2].isZero()) {
		if ((a[0] == 1) && (a[1] == 1)) {
			if (b[0].isZero()) {
				if ((b[1] == 1)) {
					c[0] = 1; c[1] = 1; c[2] = 0; return;

				} else {
					c[0] = b[0];
					c[1] = b[1];
					c[2] = 1;
					return;
				}

			} else {
				c[0] = b[0];
				c[1] = b[1];
				c[2] = 1;
				return;
			}
		}

	} else if (b[0].isZero())
		if ((b[1] == 1)) {
			c[0] = a[0]; c[1] = a[1]; c[2] = a[2]; return;
		}

	Fp2 t21, t22, t23, t24;
	Fp2::square(t21, a[2]);
	Fp2::mul(t22, t21, a[2]);
	Fp2::mul(t21, t21, b[0]);
	Fp2::mul(t22, t22, b[1]);
	Fp2::sub(t21, t21, a[0]);
	Fp2::sub(t22, t22, a[1]);

	if (t21.isZero()) {
		if (!t22.isZero()) {
			c[0] = 1; c[1] = 1; c[2] = 0;
			return;

		} else {
			//std::cout<<"+"<<std::endl;
			//dblJG2(c,b);
			Fp2 b_[3]; b_[0] = b[0]; b_[1] = b[1]; b_[2] = b[2];
			bn::ecop::ECDouble(c, b_);
			return;
		}
	}

	Fp2::mul(c[2], a[2], t21);
	Fp2::square(t23, t21);
	Fp2::mul(t24, t23, t21);
	Fp2::mul(t23, t23, a[0]);
	Fp2::add(t21, t23, t23);
	Fp2::square(c[0], t22);
	Fp2::sub(c[0], c[0], t21);
	Fp2::sub(c[0], c[0], t24);
	Fp2::sub(t23, t23, c[0]);
	Fp2::mul(t23, t23, t22);
	Fp2::mul(t24, t24, a[1]);
	Fp2::sub(c[1], t23, t24);
	//*/
}

void dblJG2(Fp2* c, const Fp2* a)
{
#if 1

	if (a[2].isZero())
		if ((a[0] == 1) && (a[1] == 1)) {
			c[0] = a[0];
			c[1] = a[1];
			c[2] = a[2];
			return;
		}

	bn::ecop::ECDouble(c, a);
	return;
#endif

	//if ((a[0]==1)&&(a[1]==1)&&(a[2]==0)) {c[0]=a[0];c[1]=a[1];c[2]=a[2]; return;}
	if (a[2].isZero())
		if ((a[0] == 1) && (a[1] == 1)) {
			c[0] = a[0];
			c[1] = a[1];
			c[2] = a[2];
			return;
		}

	Fp2 t20, t21, t22, t23, t24;
	//Fp2 t0,t1,t2,t3,t4;
	Fp2::square(t22, a[0]);
	Fp2::square(t20, a[1]);
	Fp2::square(t21, t20);
	Fp2::add(t20, a[0], t20);
	Fp2::square(t20, t20);
	Fp2::sub(t20, t20, t22);
	Fp2::sub(t20, t20, t21);
	Fp2::add(t20, t20, t20); //2*t3;
	// Fp2::mul3(t22,t22);
	Fp2::add(t24, t22, t22);
	Fp2::add(t22, t24, t22);
	Fp2::square(t23, t22);
	Fp2::add(t24, t20, t20); //2*D
	Fp2::sub(c[0], t23, t24);
	Fp2::sub(t20, t20, c[0]);
	// Fp2::mul8(t21,t21);
	Fp2::add(t24, t21, t21);
	Fp2::add(t24, t24, t24);
	Fp2::add(t21, t24, t24);
	Fp2::mul(t20, t22, t20);
	Fp2::sub(c[1], t20, t21);
	Fp2::mul(t20, a[1], a[2]);
	Fp2::add(c[2], t20, t20); //2*t8
}


////////////////////////////////////////////////////////////
// Scalar-Point Multiplication
////////////////////////////////////////////////////////////
//64 bit k
M_uint One = 1; int Uno = 1; M_uint l = 1;
void NAFw2(int mp[], int mn[], int& tlen, const M_uint& k, int w)
{
	// @todo this assertion does not work.
	// assert(sizeof(*M_bare(k)) == 8); // assume block is 64bit

	if (M_blockSize(k) == 2)
		NAFw128(mp, mn, tlen, k, w);
	else //k=1
		NAFw64 (mp, mn, tlen, k, w);
}
void NAFw64(int mp[], int mn[], int& tlen, const M_uint& k, int w)
{
	uint64_t l;
	l = M_block(k, 0); //l.forceAlloc(1);
	int temp;
	int i;

	for (i = 0; i < 70; i++) mp[i] = mn[i] = 0;

	int twotoW;//=pow(2,w);
	int twotoW1;//=pow(2,w-1);
	int mask;

	//int l0;
	if (w == 2) {
		twotoW = 4;
		twotoW1 = 2;
		mask = 3;
	}
	if (w == 3) {
		twotoW = 8;
		twotoW1 = 4;
		mask = 7;
	}

	if (w == 7) {
		twotoW = 128;
		twotoW1 = 64;
		mask = 127;
	}

	//for (i=0;i<tlen;i++) {mp[i]=mn[i]=0;}
	tlen = 0; i = 0;

	while (l > 0) {
		//l0=l[0];
		if (l & 1) {
			temp = l & mask;

			if (temp < twotoW1) {
				mp[i] = temp;
				l = l - mp[i];
			}

			//M_uint::sub(l,l,mp[i]);}
			else              {
				mn[i] = twotoW - temp;
				l = l + mn[i];
			}

			//M_uint::add(l,l,mn[i]);}
		}

		//l>>=1;//M_uint::shr(l,l,1);
		//l[0]/=2;
		l >>= 1;
		i++; tlen++;//std::cout<<l.bitLen()<<":"<<i<<"\n";
	}//std::cout<<tlen<<std::endl;

	//for (i=0;i<tlen;i++) std::cout<<mp[i]<<","; std::cout<<std::endl;
	//for (i=0;i<tlen;i++) std::cout<<mn[i]<<","; std::cout<<std::endl;
}

uint64_t l64 = (uint64_t(1) << 63);
void NAFw128(int mp[], int mn[], int& tlen, const M_uint& k, int w)
{
	uint64_t l[2];
	l[0] = M_block(k, 0);
	l[1] = M_block(k, 1); //l.forceAlloc(2);
	int temp;
	int i;

	for (i = 0; i < 128; i++) mp[i] = mn[i] = 0;

	int twotoW;//=pow(2,w);
	int twotoW1;//=pow(2,w-1);
	int mask;

	//int l0;
	if (w == 3) {
		twotoW = 8;
		twotoW1 = 4;
		mask = 7;
	}

	if (w == 7) {
		twotoW = 128;
		twotoW1 = 64;
		mask = 127;
	}

	tlen = 0; i = 0;

	while ((l[0] > 0) || (l[1] > 0)) {
		//l0=l[0];
		if (l[0] & 1) { //Uno) {
			temp = l[0] & mask;

			if (temp < twotoW1) {
				mp[i] = temp;
				l[0] = l[0] - mp[i];
			}

			//M_uint::sub(l,l,mp[i]);}
			else              {
				mn[i] = twotoW - temp;
				l[0] = l[0] + mn[i];
			}

			//M_uint::add(l,l,mn[i]);}
		}

		//l>>=1;//M_uint::shr(l,l,1);

		l[0] >>= 1; if (l[1] & 1) l[0] |= l64; l[1] >>= 1; //if ((l[1]==0)&&(l.size()>1))l.forceAlloc(1);

		//M_uint::shr(l,l,1);
		i++; tlen++;//std::cout<<l.bitLen()<<":"<<i<<"\n";
	}//std::cout<<tlen<<std::endl;

	//for (i=0;i<tlen;i++) std::cout<<mp[i]<<","; std::cout<<std::endl;
	//for (i=0;i<tlen;i++) std::cout<<mn[i]<<","; std::cout<<std::endl;
}


////////////////////////////////////////////////////////////
// End of Scalar-Point Multiplication
////////////////////////////////////////////////////////////
void p_power_Frobenius(Fp2* R, const Fp2* Q)
{
	/*
	R[0].a_=Q[0].a_;
	Fp::neg(R[0].b_,Q[0].b_);
	Fp2::mul(R[0].a_,R[0].a_,Param::W2p);
	Fp2::mul(R[0].b_,R[0].b_,bn::ParamT<Fp2>::W2p);
	R[1].a_=Q[1].a_;
	Fp::neg(R[1].b_,Q[1].b_);
	Fp2::mul(R[1].a_,R[1].a_,bn::ParamT<Fp2>::W3p);
	Fp2::mul(R[1].b_,R[1].b_,bn::ParamT<Fp2>::W3p);
	*/
	typedef bn::Fp2T<mie::Fp> Fp2;
	typedef bn::ParamT<Fp2> Param;
	R[0].a_ = Q[0].a_;
	mie::Fp::neg(R[0].b_, Q[0].b_);
	Fp2::mul_Fp_1(R[0], Param::W2p.b_);
	R[1].a_ = Q[1].a_;
	mie::Fp::neg(R[1].b_, Q[1].b_);
	R[1] *= Param::W3p;
	//R[0]=Q[0];R[1]=Q[1];
}

void p_power_FrobeniusJac3(Fp2* R, const Fp2* Q)
{
    p_power_Frobenius(R, Q);
    R[2].a_ = Q[2].a_;
	mie::Fp::neg(R[2].b_, Q[2].b_);
}

void p_power_FrobeniusJac3i(Fp2* R, const Fp2* Q, int n)
{
	typedef bn::Fp2T<mie::Fp> Fp2;
	typedef bn::ParamT<Fp2> Param;
	R[0] = Q[0];
	R[1] = Q[1];
	R[2] = Q[2];

	for (int i = 0; i < n; i++)
	{
	    mie::Fp::neg(R[0].b_, R[0].b_);
	    mie::Fp::neg(R[1].b_, R[1].b_);
        mie::Fp::neg(R[2].b_, R[2].b_);

        Fp2::mul_Fp_1(R[0], Param::W2p.b_);
        R[1] *= Param::W3p;
    }
}

M_sint snil = 0;
void mad(M_sint &v0, const M_sint &WB, M_uint k)
{
    //v0 = (WB * k) / bn::ParamT<Fp2>::r;
	v0 = WB * k;
#ifdef MIE_ATE_USE_GMP
	bool sign = M_isNegative(v0);
	M_uint abs_v0 = M_abs(v0);
	abs_v0 >>= 256;
	v0 = abs_v0;
	if (sign) v0 = -v0;
#else
	//v0 >>= 256; //not working with negatives?
	bool sign = M_isNegative(v0);
	M_uint v1 = v0.get();
    v1 >>= 256;
    v0 = v1;
    if (sign) v0 = -v0;
#endif
}
void DecompG2(M_sint* u, const M_uint& Vk, const M_sint* WB, const M_sint BB[4][4])
{
	int i, j;
	M_sint v0[4];

	//std::cout<<"WB=(";
	for (i = 0; i < 4; i++) {
		//std::cout<<WB[i]; if (i+1<=3) std::cout<<",";
		mad(v0[i], WB[i], Vk);
		u[i] = 0; //std::cout<<"-"<<i<<"-"<<std::endl;
	}//std::cout<<")"<<std::endl;

	//std::cout<<"v0=("<<v0[0]<<","<<v0[1]<<","<<v0[2]<<","<<v0[3]<<")"<<std::endl;
	u[0] = Vk;

	for (i = 0; i < 4; i++) {
		//std::cout<<"v0"<<i<<"=(";
		for (j = 0; j < 4; j++) {
			//std::cout<<v0[j]*BB[j][i]; if (i+1<=3) std::cout<<",";
			u[i] -= v0[j] * BB[j][i];
		}

		//std::cout<<")"<<std::endl;
	}
}

void precomputeG2(Fp2 QsI[4][PRECOMPUTE][2], Fp2 P[2], M_sint u[4])
{
	int i, j; int m = 4;

	Fp2 ux[PRECOMPUTE], uy[PRECOMPUTE]; for (i = 0; i < PRECOMPUTE; i++) ux[i] = uy[i] = 0;

	QsI[0][0][0] = P[0]; QsI[0][0][1] = P[1];// QsI[0][0][2] = 1;
	precomputePUNTOG2(ux, uy, P, PRECOMPUTE);

	for (j = 1; j < PRECOMPUTE; j++) {
		QsI[0][j][0] = ux[j - 1]; QsI[0][j][1] = uy[j - 1];
	}

	for (i = 1; i < m; i++)
		for (j = 0; j < PRECOMPUTE; j++)
			p_power_Frobenius(QsI[i][j], QsI[i - 1][j]);

	for (i = 0; i < m; i++) if (u[i] < 0) {
			for (j = 0; j < PRECOMPUTE; j++) {
				Fp2::neg(QsI[i][j][1], QsI[i][j][1]); u[i] = -u[i]; /*std::cout<<"!!!"<<std::endl;*/
			}
		}
}

void precomputeG2b(Fp2 QsI[4][PRECOMPUTE][2], const Fp2 *P, M_sint u[4])
{
	int i, j; int m = 4;

	Fp2 Q0[3],Q1[3];
	Q1[0]=P[0]; Q1[1]=P[1]; Q1[2]=P[2];
	bn::ecop::NormalizeJac(Q0,P);
	QsI[0][0][0] = Q0[0]; QsI[0][0][1] = Q0[1];// QsI[0][0][2] = Q0[2];
	//bn::ecop::ECDouble(Q0, Q1);
	dblJG2(Q0, Q1);
	//addJJAG2c(Q0, Q0, P);
	addJJJG2(Q0,Q0,P);
	bn::ecop::NormalizeJac(Q0, Q0);
	QsI[0][1][0] = Q0[0]; QsI[0][1][1] = Q0[1];

	for (i = 1; i < m; i++)
		for (j = 0; j < PRECOMPUTE; j++)
			p_power_Frobenius(QsI[i][j], QsI[i - 1][j]);

	for (i = 0; i < m; i++) if (u[i] < 0) {
			for (j = 0; j < PRECOMPUTE; j++) {
				Fp2::neg(QsI[i][j][1], QsI[i][j][1]); u[i] = -u[i]; /*std::cout<<"!!!"<<std::endl;*/
			}
		}
}

void precomputeG2known(Fp2 (*QsI)[PRECOMPUTEknown][2], const Fp2* P)
{
	Fp2 ux[PRECOMPUTEknown];
	Fp2 uy[PRECOMPUTEknown];
	QsI[0][0][0] = P[0];
	QsI[0][0][1] = P[1];
//	QsI[0][0][2] = 1;
	precomputePUNTOG2(ux, uy, P, PRECOMPUTEknown);

	for (int j = 1; j < PRECOMPUTEknown; j++) {
		QsI[0][j][0] = ux[j - 1];
		QsI[0][j][1] = uy[j - 1];
	}

	const int m = 4;

	for (int i = 1; i < m; i++) {
		for (int j = 0; j < PRECOMPUTEknown; j++) {
			p_power_Frobenius(QsI[i][j], QsI[i - 1][j]);
		}
	}
}

void precomputeG2SmallJ(Fp2 QPrec[][3], const Fp2* P)
{
	using namespace mie;
	Fp2 Qo[3];
	QPrec[0][0] = P[0]; QPrec[0][1] = P[1]; QPrec[0][2] = P[2];

	Qo[0] = QPrec[0][0]; Qo[1] = QPrec[0][1]; Qo[2] = QPrec[0][2];
	bn::ecop::ECDouble(Qo, QPrec[0]);
	bn::ecop::ECAdd(QPrec[1], Qo, QPrec[0]);
}

void precomputeG2Small(Fp2 QPrec[][2], const Fp2* Q, const M_sint& u)
{
	using namespace mie;
	int i, j;
	Fp2 Qo[2];

	Fp2 ux[PRECOMPUTE], uy[PRECOMPUTE]; for (i = 0; i < PRECOMPUTE; i++) ux[i] = uy[i] = 0;

	QPrec[0][0] = Q[0]; QPrec[0][1] = Q[1];

	if (u < 0) {
		Fp2::neg(QPrec[0][1], QPrec[0][1]); /*u=-u;*/
	}

	Qo[0] = QPrec[0][0]; Qo[1] = QPrec[0][1];
	precomputePUNTOG2(ux, uy, Qo, PRECOMPUTE);

	for (j = 1; j < PRECOMPUTE; j++) {
		QPrec[j][0] = ux[j - 1]; QPrec[j][1] = uy[j - 1];
	}
}

void G2_mulSmall(Fp2* R, const M_uint& k, const Fp2* QA)
{
	using namespace mie;
	if (k==1) {R[0] = QA[0]; R[1] = QA[1]; R[2] = QA[2]; return;}
	//bn::ecop::NormalizeJac(const_cast<Fp2*>(QA), QA); // QQQ
//std::cout<<k<<std::endl;
	Fp2 QPrec[PRECOMPUTE][3];
	Fp2 Q[3]; Q[0] = QA[0]; Q[1] = QA[1]; Q[2] = QA[2];
	int i, j;
	int v[2][70];

	for (i = 0; i < 2; i++)for (j = 0; j < 70; j++) v[i][j] = 0;

	int tlent = 0;
	NAFw64(v[0], v[1], tlent, k, WNAF);
	precomputeG2SmallJ(QPrec, Q);

	Fp2 S[3]; Fp2 R2[3];
	R[0] = 1; R[1] = 1; R[2] = 0;

	for (i = tlent - 1; i >= 0; i--) {
		dblJG2(R2, R); //bn::ecop::ECDouble(R2,R);
		R[0] = R2[0]; R[1] = R2[1]; R[2] = R2[2];

		if (v[0][i] > 0) {
			S[0] = QPrec[(v[0][i] - 1) >> 1][0];
			S[1] = QPrec[(v[0][i] - 1) >> 1][1];
			S[2] = QPrec[(v[0][i] - 1) >> 1][2];
			addJJJG2(R2, R, S);
			R[0] = R2[0]; R[1] = R2[1]; R[2] = R2[2];
		}

		if (v[1][i] > 0) {
			S[0] = QPrec[(v[1][i] - 1) >> 1][0];
			Fp2::neg(S[1], QPrec[(v[1][i] - 1) >> 1][1]);
			S[2] = QPrec[(v[1][i] - 1) >> 1][2];
			addJJJG2(R2, R, S);
			R[0] = R2[0]; R[1] = R2[1]; R[2] = R2[2];
		}
	}
}

void G2_mulSmall2(Fp2* R, const M_uint& k, const Fp2* QA)
{
	using namespace mie;
	if (k==1) {R[0] = QA[0]; R[1] = QA[1]; R[2] = QA[2]; return;}
	//bn::ecop::NormalizeJac(const_cast<Fp2*>(QA), QA); // QQQ

	Fp2 Q[3]; Q[0] = QA[0]; Q[1] = QA[1]; Q[2] = QA[2];
	int i, j;
	int v[2][70];

	for (i = 0; i < 2; i++)for (j = 0; j < 70; j++) v[i][j] = 0;

	int tlent = 0;
	NAFw64(v[0], v[1], tlent, k, 2);

	Fp2 S[3]; Fp2 R2[3];
	R[0] = 1; R[1] = 1; R[2] = 0;

	for (i = tlent - 1; i >= 0; i--) {
		dblJG2(R2, R);
		R[0] = R2[0]; R[1] = R2[1]; R[2] = R2[2];

		if (v[0][i] > 0) {
			addJJJG2(R2, R, Q);
			R[0] = R2[0]; R[1] = R2[1]; R[2] = R2[2];
		}

		if (v[1][i] > 0) {
			S[0] = Q[0];
			Fp2::neg(S[1], Q[1]);
			S[2] = Q[2];
			addJJJG2(R2, R, S);
			R[0] = R2[0]; R[1] = R2[1]; R[2] = R2[2];
		}
	}
}

void G2_mul(Fp2* Q, Fp2* P, const M_uint& Vk, const M_sint* WB, const M_sint (*BB)[4])
{
#if 0
	Fp2 QA[3]; QA[0] = P[0]; QA[1] = P[1]; QA[2] = 1;
	bn::ecop::ScalarMult(Q, QA, Vk);
	return;
#endif
  //if (Vk.bitLen() < 31) {
  if (M_bitLen(Vk) < 31) {
		G2_mulSmall2(Q, Vk, P);
		return;
  }
//  } else {
	int i, j;
	//int m=4;
	Fp2 QsI[4][PRECOMPUTE][2];
	//Fp2 Qo[2];
	Fp2 Q2[3];

	//Fp2 ux[PRECOMPUTE], uy[PRECOMPUTE]; for (i = 0; i < PRECOMPUTE; i++) ux[i] = uy[i] = 0;

	M_sint u[4];
	int v[8][70];

	for (i = 0; i < 8; i++)for (j = 0; j < 70; j++) v[i][j] = 0;

	int tlen[4], tlent = 0;
	DecompG2(u, Vk, WB, BB); //for(i=0;i<4;i++) std::cout<<u[i]<<","; std::cout<<std::endl;

	for (i = 0; i < 4; i++) tlen[i] = 70;

	NAFw2(v[0], v[1], tlen[0], M_abs(u[0]), WNAF); if (tlen[0] > tlent) tlent = tlen[0];

	NAFw2(v[2], v[3], tlen[1], M_abs(u[1]), WNAF); if (tlen[1] > tlent) tlent = tlen[1];

	NAFw2(v[4], v[5], tlen[2], M_abs(u[2]), WNAF); if (tlen[2] > tlent) tlent = tlen[2];

	NAFw2(v[6], v[7], tlen[3], M_abs(u[3]), WNAF); if (tlen[3] > tlent) tlent = tlen[3];

	//for (i=0;i<8;i++) {for (j=0;j<tlent;j++) std::cout<<((j<=tlen[i])?v[i][j]:0)<<","; /*std::cout<<tlen[i]<<std::endl;*/ std::cout<<"\n";}
	precomputeG2b(QsI, P, u);
	Fp2 S[2];//S[2]=0;//S[0]=1;S[1]=1;S[2]=0;
	Q[0] = 1; Q[1] = 1; Q[2] = 0;

	for (i = tlent - 1; i >= 0; i--) {
		dblJG2(Q2, Q); //bn::ecop::ECDouble(Q2,Q);
		Q[0] = Q2[0]; Q[1] = Q2[1]; Q[2] = Q2[2];

		for (j = 0; j <= 7; j += 2) {
			if (i <= tlen[j / 2] - 1) {
				//std::cout<<i<<":"<<j/2<<":"<<v[j][i]<<","<<v[j+1][i]<<std::endl;
				if (v[j][i] > 0) {
					S[0] = QsI[(j >> 1)][(v[j][i] - 1) >> 1][0];
					S[1] = QsI[(j >> 1)][(v[j][i] - 1) >> 1][1];
					addJJAG2c(Q, Q, S); //,half);
				}

				if (v[j + 1][i] > 0) {
					S[0] = QsI[(j >> 1)][(v[j + 1][i] - 1) >> 1][0];
					Fp2::neg(S[1], QsI[(j >> 1)][(v[j + 1][i] - 1) >> 1][1]);
					addJJAG2c(Q, Q, S); //,half);
				}
			}
		}
	}
//  }
}
void G2_mulknown(Fp2* Q, const M_uint& Vk, const M_sint* WB, const M_sint (*BB)[4], const Fp2 QsI[4][PRECOMPUTEknown][2])
{
#if 0
	Fp2 QA[3]; QA[0] = QsI[0][0][0]; QA[0] = QsI[0][0][1]; QA[0] = Fp2(Fp("1"), Fp("0"));
	bn::ecop::ScalarMult((Fp2 (&)[3])(*Q), QA, Vk);
	return;
#endif
	int i, j;
	M_sint u[4];
	int v[8][70];

	for (i = 0; i < 8; i++)for (j = 0; j < 70; j++) v[i][j] = 0;

	int tlen[4], tlent = 0;
	Fp2 Q2[3];
	DecompG2(u, Vk, WB, BB); //for(i=0;i<4;i++) std::cout<<u[i]<<","; std::cout<<std::endl;

	for (i = 0; i < 4; i++) tlen[i] = 70;

	NAFw2(v[0], v[1], tlen[0], M_abs(u[0]), WNAFknown); if (tlen[0] > tlent) tlent = tlen[0];

	NAFw2(v[2], v[3], tlen[1], M_abs(u[1]), WNAFknown); if (tlen[1] > tlent) tlent = tlen[1];

	NAFw2(v[4], v[5], tlen[2], M_abs(u[2]), WNAFknown); if (tlen[2] > tlent) tlent = tlen[2];

	NAFw2(v[6], v[7], tlen[3], M_abs(u[3]), WNAFknown); if (tlen[3] > tlent) tlent = tlen[3];

	//for (i=0;i<8;i++) {for (j=0;j<66;j++) std::cout<<v[i][j]<<","; std::cout<<":"<<tlen[i]/2<<std::endl;}std::cout<<tlent<<std::endl;

	// recover QsI at the end of this function
	//precomputeG2known(QsI,P);
	for (i = 0; i < 4; i++)
		if (u[i] < 0)
			for (j = 0; j < PRECOMPUTEknown; j++)
				Fp2::neg(const_cast<Fp2&>(QsI[i][j][1]), QsI[i][j][1]);

	Fp2 S[2];//S[2]=0;//S[0]=1;S[1]=1;S[2]=0;
	Q[0] = 1; Q[1] = 1; Q[2] = 0;

	for (i = tlent - 1; i >= 0; i--) {
		dblJG2(Q2, Q); //bn::ecop::ECDouble(Q2,Q);
		Q[0] = Q2[0]; Q[1] = Q2[1]; Q[2] = Q2[2];

		for (j = 0; j <= 7; j += 2) {
			if (i <= tlen[j / 2] - 1) {
				//std::cout<<i<<":"<<j/2<<":"<<v[j][i]<<","<<v[j+1][i]<<std::endl;
				if (v[j][i] > 0) {
					S[0] = QsI[j / 2][(v[j][i] - 1) >> 1][0];
					S[1] = QsI[j / 2][(v[j][i] - 1) >> 1][1];
					addJJAG2c(Q, Q, S); //,half);
				}

				if (v[j + 1][i] > 0) {
					S[0] = QsI[j / 2][(v[j + 1][i] - 1) >> 1][0];
					Fp2::neg(S[1], QsI[j / 2][(v[j + 1][i] - 1) >> 1][1]);
					addJJAG2c(Q, Q, S); //,half);
				}
			}
		}
	}

	for (i = 0; i < 4; i++)
		if (u[i] < 0)
			for (j = 0; j < PRECOMPUTEknown; j++)
				Fp2::neg(const_cast<Fp2&>(QsI[i][j][1]), QsI[i][j][1]);
}

void GT_expo(Fp12& z, const Fp12& x, const M_uint& e, const M_sint* WB, const M_sint (*BB)[4])
{
#if 0
	z = power(x, e);
	return;
#endif

	if (M_blockSize(e) < 2) {
		z = mie::power(x, e);
		return;
	}

	int i, j;
	M_sint u[4];//for (i=0;i<4;i++) u[i]=0;
	int v[8][70];

	for (i = 0; i < 8; i++)for (j = 0; j < 70; j++) v[i][j] = 0;

	int tlen[4], tlent = 0;
	Fp12 S;
	Fp12 fe[4][PRECOMPUTE];
	DecompG2(u, e, WB, BB); // for(i=0;i<4;i++) std::cout<<u[i]<<","; std::cout<<std::endl;

	for (i = 0; i < 4; i++) tlen[i] = 70;

	NAFw2(v[0], v[1], tlen[0], M_abs(u[0]), WNAF); if (tlen[0] > tlent) tlent = tlen[0];

	NAFw2(v[2], v[3], tlen[1], M_abs(u[1]), WNAF); if (tlen[1] > tlent) tlent = tlen[1];

	NAFw2(v[4], v[5], tlen[2], M_abs(u[2]), WNAF); if (tlen[2] > tlent) tlent = tlen[2];

	NAFw2(v[6], v[7], tlen[3], M_abs(u[3]), WNAF); if (tlen[3] > tlent) tlent = tlen[3];

	//for (i=0;i<8;i++) {for (j=0;j<70;j++) std::cout<<v[i][j]<<",";std::cout<<"\n";}
	precomputeGT(fe, x);

	for (i = 0; i < 4; i++)
		if (u[i] < 0)
			for (j = 0; j < PRECOMPUTE; j++)
				Fp6::neg(fe[i][j].b_, fe[i][j].b_);

	z = 1; //z = x;

	for (i = tlent - 1; i >= 0; i--) {
		z.sqru();

		for (j = 0; j <= 7; j += 2) {
			if (i <= tlen[j / 2] - 1) {
				if (v[j][i] > 0) {
					Fp12::mul(z, z, fe[j / 2][(v[j][i]) / 2]);
				}

				if (v[j + 1][i] > 0) {
					S.a_ = fe[j / 2][(v[j + 1][i]) / 2].a_;
					Fp6::neg(S.b_, fe[j / 2][(v[j + 1][i]) / 2].b_);
					Fp12::mul(z, z, S);
				}
			}
		}
	}
}
void GT_expoknown(Fp12& z, const Fp12 fe[4][PRECOMPUTEknown], M_uint e, const M_sint* WB, const M_sint (*BB)[4])
{
#if 0
	//Fp12 ff; ff=fe[0][0];
	z = power(fe[0][0], e);
	return;
#endif
	int i, j;
	M_sint u[4];
	int v[8][70];

	for (i = 0; i < 8; i++)for (j = 0; j < 70; j++) v[i][j] = 0;

	int tlen[4], tlent = 0;
	Fp12 S;
	DecompG2(u, e, WB, BB); //for(i=0;i<4;i++) std::cout<<u[i]<<","; std::cout<<std::endl;

	for (i = 0; i < 4; i++) tlen[i] = 70;

	NAFw2(v[0], v[1], tlen[0], M_abs(u[0]), WNAFknown); if (tlen[0] > tlent) tlent = tlen[0];

	NAFw2(v[2], v[3], tlen[1], M_abs(u[1]), WNAFknown); if (tlen[1] > tlent) tlent = tlen[1];

	NAFw2(v[4], v[5], tlen[2], M_abs(u[2]), WNAFknown); if (tlen[2] > tlent) tlent = tlen[2];

	NAFw2(v[6], v[7], tlen[3], M_abs(u[3]), WNAFknown); if (tlen[3] > tlent) tlent = tlen[3];

	//precomputeGTknown(fe,x);
	// recover fe at the end of function
	for (i = 0; i < 4; i++)
		if (u[i] < 0)
			for (j = 0; j < PRECOMPUTEknown; j++)
				Fp6::neg(const_cast<Fp6&>(fe[i][j].b_), fe[i][j].b_);

	z = 1; //z = x;

	for (i = tlent - 1; i >= 0; i--) {
		z.sqru();

		for (j = 0; j <= 7; j += 2) {
			if (i <= tlen[j / 2] - 1) {
				if (v[j][i] > 0) {
					Fp12::mul(z, z, fe[j / 2][(v[j][i]) / 2]);
				}

				if (v[j + 1][i] > 0) {
					S.a_ = fe[j / 2][(v[j + 1][i]) / 2].a_;
					Fp6::neg(S.b_, fe[j / 2][(v[j + 1][i]) / 2].b_);
					Fp12::mul(z, z, S);
				}
			}
		}
	}

	for (i = 0; i < 4; i++)
		if (u[i] < 0)
			for (j = 0; j < PRECOMPUTEknown; j++)
				Fp6::neg(const_cast<Fp6&>(fe[i][j].b_), fe[i][j].b_);
}


void precomputeGT(Fp12 fe[4][PRECOMPUTE], const Fp12& x)
{
	int j;//,t; t=PRECOMPUTE;
	Fp12 S;
	fe[0][0] = x;
	//S=fe[0][0]*fe[0][0];
	S = fe[0][0];
	Fp12::square(S);

	for (j = 1; j < PRECOMPUTE; j++) {
		fe[0][j] = S * fe[0][j - 1];
	}

	//for (i=1;i<4;i++)
	//for (j=0;j<PRECOMPUTE;j++)
	//fe[i-1][j].Frobenius(fe[i][j]);
	for (j = 0; j < PRECOMPUTE; j++) {
		fe[0][j].Frobenius(fe[1][j]);
		fe[0][j].Frobenius2(fe[2][j]);
		fe[0][j].Frobenius3(fe[3][j]);
	}
}

void precomputeGTknown(Fp12 (*fe)[PRECOMPUTEknown], const Fp12& x)
{
	fe[0][0] = x;
	Fp12 S = fe[0][0];
	Fp12::square(S);

	for (int j = 1; j < PRECOMPUTEknown; j++) {
		fe[0][j] = S * fe[0][j - 1];
	}

	for (int j = 0; j < PRECOMPUTEknown; j++) {
		fe[0][j].Frobenius(fe[1][j]);
		fe[0][j].Frobenius2(fe[2][j]);
		fe[0][j].Frobenius3(fe[3][j]);
	}
}

void GT_expoSmall(Fp12& z, const Fp12& x, const M_uint& e)
{
	int i, j;
	//M_sint u[4];
	int v[8][70];

	for (i = 0; i < 8; i++)for (j = 0; j < 65; j++) v[i][j] = 0;

	int tlen = 0;
	Fp12 S = 0;
	Fp12 fe[4][PRECOMPUTE];
	NAFw64(v[0], v[1], tlen, e, WNAF);
	//std::cout<<tlen<<std::endl;
	//v[1][0]=1;v[0][2]=1;tlent=3;
	//for (i=0;i<tlen;i++) std::cout<<v[0][i]; std::cout<<std::endl;
	//for (i=0;i<tlen;i++) std::cout<<v[1][i]; std::cout<<std::endl;
	precomputeGT(fe, x);
	//std::cout<<"1a:"<<fe[0][0]<<std::endl;
	//std::cout<<"1b:"<<fe[0][1]<<std::endl;
	//PUT(fe[0][0]*fe[0][0]*fe[0][0]);
	z = x;

	for (i = tlen - 2; i >= 0; i--) {
		//std::cout<<"."<<std::endl;
		z.sqru();

		if (v[0][i] > 0) {
			Fp12::mul(z, z, fe[0][(v[0][i] - 1) / 2]);
			//std::cout<<"+"<<std::endl;
		}

		if (v[1][i] > 0) {
			//S=fe[0][(v[1][i]-1)/2];
			//S.inverse();
			S.a_ = fe[0][(v[1][i] - 1) / 2].a_;
			Fp6::neg(S.b_, fe[0][(v[1][i] - 1) / 2].b_);
			Fp12::mul(z, z, S);
			//std::cout<<"-"<<std::endl;
		}
	}
}

int Jacobi(const M_uint &a)
{
	const mie::Fp aa(M_toFp(a));
	mie::Fp v0(aa);
	
	const int siTbl[] = {
		1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1
	};

	for (int i = 0; i <= 251; i++) {
		mie::Fp::square(v0, v0);
		if (siTbl[251 - i])
			mie::Fp::mul(v0, v0, aa);
	}
	
	if ((v0 + 1).isZero()) return -1;
	return v0.get()[0];
}

int jack(M_uint &a, M_uint &n)
{
	//M_sint r;
	int t = 1;
	M_uint temp;

	while (a != 0) {
		uint32_t n_mod_8 = M_low32bit(n) % 8;
		while (M_low32bit(a) % 2 == 0) {
			a >>= 1;
			if ((n_mod_8 == 3) || (n_mod_8 == 5)) t = -t; //t=t*-1;
		}

		if (a < n) {
			temp = a; a = n; n = temp;

			if ((M_low32bit(a) % 4 == 3) && (M_low32bit(n) % 4 == 3)) t = -t; //t=t*-1;
		}

		//a=(a-n)/2;
//		a.shr(a, a - n, 1);
		a = (a - n) >> 1;
		n_mod_8 = M_low32bit(n) % 8;
		if ((n_mod_8 == 3) || (n_mod_8 == 5)) t = -t; //t=t*-1;
	}

	if (n != 1) t = 0;

	return t;
}

//*
// Based on mrlucas.c
mie::Fp V2(mie::Fp m, mie::Fp P)
{
	int i, l;
	mie::Fp w3, w4, w8, w9, w1;
	w3 = P;
	w4 = 2;
	w8 = w4;
	w9 = w3;
	w1 = m;
	M_uint binm = M_fromFp(m);
	l = M_bitLen(binm);

	for (i = l; i >= 0; i--) {
		if (M_testBit(binm, i)) {
//      std::cout<<"<1";
			w8 *= w9;
			//mie::Fp::mul(w8,w8,w9);
			w8 -= w3;
			//mie::Fp::sub(w8,w8,w3);
			w9 *= w9;
			//mie::Fp::mul(w9,w9,w9);
			w9 -= w4;
			//mie::Fp::sub(w9,w9,w4);
//      std::cout<<">"<<endl;

		} else { //if (m.testBit(i)==0)
//      std::cout<<d1<<","<<d2<<","<<P<<endl;
//      std::cout<<"<2";
			w9 *= w8;
			//mie::Fp::mul(w9,w9,w8);
			w9 -= w3;
			//mie::Fp::sub(w9,w9,w3);
			w8 *= w8;
			//mie::Fp::mul(w8,w8,w8);
			w8 -= w4;
			//mie::Fp::sub(w8,w8,w4);
//      std::cout<<">"<<endl;
		}

		//std::cout<<m.get().testBit(i)<<":"<<d1<<","<<d2<<endl;
	}

	//PUT(w9);PUT(w8);
	//mie::Fp v=w9;
	return w9;
}
//*/
bool mikesqrt(mie::Fp& w, mie::Fp& x)
{
	int t;
	M_uint w10, w4, w1;
	M_uint t0;
	//M_sint js;
	w = x;

	if (w == 0) return true;

	if (w == 1) return true;

	if (w == 4) {
		w = 2;
		return true;
	}

	M_uint w_org = M_fromFp(w);
	M_uint p = M_param_p();
	if (jack(w_org, p) != 1) {
		w = 0;
		return false;
	}

	//js=-1;
	//w10=p-1;
	//w10=w10/4; // that's a parameter of the system :S
	for (t = 1;; t++) {
		if (t == 1) w4 = w_org;
		else {
			w4 = w_org * t;
			//mie::ZmZ_1::mul(w4,w,t);
			w4 %= p;
			w4 *= t;
			//mie::ZmZ_1::mul(w4,w4,t);
			w4 %= p;
		}

		w1 = w4 - 4; // t0=w1.get(); //PUT(t0);

		t0 = w1; p = M_param_p();
		if (jack(t0, p) == -1) break;
	}

	static const Fp pm1o4("12598581548261874213705603106673800432416890341079302386700286569554565398542");
	w1 = w4 - 2; //PUT(t);PUT(w1); PUT(t); PUT(pm1o4);
	w = V2(pm1o4, M_toFp(w1)); //(w10,w1)

	//PUT(w);
	if (t != 1) w = w / t;

	return true;
	//if (w*w==x) return true; else return false;
}

bool mikesqrtbis(mie::Fp& w, mie::Fp& x, int& cpt)
{
	int t;
	M_uint w10, w4, w1;
	M_uint t0;
	//M_sint js;
	M_uint p = M_param_p();
	w = x;

	if (w == 0) return true;

	if (w == 1) return true;

	if (w == 4) {
		w = 2;
		return true;
	}

	M_uint w_org = M_fromFp(w);
	if (jack(w_org, p) != 1) {
		w = 0;
		return false;
	}

	//js=-1;
	//w10=p-1;
	//w10=w10/4; // that's a parameter of the system :S
	for (t = 1;; t++) {
		if (t == 1) w4 = w_org;
		else {
			w4 = w_org * t;
			//mie::ZmZ_1::mul(w4,w,t);
			w4 %= p;
			w4 *= t;
			//mie::ZmZ_1::mul(w4,w4,t);
			w4 %= p;
		}

		w1 = w4 - 4; // t0=w1.get(); //PUT(t0);

		t0 = w1; p = M_param_p();
		if (jack(w1, p) == -1) break;
	}

	cpt = t;
	static const Fp pm1o4("12598581548261874213705603106673800432416890341079302386700286569554565398542");
	w1 = w4 - 2; //PUT(t);PUT(w1); PUT(t); PUT(pm1o4);
	w = V2(pm1o4, M_toFp(w1)); //(w10,w1)
	//PUT(w);
	mie::Fp tmp;
	tmp = w;

	if (t != 1 && t) w = w / t;

	/*
	 * if(t==2)t_inv=543654;
	 * if(t==3)
	 * if(t==4)
	 * if(t==5)
	 * if(t==6)
	 * if(t==7)
	 * if(t>7)t_inv=mie::Fp(1)/t
	 * w=w*t_inv;
	 * */
	return true;
	//if (w*w==x) return true; else return false;
}

// It is cheaper to get the square root and verify it, then to check if it has a square root.
bool mikesqrt3mod4(mie::Fp& z, mie::Fp& x, bool flag=true)
{
	//M_uint t0, p0;
	mie::Fp z2 = x;
	mie::Fp z3;

	if (z2 == 4) {
		z2 = 2;
		return true;
	}

//mie::Fp pp1o4; pp1o4.set("4199527182753958071235201035557933477472296780359767462233428856518188466181");
	const int siTbl[] = {
		1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1
	};
	
	z2 = x;
	for (int i = 0; i <= 250; i++) {
		mie::Fp::square(z2, z2);
		if (siTbl[250 - i])
			mie::Fp::mul(z2, z2, x);
	}
	
	// we can even skip the verification, if we already know x has a square root
	if (flag)
	{
		mie::Fp::square(z3, z2);
		if (z3 == x)
		{
			z = z2;
			return true;
		}
		return false;
	}

	z = z2;
	return true;
}

bool ComplexSQRTFp2(Fp2 &u, Fp2 &v)
{
	mie::Fp t0, t1;
	u = v;
	
	
	if (u.b_.isZero())
	{
		mikesqrt3mod4(u.a_, u.a_);
		return true;
	}
	
	mie::Fp::square(t0, u.a_);
	mie::Fp::square(t1, u.b_);
	t0 += t1;
	
	if (!mikesqrt3mod4(t0, t0))//, false);
		return false;
	
	t1 = u.a_ + t0;
	t1 *= bn::Param::half;
	//mie::Fp::divBy2(t1, t1);

	if (!mikesqrt3mod4(t1, t1))
	{
		t1 = u.a_ - t0;
		t1 *= bn::Param::half;//mie::Fp::divBy2(t1, t1);
		mikesqrt3mod4(t1, t1, false);
	}
	
	u.a_ = t1;
	t1 = t1 + t1;
	u.b_ = u.b_ / t1;
	
	return true;
}

#include <openssl/sha.h>
/* rename H1_l as H1 */
void H1(mie::Fp& f, const char* M)
{
#if 1
	assert(HASH_LEN == 32);
	assert(SHA256_DIGEST_LENGTH == 32);
	assert(sizeof(f) == 32);
	unsigned char *md = (unsigned char*)&f; // dangerous
	SHA256((const unsigned char*)M, strlen(M), md);
	md[31] &= (1 << 5) - 1;
#else
	// Hash a zero-terminated string to a number < modulus
	using namespace bn::ecop;
	M_uint h;
//	char H1_M[HASH_LEN];
	assert(HASH_LEN == 32);
	uint64_t buf[4];
	char *H1_M = (char*)buf;
	int i;
	sha256_st sha256;
	sha256_init(&sha256);

	for (i = 0;; i++) {
		if (M[i] == 0) break;

		sha256_fillchunks(&sha256, M[i]);
	}

	sha256_hash(&sha256, H1_M);
	M_setBuf(h, buf, 4);
	buf[3] &= (uint64_t(1) << 61) - 1;
	assert(h < M_param_p());
	f = M_toFp(h);
#endif
}

#if 0 // use miracl
void H1(mie::Fp& f, const char* string)
{
	// Hash a zero-terminated string to a number < modulus
	using namespace bn::ecop;
	M_uint h;
	//const M_uint& p = bn::ParamT<mie::Fp, Fp2>::p;
	char s[HASH_LEN];
	int i, j;
	sha256 sh;
	shs256_init(&sh);

	for (i = 0;; i++) {
		if (string[i] == 0) break;

		shs256_process(&sh, string[i]);
	}

	shs256_hash(&sh, s);
	h = 1; j = 0; i = 1;

	for (;;) {
		h *= 256;

		if (j == HASH_LEN)  {
			h += i++;
			j = 0;

		} else               h += s[j++];

		if (h >= M_param_p()) break;
	}

	h %= M_param_p();
	f = h;
}
#endif

int Legendre2(const mie::Fp& a)
{
#if 1
	static const mpz_class p("16798108731015832284940804142231733909889187121439069848933715426072753864723");
	mpz_class x;
	M_Fp_to_mpz(x, a);
	return mpz_legendre(x.get_mpz_t(), p.get_mpz_t());
#else
        int k = 1;
        int v;
        M_uint temp;

        int t0;
        M_uint r;

        M_uint B = M_param_p();
        M_uint A = B;
        for (int i = 0; i < 4; i++) A[i] = a.get()[i];

        while (B!=1) {
                if (A==0) return 0;
                v = 0;
                while ((A[0]&1) == 0) {
                        v++;
                        A>>=1;
                }

                t0 = M_low32bit(B)&7;
                if (((v&1) == 1) && ((t0==3)||(t0==5))) k = -k;

                if (((A[0]&3)==3) && ((t0&3)==3)) k = -k;

                r = A;
                A = B % r;
                B = r;
        }

        if (B > 1) k = 0;
        return k;
#endif
}

int Xp2(const Fp2& a)
{
        Fp2 b = a;
        b.b_ = -b.b_;
        b *= a;

        return Legendre2(b.a_);
}

void hash_and_mapDetG1(mie::Fp& Q1, mie::Fp& Q2, const char* ID)
{
        mie::Fp t, SQRTM3, c0;
        mie::Fp a[3], x[3];
        int alpha[2];
        mie::Fp d, w;
        int c;
        int ii;

        SQRTM3.set("16798108731015832281326531451663778978262632916709829269670106367266327101444");
        c0.set("16798108731015832283133667796947756444075910019074449559301910896669540483083");
        H1(t, ID);

        w = t * t;
        w = w + 3;//y^2 = x^3 + 2
        //w = w + 1;
        Fp::inv(w, w);
        w = w * t;
        w = w * SQRTM3;
        x[0] =  t * w;
        x[0] =  x[0] - c0;
        x[0] = -x[0];

        x[1] =  x[0] + 1;
        x[1] = -x[1];

        x[2] = w * w;
        Fp::inv(x[2], x[2]);
        x[2] = x[2] + 1;

        for (int i = 0; i < 3; i++) {
                a[i] = x[i] * x[i];
                a[i] = a[i] * x[i];
                a[i] = a[i] + 2;//y^2 = x^3 + 2
        }

        alpha[0] = Legendre2(a[0]);//jackFp(a[0]);
        alpha[1] = Legendre2(a[1]);//jackFp(a[1]);

        ii = alpha[0] - 1;
        ii *= alpha[1];
        ii %= 3;
        if (ii==-2) ii=1;//!!!
        if (ii==-1) ii=2;//!!!

        Q1 = x[ii];
        c = Legendre2(t);//jackFp(t);
        mikesqrt3mod4(d, a[ii], false);
        if (c<0)
                Q2 = -d;
        else
                Q2 = d;

        return;
}

void hash_and_mapDetG2(Fp2& Q1, Fp2& Q2, const char* ID)
{
        static const Fp2 SQRTM3(Fp("16798108731015832281326531451663778978262632916709829269670106367266327101444"), Fp(0));
        static const Fp2 c0(Fp("16798108731015832283133667796947756444075910019074449559301910896669540483083"), Fp(0));
        Fp2 t;
        Fp2 a[3], x[3];
        int alpha[3];
        Fp2 d, w;
        int c;
        int ii;

        H1(t.a_, ID);
        t.b_ = t.a_;

        Fp2::square(w, t);
        w += bn::ParamT<Fp2>::b_invxi;
        w.a_ += 1;
        w.inverse();
        w *= t;
        w *= SQRTM3;

        x[0] =  t * w;
        x[0] -=  c0;
        Fp2::neg(x[0], x[0]);

        x[1] =  x[0] + 1;
        Fp2::neg(x[1], x[1]);
        Fp2::square(x[2], w);
        x[2].inverse();
        x[2].a_ += 1;

        for (int i = 0; i < 3; i++) {
                Fp2::square(a[i], x[i]);
                Fp2::mul(a[i], a[i], x[i]);
                a[i] += bn::ParamT<Fp2>::b_invxi;
        }

        alpha[0] = Xp2(a[0]);
        alpha[1] = Xp2(a[1]);

        ii = alpha[0] - 1;
        ii *= alpha[1];
        ii %= 3;//.a_[0] & 3;
        if (ii==-2) ii=1;//!!!

        Q1 = x[ii];
        c = Xp2(t);
        ComplexSQRTFp2(d, a[ii]);

        if (c<0)
                Q2 = -d;
        else
                Q2 = d;

        return;
}



// @note it seems to be hash onto curve, we have to more investigate this algorithm
void hash_and_mapB(mie::Fp& Q1, mie::Fp& Q2, const char* ID)
{
	mie::Fp x0, x3, y;

	H1(x0, ID);
	//mie::Fp Q[] = {x0, x0, mie::Fp("1")};

	while(true) {
		//x3 = x0;
		//x3 *= x3;
		//x3 *= x0;
        Fp::square(x3,x0);
        Fp::mul(x3,x3,x0);
		x3 += mie::Fp("2");

		if (mikesqrt3mod4(y, x3))
			break;
		
		mie::Fp::add(x0, x0, 1);
	}

	Q1 = x0;
	Q2 = y;
}

bool SetY_NewExtT(Fp2& X, Fp2& Y)
{
//  Y = (X * X * X) + bn::Param::b_invxi;
	Fp2::square(Y, X);
	Y *= X;
	Y += bn::Param::b_invxi;

	return ComplexSQRTFp2(Y, Y);
}

void hash_and_map2(Fp2& X, Fp2& Y, const char* ID)
{
    X.a_ = 1;
    H1(X.b_, ID);
    while (true)
    {
        if (SetY_NewExtT(X, Y)) break;
        X.b_ += 1;
    }
}

// @note Q and P must point to different places
// mmm... we should change that, P is of no use afterwards
void HashtoG2(Fp2 *Q, Fp2 *P)
{
    Fp2 Px[3];
    Fp2 P3x[3];

    Px[0] = P[0];
    Px[1] = P[1];
    Px[2] = 1;

    bn::ecop::ScalarMult(P3x,Px,M_param_abs_z());
    Px[0] = P3x[0];
	Px[1] = P3x[1];
	Px[2] = P3x[2];

    Fp2::neg(Px[1], Px[1]);

    dblJG2(P3x, Px);
    addJJJG2(P3x, P3x, Px);
    p_power_FrobeniusJac3(P3x, P3x);
    addJJJG2(Q, Px, P3x);
    p_power_FrobeniusJac3i(Px, Px, 2);
     addJJJG2(Q, Q, Px);
    Px[0] = P[0];
    Px[1] = P[1];
    Px[2] = 1;
    p_power_FrobeniusJac3i(Px, Px, 3);
    addJJJG2(Q, Q, Px);
}

/***
    Local Variables:
    c-basic-offset: 4
    indent-tabs-mode: t
    tab-width: 4
    End:
*/

