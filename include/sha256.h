/**
	@file
	@brief SHA-2 functions
    @author Zavattoni et al.
	@license modified new BSD license
	http://www.opensource.org/licenses/BSD-3-Clause
*/
#pragma once

#include <iostream>

#if !defined(_MSC_VER) || (_MSC_VER >= 1600)
#include <stdint.h>
#else
typedef unsigned __int64 uint64_t;
typedef __int64 int64_t;
typedef unsigned int uint32_t;
typedef int int32_t;
#endif

static const uint32_t sha256_h[8] = {
	0x6A09E667L, 0xBB67AE85L, 0x3C6EF372L, 0xA54FF53AL, 0x510E527FL, 0x9B05688CL, 0x1F83D9ABL, 0x5BE0CD19L
};

static const uint32_t sha256_k[64] = {
	0x428a2f98L, 0x71374491L, 0xb5c0fbcfL, 0xe9b5dba5L, 0x3956c25bL, 0x59f111f1L, 0x923f82a4L, 0xab1c5ed5L,
	0xd807aa98L, 0x12835b01L, 0x243185beL, 0x550c7dc3L, 0x72be5d74L, 0x80deb1feL, 0x9bdc06a7L, 0xc19bf174L,
	0xe49b69c1L, 0xefbe4786L, 0x0fc19dc6L, 0x240ca1ccL, 0x2de92c6fL, 0x4a7484aaL, 0x5cb0a9dcL, 0x76f988daL,
	0x983e5152L, 0xa831c66dL, 0xb00327c8L, 0xbf597fc7L, 0xc6e00bf3L, 0xd5a79147L, 0x06ca6351L, 0x14292967L,
	0x27b70a85L, 0x2e1b2138L, 0x4d2c6dfcL, 0x53380d13L, 0x650a7354L, 0x766a0abbL, 0x81c2c92eL, 0x92722c85L,
	0xa2bfe8a1L, 0xa81a664bL, 0xc24b8b70L, 0xc76c51a3L, 0xd192e819L, 0xd6990624L, 0xf40e3585L, 0x106aa070L,
	0x19a4c116L, 0x1e376c08L, 0x2748774cL, 0x34b0bcb5L, 0x391c0cb3L, 0x4ed8aa4aL, 0x5b9cca4fL, 0x682e6ff3L,
	0x748f82eeL, 0x78a5636fL, 0x84c87814L, 0x8cc70208L, 0x90befffaL, 0xa4506cebL, 0xbef9a3f7L, 0xc67178f2L
};

#define ONE_ZEROS  0x80
#define ZERO 0

typedef struct {
	uint32_t length[2]; //64-bit file length
	uint32_t h[8];  //0..7
	uint32_t w[80];     //64 elements, but can be up to 80
} sha256_st;

typedef sha256_st sha_st;

#define rr(a,b) (((a)>>b)|((a)<<(32-b)))
#define rs(a,b) ((a)>>b)

#define Che(e,f,g) ((e&f)^(~(e)&g))
#define Ma(a,b,c)  ((a&b)^(a&c)^(b&c))
#define Sigma0(a)  (rr(a,2)^rr(a,13)^rr(a,22))
#define Sigma1(e)  (rr(e,6)^rr(e,11)^rr(e,25))

#define S0(a) (rr(a,7)^rr(a,18)^rs(a,3))
#define S1(a) (rr(a,17)^rr(a,19)^rs(a,10))


void sha256_init(sha256_st* sha);
void sha256_loop512(sha256_st* sha);
void sha256_fillchunks(sha256_st* sha, int byte);
void sha256_hash(sha256_st* sha, char hash[32]);

