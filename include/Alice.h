#pragma once
/**
	@file
	@brief Encryption steps
    @author Zavattoni et al.
	@license modified new BSD license
	http://www.opensource.org/licenses/BSD-3-Clause
*/
#include "CypherText.h"
#include "MatrizLSSS.h"
#include "Precompute.h"
#include "PublicKey.h"
#include "CypherTextG2.h"
#include "PrecomputeG2.h"
#include "PublicKeyG2.h"

struct Alice {
	static void enc(CypherText* CT, const PublicKey& PK, const Fp& M, const MatrizType& mlsss, const Precompute& precompute);
    static void encG2(CypherTextG2* CT, const PublicKeyG2& PK, const Fp& M, const MatrizType& mlsss, const PrecomputeG2& precompute);
};

