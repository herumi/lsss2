#pragma once
/**
	@file
	@brief Access Tree structure
    @author Zavattoni et al.
	@license modified new BSD license
	http://www.opensource.org/licenses/BSD-3-Clause
*/
#include"Attribute.h"

class Tree {
	static int counter;
public:
	typedef enum {
	    NONE,
	    AND,
	    OR
	} OPERATOR;

	Tree();
	Tree(Attribute* atr);
	Tree(OPERATOR op, Tree* left, Tree* right);
	~Tree();
	void SetPoly(M_uint s);
	void clearPoly();
	/////
	M_uint getCste() { return Poly[1]; }
	void print(std::ostream& os);
	int isLeaf() const { return leaf; }
	std::string getName();
	Tree* getLeft();
	Tree* getRight();
	M_uint getID();
	OPERATOR getOper();

	friend std::ostream& operator<<(std::ostream& os, const Tree& t);
	friend std::istream& operator>>(std::istream& is, Tree& t);

private:
	Attribute* attribute;
	Tree* left, *right;
	int leaf;
	OPERATOR oper;
	M_uint id;
	M_uint Poly[2];
};

