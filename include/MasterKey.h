#pragma once
/**
	@file
	@brief Master Secret Key structure
    @author Zavattoni et al.
	@license modified new BSD license
	http://www.opensource.org/licenses/BSD-3-Clause
*/

#include "cpabe.h"

class MasterKey {
public:
	void SetGalpha(const Fp g_alpha[3]);
	void getG_alpha(Fp& x, Fp& y, Fp& z) const;

	friend std::ostream& operator<<(std::ostream& os, const MasterKey& mk);
	friend std::istream& operator>>(std::istream& is, MasterKey& mk);
private:
	Fp g_alpha[3];
};


