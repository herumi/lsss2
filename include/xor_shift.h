#pragma once
/**
	@file
	@brief XOR supportive function
    @author Zavattoni et al.
	@license modified new BSD license
	http://www.opensource.org/licenses/BSD-3-Clause
*/

class XorShift {
	unsigned int x_, y_, z_, w_;
public:
	/* generate uniform random value in [a, b) */
	explicit XorShift(int seed = 0)
	{
		init(seed);
	}
	explicit XorShift(const unsigned int buf[4])
	{
		init(buf);
	}
	void init(int seed = 0)
	{
		x_ = 123456789 + seed;
		y_ = 362436069;
		z_ = 521288629;
		w_ = 88675123;
	}
	void init(const unsigned int buf[4])
	{
		x_ = buf[0];
		y_ = buf[1];
		z_ = buf[2];
		w_ = buf[3];
	}
	/* [0, 2^32) random number */
	unsigned int get()
	{
		unsigned int t = x_ ^ (x_ << 11);
		x_ = y_;
		y_ = z_;
		z_ = w_;
		return w_ = (w_ ^ (w_ >> 19)) ^ (t ^ (t >> 8));
	}
	unsigned int operator()()
	{
		return get();
	}
	void read(void *buf, int byteSize)
	{
		unsigned char *p = (unsigned char *)buf;
		for (int i = 0; i < byteSize; i++) {
			p[i] = (unsigned char)get();
		}
	}
};

