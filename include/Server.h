#pragma once
/**
	@file
	@brief Setup and KeyGen steps
    @author Zavattoni et al.
	@license modified new BSD license
	http://www.opensource.org/licenses/BSD-3-Clause
*/

#include "cpabe.h"
#include "Precompute.h"
#include "PublicKey.h"
#include "MasterKey.h"
#include "Bob.h"
#include "Key.h"

class Server {
public:
	void Setup(PublicKey& PK, Precompute& prec);
	void KeyGen(Key& UserKey, const Bob& b, PublicKey* PK, Precompute* prec);
	void save(std::ostream& of)
	{
		of << MK;
	};
	void save(const char* filename)
	{
		std::ofstream fs(filename);
		fs << MK;
	};
	void load(std::istream& of)
	{
		of >> MK;
	}
	void load(const char* filename)
	{
		std::ifstream fs(filename);
		fs >> MK;
	}
private:
	MasterKey MK;
};

/***
    Local Variables:
    c-basic-offset: 4
    indent-tabs-mode: t
    tab-width: 4
    End:
*/
