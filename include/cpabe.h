#pragma once
/**
	@file
	@brief Input/Output helpers 
    @author Zavattoni et al.
	@license modified new BSD license
	http://www.opensource.org/licenses/BSD-3-Clause
*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <list>
#include <iosfwd>

#include "basedef.h"
#include "bn-multi.h"

extern M_sint jack(M_uint a, M_uint n);
extern Fp V2(Fp m, Fp P);
//BOOL mikesqrt(Fp &w, Fp &x);
extern void H1(mie::Fp& f, const char* string);
extern void hash_and_mapB(Fp& Q1, Fp& Q2, const char* ID);

class PublicKey;
class CypherText;
class Key;
class Tree;

PublicKey* setup();
CypherText* enc(std::string s, PublicKey* PK, M_uint key);
M_uint inverse2(const M_uint& a, const M_uint& mod);
bool isInSet(const std::string s, const std::vector<std::string> Set);
void extended_gcd(const M_sint& a, const M_uint& b2, M_sint& c, M_sint& d);
int Lagrange(int id, const std::vector<int>& brother);

bool DecryptNode(CypherText* CT, Key* SK, Tree* x, Fp12& F);

template<class T>
std::ostream& serialize(std::ostream& os, const T* x, size_t n)
{
	for (size_t i = 0; i < n; i++) {
		os << x[i];
		os << ' ';
	}

	return os;
}

template<class T>
std::istream& deserialize(std::istream& is, T* x, size_t n)
{
	for (size_t i = 0; i < n; i++) {
		is >> x[i];
	}

	return is;
}

