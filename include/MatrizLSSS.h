#pragma once
/* -*- mode: c++ -*- */
/**
	@file
	@brief LSSS matrix structure
    @author Zavattoni et al.
	@license modified new BSD license
	http://www.opensource.org/licenses/BSD-3-Clause
*/

#include <fstream>
#include <stdio.h>
#include "global.h"
#include "cpabe.h"

class MatrizLSSS {
public:
	MatrizLSSS(const char* msg = 0)
		: m(1)
		, d(1)
		, ind(0)
		, rowT(0)
		, colT(0)
	{
		MatrizMSP();
		if (msg) setupByMessage(msg);
	}
	~MatrizLSSS()
	{
		if (matrix.Mat != NULL) {
			if (matrix.Mat == matriz.Mat)matriz.Mat = NULL;

			int r = rowT;
			matrix_free(matrix.Mat);
			rho_free(matrix.rho, r);
			matrix.Mat = NULL;
		}

		if (matriz.Mat != NULL) {
			rho_free(matriz.rho, matriz.Mat->rows);
			matrix_free(matriz.Mat);
			matriz.Mat = NULL;
		}
	}
	void setMatrixType(const MatrizType& mlsss) { matriz = mlsss; }
	MatrizType getMatrixType() const { return matriz; }
	MATRIX* matrix_allocate (int row, int col);
	void matrix_free( MATRIX* A );
	MAP* rho_allocate(int row);
	void rho_free(MAP* r, int T)
	{
		for (int i = 0; i < T; i++) {
			if (r[i].atributo != NULL) delete [] r[i].atributo;
		}

		delete[] r;
	}
	void iniciaMatriz ();
	MatrizType matrix;
	bool getCoeffients(MATRIX* A, COEFF* coef, M_sint* delta );
	int ex(nodeType* p);
	void parser(const char* input, MatrizLSSS* M);

	friend std::ostream& operator<<(std::ostream& os, const MatrizLSSS& M);
	friend std::istream& operator>>(std::istream& os, MatrizLSSS& M);

	void save(const char* filename)
	{
		std::ofstream fs(filename);
		fs << *this;
	}

	std::string toStringMatrizType() const
	{
		std::stringstream sst;

		for (int j = 0; j < matriz.Mat->rows; j++) {
			for (int i = 0; i < matriz.Mat->cols; i++) {
				sst << " " << matriz.Mat->ptr[j][i];
			}

			sst << " " << matriz.rho[j].atributo << std::endl;
		}
		return sst.str();
	}

	void setupByMessage(const char* msg)
	{
		parser(msg, this); // QQQ
		setMatrixType(matrix); // QQQ
	}
	void increment()
	{
		rowT++;
		colT++;
	}

private:
	int m, d, ind, rowT, colT;
	M_sint MSP[tmsp][tmsp];
	void MatrizMSP()
	{
		int i, j;

		for (i = 0; i < tmsp; i++) {
			MSP[0][i] = 1;
			MSP[i][0] = 1;
		}

		for (i = 1; i < tmsp; i++) {
			for (j = 1; j < tmsp; j++) {
				MSP[i][j] = MSP[i - 1][j] * (j + 1);
			}
		}
	}
	int conteo(nodeType* p, int ind);
	MatrizType matriz;
	MATRIX* Matt;
	void trianguliza(MATRIX* A);
	int llena(int t2, int m2);
};

/***
    Local Variables:
    c-basic-offset: 4
    indent-tabs-mode: t
    tab-width: 4
    End:
*/
