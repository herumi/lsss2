#pragma once
/**
	@file
	@brief Fast integer functions
    @author Zavattoni et al.
	@license modified new BSD license
	http://www.opensource.org/licenses/BSD-3-Clause
*/
#include "bn-multi.h"

using namespace mie;

#include <gmpxx.h>
inline void M_Fp_to_mpz(mpz_class &z, const Fp& x)
{
	static const uint64_t _1[4] = { 1, 0, 0, 0 };
	Fp t;
	Fp::mul(t, x, *(const Fp*)_1);
	mpz_import(z.get_mpz_t(), 4, -1, sizeof(uint64_t), 0, 0, &t[0]);
}
#ifdef MIE_ATE_USE_GMP
typedef mpz_class M_sint;
typedef mpz_class M_uint;

inline const M_uint& M_param_r()
{
//	static M_uint r(bn::Param::r.toString());
	static M_uint r("16798108731015832284940804142231733909759579603404752749028378864165570215949");
	return r;
}

inline const M_sint& M_param_z()
{
//	static M_sint z(bn::Param::z.toString());
	static M_sint z("-4647714815446351873");
	return z;
}

inline const M_uint& M_param_p()
{
//	static M_uint p(bn::Param::p.toString());
	static M_uint p("16798108731015832284940804142231733909889187121439069848933715426072753864723");
	return p;
}

inline const M_uint& M_param_abs_z()
{
	static M_uint abs_z("4647714815446351873");
	return abs_z;
}

inline void M_set(M_sint& z, const M_uint& x)
{
	z = x;
}
inline void M_setBuf(M_uint& z, const uint64_t *buf, int size)
{
	mpz_import(z.get_mpz_t(), size, -1, sizeof(*buf), 0, 0, buf);
}
template<class T>
inline void M_divmod(T& q, T& r, const T& x, const T& y)
{
	mpz_divmod(q.get_mpz_t(), r.get_mpz_t(), x.get_mpz_t(), y.get_mpz_t());
}

inline M_uint M_abs(const M_sint& x)
{
	return abs(x);
}
inline bool M_isNegative(const M_sint& x)
{
	return mpz_sgn(x.get_mpz_t()) < 0;
}
template<class T>
inline bool M_isZero(const T& x)
{
	return mpz_sgn(x.get_mpz_t()) == 0;
}
typedef mp_limb_t block_type;

template<class T>
void M_setStr(T& z, const std::string& str)
{
	z.set_str(str, 0);
}
static inline std::string M_toStr(const M_uint& x)
{
	return x.get_str(10);
}

static inline mie::Fp M_toFp(const M_uint& x)
{
	return Fp(std::string("0x") + x.get_str(16));
}
static inline M_uint M_fromFp(const mie::Fp& x)
{
	M_uint z;
	z.set_str(x.toString(16), 0);
	return z;
}
static inline bool M_testBit(const M_uint& x, int i)
{
	int unit_pos = i / (sizeof(block_type) * 8);
	int bit_pos  = i % (sizeof(block_type) * 8);
	block_type mask = block_type(1) << bit_pos;
	return (M_block(x, unit_pos) & mask) != 0;
}

#else
typedef mie::Vsint M_sint;
typedef mie::Vuint M_uint;

inline const M_uint& M_param_r()
{
	return bn::Param::r;
}

inline const M_sint& M_param_z()
{
	return bn::Param::z;
}

inline const M_uint& M_param_p()
{
	return bn::Param::p;
}

inline const M_uint& M_param_abs_z()
{
	return bn::Param::z.get();
}

inline void M_set(M_sint& z, const M_uint& x)
{
	z.set(x);
}
inline void M_setBuf(M_uint& z, const uint64_t *buf, int size)
{
	z.set(buf, size);
}
template<class T>
void M_divmod(T& q, T& r, const T& x, const T& y)
{
	T::div(&q, r, x, y);
}

inline const M_uint& M_abs(const M_sint& x)
{
	return x.get();
}
template<class T>
inline bool M_isZero(const T& x)
{
	return x.isZero();
}
template<class T>
inline size_t M_bitLen(const T& x)
{
	return x.bitLen();
}
inline bool M_isNegative(const M_sint& x)
{
	return x.isNegative();
}
template<class T>
void M_setStr(T& z, const std::string& str)
{
	z.set(str);
}
template<class T>
std::string M_toStr(const T& x)
{
	return x.toString();
}
inline size_t M_blockSize(const M_uint& x)
{
	return x.size();
}
typedef M_uint::value_type block_type;
static inline block_type M_block(const M_uint& x, size_t i)
{
	return x[i];
}
static inline uint32_t M_low32bit(const M_uint& x)
{
	return (uint32_t)x[0];
}

static inline mie::Fp M_toFp(const M_uint& x)
{
	return Fp(x);
}
static inline M_uint M_fromFp(const mie::Fp& x)
{
	return x.get();
}
static inline bool M_testBit(const M_uint& x, int i)
{
	return x.testBit(i);
}

#endif

typedef mie::Fp Fp;
typedef bn::Fp2T<Fp> Fp2;
typedef bn::Fp6T<Fp2> Fp6;
typedef bn::Fp12T<Fp6> Fp12;

#ifndef NUM_OF_ARRAY
#define NUM_OF_ARRAY(x) (sizeof(x) / sizeof(*x))
#define NUM_OF_DBL_ARRAY(x) (sizeof(x) / sizeof(x[0][0]))
#endif

struct Pos {
	const int w_;
	explicit Pos(int w)
		: w_(w)
	{
	}
	int get(int y, int x) const
	{
		return y * w_ + x;
	}
};

