#pragma once
/**
	@file
	@brief Public and Private Key structures (G2 version)
    @author Zavattoni et al.
	@license modified new BSD license
	http://www.opensource.org/licenses/BSD-3-Clause
*/

#include <map>
#include <fstream>
#include "cpabe.h"
#include "PointG2.h"
#include "PointG1.h"
// #include "PrecomputeG2.h"

class KeyG2 {
public:
	const Fp2* getK() const { return K; }
	Fp2* getK_W() { return K; }
	const Fp* getL() const { return L; }
	Fp* getL_W() { return L; }
	Fp2_map& getK_Att_W() { return K_Att; }
	const Fp2_map& getK_Att() const { return K_Att; }
	bool isOnAttributes(const std::string& str) const
	{
		return K_Att.find(str) != K_Att.end();
	}
	friend std::ostream& operator<<(std::ostream& os, const KeyG2& key);
	friend std::istream& operator>>(std::istream& is, KeyG2& key);

	void save(const char* filename)
	{
		std::ofstream fs(filename);
		fs << *this;
	}

	void load(const char* filename)
	{
		std::ifstream fs(filename);
		fs >> *this;
	}

    /*
    * As for precomputing the line values of the pairing for the
    * given attributes (K_i) in the SecretKey, and the K element.
    */
    void PrecomputeForPairing(const std::vector<std::string>& s) {
        Fp2 A[s.size()+1][3];
        	
        for (size_t i = 0; i < s.size(); i++) {
            PointG2& puntoG2 = K_Att[s[i]];
            Fp2* Q = puntoG2.getG2_W();
            A[i][0] = Q[0];
            A[i][1] = Q[1];
            A[i][2] = Q[2];
        }
        A[s.size()][0] = K[0];
        Fp2::neg(A[s.size()][1], K[1]);
        A[s.size()][2] = K[2];
	    PrecomputePairingKnownG2(this->l, s.size()+1, A);
	}
	void getPrecomputeForPairing(Fp6 l[][70], int n) const
    {
        if (n > MAXATTRIBUTES + 1) return;
        
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < 70; j++) 
            l[i][j] = this->l[i][j];
        }
    }

private:
	Fp2 K[3];
	Fp L[3];
	Fp2_map K_Att;
	Fp6 l[MAXATTRIBUTES+1][70];
};

/***
    Local Variables:
    c-basic-offset: 4
    indent-tabs-mode: t
    tab-width: 4
    End:
*/
