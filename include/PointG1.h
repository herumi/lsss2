#pragma once
/**
	@file
	@brief Point in G1 structure
    @author Zavattoni et al.
	@license modified new BSD license
	http://www.opensource.org/licenses/BSD-3-Clause
*/

#include <stdexcept>
#include <map>
#include "cpabe.h"
#include "opti.h"

class PointG1 {
public:
	PointG1();
	const Fp* getG1() const { return pt; }
	Fp* getG1_W() { return pt; }
	void setAtt(const std::string& Attr) { this->Attr = Attr; }
	const std::string& getAtt() const { return Attr; }
	const Fp (*getPsI() const)[2] { return PsI; }
	Fp (*getPsI_W())[2] { return PsI; }

	friend std::ostream& operator<<(std::ostream& os, const PointG1& pg1);
	friend std::istream& operator>>(std::istream& is, PointG1& pg1);
private:
	Fp pt[3];
	std::string Attr;
	Fp PsI[PRECOMPUTEknownS][2];
};

// @note should be changed
struct Fp_map : std::map<std::string, PointG1> {
	const PointG1& get(const std::string& str) const
	{
		const_iterator i = find(str);

		if (i == end()) {
			fprintf(stderr, "ERR : Fp_map has %s\n", str.c_str());
			throw std::range_error("no key");
		}
		return i->second;
	}
};

/***
    Local Variables:
    c-basic-offset: 4
    indent-tabs-mode: t
    tab-width: 4
    End:
*/
