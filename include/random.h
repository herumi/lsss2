/**
	@file
	@brief Random number generator
    @author Zavattoni et al.
	@license modified new BSD license
	http://www.opensource.org/licenses/BSD-3-Clause
*/
#pragma once
#include <mie/random_generator.hpp>
#include "basedef.h"
#include "xor_shift.h"

#define RANDOM_ALWAYS_SAME_SEED

struct RandInt {
#ifdef RANDOM_ALWAYS_SAME_SEED
	XorShift rg_;
#else
	mie::RandomGenerator rg_;
#endif
	M_uint get()
	{
		uint64_t buf[4];
		setRand(buf);

		M_uint x;
		M_setBuf(x, buf, 4);
		return x;
	}
	Fp getFp()
	{
		uint64_t buf[4];
		setRand(buf);

		Fp x(buf);
		return x;
	}
private:
	void setRand(uint64_t (&buf)[4])
	{
#if 1
		rg_.read(buf, 4 * sizeof(uint64_t));
#else
		for (int i = 0; i < 4; i++) {
			buf[i] = rg_();
		}
#endif

		buf[3] >>= 3; // QQQ:clear 3 bit to be less than p
	}
};


