#pragma once
/**
	@file
	@brief Precomputing steps
    @author Zavattoni et al.
	@license modified new BSD license
	http://www.opensource.org/licenses/BSD-3-Clause
*/

#define BENCH_TIMES	50

#include <fstream>

#include "cpabe.h"
#include "opti.h"
#include <map>
#include "PointG1.h"
#include "bench.h"

namespace cpabe {

struct Param {
	typedef mie::Fp Fp;
	typedef bn::Fp2 Fp2;

	static void init();

	static const Fp* getG1Gen() { return g1_; }
	static const Fp2* getG2Gen() { return g2_; }

private:
	static Fp g1_[3];
	static Fp2 g2_[3];

	static void setG1Gen3(Fp* g1);
	static void setG1Gen2(Fp* g1);
	static void setG2Gen3(Fp2* g2);
	static void setG2Gen2(Fp2* g2);
};

} // namespace cpabe

class Precompute {
public:
	void setElement();
	void setFeAlpha(const Fp12 (*fe_alpha))
	{
		for (int j = 0; j < PRECOMPUTEknownS; j++) {
			this->fe_alpha[j] = fe_alpha[j];
		}
	}
	const M_sint& getBetaS() const { return BetaS_; }
	const M_sint* getWB() const { return WB; }
	const M_sint (*getBB() const)[4] { return BB; }
	const M_sint (*getSB() const)[2] { return SB; }
	const M_sint* getW() const { return W; }
	void getQsI(Fp2 QsIPrec[PRECOMPUTEknownS][2]) const
	{
		for (int j = 0; j < PRECOMPUTEknownS; j++) {
			QsIPrec[j][0] = QsI[j][0];
			QsIPrec[j][1] = QsI[j][1];
		}
	}
	const Fp2 (* getQsIPtr() const) [2] { return QsI; }
	const Fp12 (* getFePtr() const) { return fe; }

	void getFeAlpha(Fp12 fe_alpha[PRECOMPUTEknownS]) const
	{
		for (int j = 0; j < PRECOMPUTEknownS; j++) {
			fe_alpha[j] = this->fe_alpha[j];
		}
	}
	const Fp12 (*getFeAlphaPtr() const) { return fe_alpha; }
	const Fp12& getE() const { return e; }
	const mie::Fp (*getPsI() const)[2] { return PsI; }
	const mie::Fp (*getPsIga() const)[2] { return PsIga; }
	mie::Fp (*getPsIga_W())[2] { return PsIga; }

	Fp_map& getSetOfAttributes_W() { return SetOfAttributes; }
	const Fp_map& getSetOfAttributes() const { return SetOfAttributes; }

	friend std::ostream& operator<<(std::ostream& os, const Precompute& prec);
	friend std::istream& operator>>(std::istream& os, Precompute& prec);

	void save(const char* filename)
	{
		std::ofstream fs(filename);
		fs << *this;
	}

	void load(const char* filename)
	{
		std::ifstream fs(filename);
		fs >> *this;
	}

	void initByVector(const std::vector<std::string>& vec)
	{
		setElement();
		const M_sint& BetaS = BetaS_;
		Fp_map& Attributes = getSetOfAttributes_W();

		for (size_t i = 0; i < vec.size(); i++) {
			PointG1& puntoG1 = Attributes[vec[i]];
			Fp* tmp = puntoG1.getG1_W();
			/* @note This function is declared in opti.h */
			hash_and_mapDetG1(tmp[0], tmp[1], vec[i].c_str());
			tmp[2] = Fp(1);
			Fp tmp_pack[2];
			tmp_pack[0] = tmp[0];
			tmp_pack[1] = tmp[1];
			puntoG1.setAtt(vec[i]);
			/* @note This function is declared in opti.h */
			precomputeG1knownSC(puntoG1.getPsI_W(), tmp_pack,  getW(), getSB(), BetaS);
			// Fp::mul(tmp[0],tmp[0],BetaS.get()); // is necessary? QQQ
		}
	}
    M_uint t256;
    M_uint Half_Zr;

private:
	Fp2 g2_pack[2];
	Fp2 QsI[PRECOMPUTEknownS][2];
	Fp12 e;
	Fp12 fe[PRECOMPUTEknownS];
	Fp12 fe_alpha[PRECOMPUTEknownS];
	M_sint BetaS_;
	M_sint WB[4];
	M_sint BB[4][4];
	M_sint SB[2][2];
	M_sint W[2];
	Fp PsI[PRECOMPUTEknownS][2];
	Fp PsIga[PRECOMPUTEknownS][2];
	Fp_map SetOfAttributes;
};


/***
    Local Variables:
    c-basic-offset: 4
    indent-tabs-mode: t
    tab-width: 4
    End:
*/

