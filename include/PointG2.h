#pragma once
/**
	@file
	@brief Point in G2 structure
    @author Zavattoni et al.
	@license modified new BSD license
	http://www.opensource.org/licenses/BSD-3-Clause
*/

#include "cpabe.h"
#include <map>
#include "opti.h"

class PointG2 {
public:
	PointG2();
	const Fp2* getG2() const { return pt; }
	Fp2* getG2_W() { return pt; }

	void setAtt(const std::string& Attr) { this->Attr = Attr; }
	const std::string& getAtt() const { return Attr; }

    const Fp2 (*getQsI() const)[2] { return QsI; }
    Fp2 (*getQsI_W())[2] { return QsI; }

	friend std::ostream& operator<<(std::ostream& os, const PointG2& pg2);
	friend std::istream& operator>>(std::istream& is, PointG2& pg2);

private:
	Fp2 pt[3];
	std::string Attr;
    Fp2 QsI[PRECOMPUTEknownS][2];
};

// @note should be changed
struct Fp2_map : std::map<std::string, PointG2> {
	const PointG2& get(const std::string& str) const
	{
		const_iterator i = find(str);

		if (i == end()) {
			fprintf(stderr, "ERR : Fp2_map has %s\n", str.c_str());
			throw std::range_error("no key");
		}
		return i->second;
	}
};



