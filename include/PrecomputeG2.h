#pragma once
/* -*- mode: c++ -*- */
/**
	@file
	@brief Precomputing steps (G2 version)
    @author Zavattoni et al.
	@license modified new BSD license
	http://www.opensource.org/licenses/BSD-3-Clause
*/

#define BENCH_TIMES	50

#include <fstream>

#include "cpabe.h"
#include "opti.h"
#include <map>
#include "PointG2.h"
#include "bench.h"

class PrecomputeG2 {
public:
    void setElement();
    void setFeAlpha(const Fp12 (*fe_alpha))
    {
        for (int j = 0; j < PRECOMPUTEknownS; j++) {
            this->fe_alpha[j] = fe_alpha[j];
        }
    }
    const M_sint& getBetaS() const { return BetaS_; }
    const M_sint* getWB() const { return WB; }
    const M_sint (*getBB() const)[4] { return BB; }
    const M_sint (*getSB() const)[2] { return SB; }
    const M_sint* getW() const { return W; }
    void getQsI(Fp2 QsIPrec[PRECOMPUTEknownS][2]) const
    {
        for (int j = 0; j < PRECOMPUTEknownS; j++) {
            QsIPrec[j][0] = QsI[j][0];
            QsIPrec[j][1] = QsI[j][1];
        }
    }
    void getPsI(mie::Fp PsIPrec[PRECOMPUTEknownS][2]) const
    {
        for (int j = 0; j < PRECOMPUTEknownS; j++) {
            PsIPrec[j][0] = PsI[j][0];
            PsIPrec[j][1] = PsI[j][1];
        }
    }

    const Fp2 (* getQsIPtr() const) [2] { return QsI; }
    const Fp12 (* getFePtr() const) { return fe; }

    void getFeAlpha(Fp12 fe_alpha[PRECOMPUTEknownS]) const
    {
        for (int j = 0; j < PRECOMPUTEknownS; j++) {
            fe_alpha[j] = this->fe_alpha[j];
        }
    }
    const Fp12 (*getFeAlphaPtr() const) { return fe_alpha; }
    const Fp12& getE() const { return e; }
    const mie::Fp (*getPsI() const)[2] { return PsI; }
    const Fp2 (*getQsIga() const)[2] { return QsIga; }
    Fp2 (*getQsIga_W())[2] { return QsIga; }

    Fp2_map& getSetOfAttributes_W() { return SetOfAttributes; }
    const Fp2_map& getSetOfAttributes() const { return SetOfAttributes; }

	friend std::ostream& operator<<(std::ostream& os, const PrecomputeG2& prec);
	friend std::istream& operator>>(std::istream& os, PrecomputeG2& prec);

	void save(const char* filename)
	{
		std::ofstream fs(filename);
		fs << *this;
	}

	void load(const char* filename)
	{
		std::ifstream fs(filename);
		fs >> *this;
	}

	void initByVector(const std::vector<std::string>& vec)
	{
		setElement();
		//const M_sint& BetaS = BetaS_;
		Fp2_map& Attributes = getSetOfAttributes_W();
        Fp2 G2tmpA[vec.size()][2];
        Fp2 G2tmpJ[vec.size()][3];

		for (size_t i = 0; i < vec.size(); i++) {
			/* @note This function is declared in opti.h */
			hash_and_mapDetG2(G2tmpA[i][0], G2tmpA[i][1], vec[i].c_str());
            HashtoG2(G2tmpJ[i], G2tmpA[i]);
		}
        MultiNormalizeFp2(G2tmpA, G2tmpJ, vec.size());
        for (size_t i = 0; i < vec.size(); i++) {
            PointG2& puntoG2 = Attributes[vec[i]];
            puntoG2.setAtt(vec[i]);
            precomputeG2knownSC(puntoG2.getQsI_W(), G2tmpA[i],  getWB(), getBB());
        }
	}
    M_uint t256;
    M_uint Half_Zr;

private:
	Fp2 g2_pack[2];
	Fp2 QsI[PRECOMPUTEknownS][2];
	Fp12 e;
	Fp12 fe[PRECOMPUTEknownS];
	Fp12 fe_alpha[PRECOMPUTEknownS];
	M_sint BetaS_;
	M_sint WB[4];
	M_sint BB[4][4];
	M_sint SB[2][2];
	M_sint W[2];
	Fp PsI[PRECOMPUTEknownS][2];
	Fp2 QsIga[PRECOMPUTEknownS][2];
	Fp2_map SetOfAttributes;
};


/***
    Local Variables:
    c-basic-offset: 4
    indent-tabs-mode: t
    tab-width: 4
    End:
*/

