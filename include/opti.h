#pragma once
/**
	@file
	@brief Miscelaneous functions
    @author Zavattoni et al.
	@license modified new BSD license
	http://www.opensource.org/licenses/BSD-3-Clause
*/
#define W3
#ifdef W3
#define WNAF 3
#define PRECOMPUTE 2
#endif
#ifdef W7
#define WNAF 7
#define PRECOMPUTE 32
#endif
#define WNAFknown 7
#define PRECOMPUTEknown 256
#define PRECOMPUTEknownS 128

#define MAXATTRIBUTES 20

#include <stdio.h>
#include <memory.h>
#include "xbyak/xbyak_util.h"
extern Xbyak::util::Clock sclk;
#include <iostream>
//#include "util.h"
#include <vector>
#include <math.h>
#include "basedef.h"

//#include <omp.h>
#include "sha256.h"

extern Fp2 QsI [4][PRECOMPUTEknownS][2];
extern M_sint WB[4];
extern M_sint BB[4][4];
extern M_sint SB[2][2];
extern M_sint W[2];
extern M_sint BetaS;
void dblJG2(Fp2 c[3], const Fp2 a[3]);

#define HASH_LEN 32

int myPow(int x, int p);

//G1
void G1_mul(mie::Fp* R, const M_uint& Vk, const mie::Fp* PA, const M_sint* WB, const M_sint (*BB)[2], const M_sint& Beta);
void DecompG1(M_sint* u, const M_uint& Vk, const M_sint* WB, const M_sint (*BB)[2]);
void precomputeG1(mie::Fp QsI[2][PRECOMPUTE][2], const mie::Fp P[2], const mie::Fp Q[2], M_sint u[2]);
void precomputeG1b(mie::Fp QsI[2][PRECOMPUTE][2], const mie::Fp* P, const M_sint& Beta);
void precomputeG1known(mie::Fp QsI[2][PRECOMPUTEknown][2], const mie::Fp* P, const M_sint& Beta);
void G1_mulknown(mie::Fp* R, const M_uint& Vk, const mie::Fp PsI[2][PRECOMPUTEknown][2], const M_sint* WB, const M_sint (*BB)[2]);
void precompute(mie::Fp* x, mie::Fp* y, const mie::Fp* P, int w);
void get_points(mie::Fp x[], mie::Fp y[], mie::Fp P[], int w, mie::Fp l[]);
void get_deltai(mie::Fp* l, mie::Fp* e, const mie::Fp* f, int w);
void invert_di(mie::Fp* e, mie::Fp* f, const mie::Fp* d, int w);
void get_di(mie::Fp d[], mie::Fp P[], int w);
void addJJAG1b(mie::Fp* c, const mie::Fp* a, const mie::Fp* b);
void addJJJG1(mie::Fp* c, const mie::Fp* a, const mie::Fp* b);
void dblJG1(mie::Fp* c, const mie::Fp* a);
void G1_mulSmall(mie::Fp* R, const M_uint& k, const mie::Fp* PA);
void G1_mulSmallJ(mie::Fp* R, const M_uint& k, const mie::Fp* PA);
void G1_mulSmall2(mie::Fp* R, const M_uint& k, const mie::Fp* PA);
void precomputeG1Small(mie::Fp QsI[][2], const mie::Fp* P, const M_sint& u);
void precomputeG1SmallJ(mie::Fp QsI[][3], const mie::Fp* P, const M_sint& u);

void RecodeForPrecompC2(unsigned char out[32], const M_uint& Vk, const uint64_t PT[8][256]);
void precomputeG1knownC(mie::Fp PP[256][2], const mie::Fp* P, const M_sint* WB, const M_sint (*BB)[2], M_sint Beta);
void G1_mulknownC(mie::Fp* R, const M_uint& Vk, const mie::Fp PP[256][2], const uint64_t PT[8][256]);
void precomputeG2knownC(Fp2 PQ[256][2], const Fp2 Q[2], const M_sint* WB, const M_sint (*BB)[4]);
void G2_mulknownC(Fp2* R, const M_uint& Vk, const Fp2 PQ[256][2], uint64_t PT[8][256]);
void precomputeGTknownC(Fp12 PF[256], const Fp12& f, const M_sint* WB, const M_sint (*BB)[4]);
void GT_expoknownC(Fp12& R, const M_uint& Vk, const Fp12 PF[256], uint64_t PT[8][256]);

static inline uint32_t log2(const uint32_t x) {
  uint32_t y;
  asm ( "\tbsr %1, %0\n"
      : "=r"(y)
      : "r" (x)
  );
  return y;
}
void RecodeForPrecompSC(unsigned char out[32], const M_uint &Vk, const uint64_t PT[8][256], const M_uint & t256, const M_uint &Half_Zr);
void precomputeG1knownSC(mie::Fp PP[128][2], const mie::Fp* P, const M_sint *WB, const M_sint (*BB)[2], M_sint Beta);
void G1_mulknownSC(mie::Fp *R, const M_uint &Vk, const mie::Fp PP[128][2], const uint64_t PT[8][256], const M_uint & t256, const M_uint &Half_Zr);
void precomputeG2knownSC(Fp2 PQ[128][2], const Fp2 Q[2], const M_sint *WB, const M_sint (*BB)[4]);
void G2_mulknownSC(Fp2* R, const M_uint& Vk, const Fp2 PQ[128][2], uint64_t PT[8][256], const M_uint & t256, const M_uint &Half_Zr);
void precomputeGTknownSC(Fp12 PF[128], const Fp12 &f, const M_sint *WB, const M_sint (*BB)[4]);
void GT_expoknownSC(Fp12& R, const M_uint& Vk, const Fp12 PF[128], uint64_t PT[8][256], const M_uint & t256, const M_uint &Half_Zr);

////////////////////////////////////////////////////////////
// G2 multiplications
////////////////////////////////////////////////////////////
void G2_mulSmall(Fp2* R, const M_uint& k, const Fp2* QA);
void G2_mulSmall2(Fp2* R, const M_uint& k, const Fp2* QA);
void precomputeG2Small(Fp2 QPrec[][2], const Fp2* Q, const M_sint& u);
void G2_mul(Fp2* Q, Fp2* P, const M_uint& Vk, const M_sint* WB, const M_sint (*BB)[4]); //,const Fp2 half);
void G2_mulknown(Fp2* Q, const M_uint& Vk, const M_sint* WB, const M_sint (*BB)[4], const Fp2 QsI[4][PRECOMPUTEknown][2]);


////////////////////////////////////////////////////////////
// G2 precomputation
////////////////////////////////////////////////////////////
void get_diPUNTOG2(Fp2 d[], const Fp2 P[], int w);
void invert_diPUNTOG2(Fp2* e, Fp2* f, const Fp2* d, int w);
void get_deltaiPUNTOG2(Fp2* l, const Fp2* e, const Fp2* f, int w);
void get_pointsPUNTOG2(Fp2 x[], Fp2 y[], Fp2 P[], int w, Fp2 l[]);
void precomputePUNTOG2(Fp2* x, Fp2* y, const Fp2* P, int w);

////////////////////////////////////////////////////////////
// G2 arithmetic
////////////////////////////////////////////////////////////
void addJJAG2c(Fp2* c, const Fp2* a, const Fp2* b);
void dblJG2(Fp2* c, const Fp2* a);
void addJJJG2(Fp2* c, const Fp2* a, const Fp2* b);

////////////////////////////////////////////////////////////
// GS
////////////////////////////////////////////////////////////
//void NAFw(M_uint mp[], M_uint mn[], M_uint& tlen, M_uint k, int w);
void NAFw2(int mp[], int mn[], int& tlen, const M_uint& k, int w);
void NAFw64(int mp[70], int mn[70], int& tlen, const M_uint& k, int w);
void NAFw128(int mp[130], int mn[130], int& tlen, const M_uint& k, int w);
void mad(M_sint& v0, const M_sint& WB, M_uint k);
void DecompG2(M_sint* u, const M_uint& Vk, const M_sint* WB, const M_sint BB[4][4]);
void p_power_Frobenius(Fp2* R, const Fp2* Q);


////////////////////////////////////////////////////////////
// G2 precomputation2
////////////////////////////////////////////////////////////
void precomputeG2(Fp2 QsI[4][PRECOMPUTE][2], Fp2 P[2], M_sint u[4]);
void precomputeG2b(Fp2 QsI[4][PRECOMPUTE][2], const Fp2 *P, M_sint u[4]);
void precomputeG2known(Fp2 (*QsI)[PRECOMPUTEknown][2], const Fp2* P);
//void precomputeG2known(Fp2 ***QsI,Fp2 P[2]);

////////////////////////////////////////////////////////////
// GT expo
////////////////////////////////////////////////////////////

void GT_expoSmall(Fp12& z, const Fp12& x, M_uint e);
void GT_expo(Fp12& z, const Fp12& x, const M_uint& e, const M_sint* WB, const M_sint (*BB)[4]);
void GT_expoknown(Fp12& z, const Fp12 fe[4][PRECOMPUTEknown], M_uint e, const M_sint* WB, const M_sint (*BB)[4]);
void precomputeGT(Fp12 fe[4][PRECOMPUTE], const Fp12& x);
void precomputeGTknown(Fp12 (*fe)[PRECOMPUTEknown], const Fp12& x);


////////////////////////////////////////////////////////////
// Map-To-Point
////////////////////////////////////////////////////////////
int jack(M_uint &a, M_uint &n);
bool mikesqrt(mie::Fp& w, mie::Fp& x);
bool mikesqrtbis(mie::Fp& w, mie::Fp& x, int& cpt);
bool mikesqrt3mod4(mie::Fp& w, mie::Fp& x, bool flag);
void H1(mie::Fp& f, const char* string);
void H1_l(mie::Fp& f, const char* M);
void hash_and_mapB(mie::Fp& Q1, mie::Fp& Q2, const char* ID);
void hash_and_map2(Fp2& Q1, Fp2& Q2, const char* ID);
void HashtoG2(Fp2 *Q, Fp2 *P);
void hash_and_mapDetG1(mie::Fp& Q1, mie::Fp& Q2, const char* ID);
void hash_and_mapDetG2(Fp2& Q1, Fp2& Q2, const char* ID);
void p_power_FrobeniusJac3(Fp2* R, const Fp2* Q);
void p_power_FrobeniusJac3i(Fp2* R, const Fp2* Q, int n);
bool SetY_NewExtT(Fp2& X, Fp2 & Y);

/***
    Local Variables:
    c-basic-offset: 4
    indent-tabs-mode: t
    tab-width: 4
    End:
*/
