#pragma once
/**
	@file
	@brief LSSS matrix functions
    @author Zavattoni et al.
	@license modified new BSD license
	http://www.opensource.org/licenses/BSD-3-Clause
*/
#define tmsp 20
#include <string.h>

//using namespace std;
#include "cpabe.h"

typedef enum { typeCon, typeId, typeOpr } nodeEnum;

/* constants */
typedef struct {
	int value;                  /* value of constant */
} conNodeType;

/* identifiers */
typedef struct {
	char str[50];         /* subscript to sym array */
} idNodeType;

/* operators */
typedef struct {
	int oper;                   /* operator */
	int nops;                   /* number of operands */
	struct nodeTypeTag* op[1];  /* operands (expandable) */
} oprNodeType;

typedef struct nodeTypeTag {
	nodeEnum type;              /* type of node */

	/* union must be last entry in nodeType */
	/* because operNodeType may dynamically increase */
	union {
		conNodeType con;        /* constants */
		idNodeType id;          /* identifiers */
		oprNodeType opr;        /* operators */
	};
} nodeType;


/* Matriz */
typedef class _MA {
public:
	int rows;
	int cols;
	M_sint** ptr;
	_MA(): rows(0), cols(0), ptr(NULL) {}

} MATRIX;

typedef class _MB {
public:
	_MB(): atributo(NULL), ind(0) {}
	char* atributo;
	int ind;
} MAP;

typedef class matrizLSSSType {
public:
	matrizLSSSType(): Mat(NULL), rho(NULL) {}
	MATRIX* Mat;
	MAP* rho;

} MatrizType;

typedef class _MC {
public:
	_MC(): ind(0) {}
	M_sint omega;
	int ind;
} COEFF;

extern int sym[26];

