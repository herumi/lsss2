#pragma once
/**
	@file
	@brief Public and Private Key structures
    @author Zavattoni et al.
	@license modified new BSD license
	http://www.opensource.org/licenses/BSD-3-Clause
*/
#include <map>
#include <fstream>
#include "cpabe.h"
#include "PointG2.h"
#include "PointG1.h"

class Key {
public:
	const Fp* getK() const { return K; }
	Fp* getK_W() { return K; }
	const Fp2* getL() const { return L; }
	Fp2* getL_W() { return L; }
	Fp_map& getK_Att_W() { return K_Att; }
	const Fp_map& getK_Att() const { return K_Att; }
	bool isOnAttributes(const std::string& str) const
	{
		return K_Att.find(str) != K_Att.end();
	}
	friend std::ostream& operator<<(std::ostream& os, const Key& key);
	friend std::istream& operator>>(std::istream& is, Key& key);

	void save(const char* filename)
	{
		std::ofstream fs(filename);
		fs << *this;
	}

	void load(const char* filename)
	{
		std::ifstream fs(filename);
		fs >> *this;
	}

	/*
	* As for precomputing the line values of the pairing for the
	* given L element.
	*/
	void PrecomputeForPairing() {
		Fp2 A[1][3];
		A[0][0] = this->L[0];
		A[0][1] = this->L[1];
		A[0][2] = this->L[2];
		PrecomputePairingKnownG2(l, 1, A);
	}

	void getPrecomputeForPairing(Fp6 l_T[][70]) const
	{
		for (int j = 0; j < 70; j++)
		{
			l_T[0][j] = this->l[0][j];
		}
	}

private:
	Fp K[3];
	Fp2 L[3];
	Fp_map K_Att;
	Fp6 l[1][70];
};

/***
    Local Variables:
    c-basic-offset: 4
    indent-tabs-mode: t
    tab-width: 4
    End:
*/
