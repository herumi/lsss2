#pragma once
/**
	@file
	@brief Public Key functions
    @author Zavattoni et al.
	@license modified new BSD license
	http://www.opensource.org/licenses/BSD-3-Clause
*/

#include <fstream>
#include "cpabe.h"

class PublicKey {
public:
	PublicKey()
	{
		e_alpha.clear();
	}
	void Set_Element(const Fp g1[3], const Fp2 g2[3], const Fp12& e, const Fp g_a[3])
	{
		this->g1[0] = g1[0];
		this->g1[1] = g1[1];
		this->g1[2] = g1[2];
		this->g2[0] = g2[0];
		this->g2[1] = g2[1];
		this->g2[2] = g2[2];
		this->e_alpha = e;
		this->g_a[0] = g_a[0];
		this->g_a[1] = g_a[1];
		this->g_a[2] = g_a[2];
	}

	const Fp12& getEalpha() const { return e_alpha; }
	const Fp* getg_a() const { return g_a; }

	friend std::ostream& operator<<(std::ostream& os, const PublicKey& pk);
	friend std::istream& operator>>(std::istream& os, PublicKey& pk);

	void save(const char* filename)
	{
		std::ofstream fs(filename);
		fs << *this;
	}

	void load(const char* filename)
	{
		std::ifstream fs(filename);
		fs >> *this;
	}

private:
	Fp g1[3];
	Fp2 g2[3];
	Fp12 e_alpha;
	Fp g_a[3];
};

/***
    Local Variables:
    c-basic-offset: 4
    indent-tabs-mode: t
    tab-width: 4
    End:
*/
