#pragma once
/**
	@file
	@brief Attribute structure
    @author Zavattoni et al.
	@license modified new BSD license
	http://www.opensource.org/licenses/BSD-3-Clause
*/

#include <string>
#include <iostream>

#include"cpabe.h"

class Attribute {
public:
	Attribute();
	Attribute(std::string* val);
	~Attribute();

	void print(std::ostream& os);

	M_uint getPolyCte();
	void SetPoly(M_uint p[2]);
	void clearPoly();
	std::string getName();
	void setID(M_uint id)
	{
		this->id = id;
	}
	M_uint getID()
	{
		return id;
	}
	void printPoly(std::ostream& os);

	friend std::ostream& operator<<(std::ostream& os, const Attribute& at);
	friend std::istream& operator>>(std::istream& os, Attribute& at);

private:
	std::string name;

	M_uint Poly[2];
	M_uint id;
};

