#pragma once
/* -*- mode: c++ -*- */
/**
	@file
	@brief CypherText structure and functions (G2 version)
    @author Zavattoni et al.
	@license modified new BSD license
	http://www.opensource.org/licenses/BSD-3-Clause
*/

#include "cpabe.h"
#include <map>

#include "Tree.h"
#include "PointG2.h"
#include "PointG1.h"
#include "MatrizLSSS.h"

class CypherTextG2 {
public:
	void SetC(const Fp12& C)
	{
		this->C = C;
	}
	void SetC_Prime(const Fp C_Prime[3])
	{
		this->C_Prime[0] = C_Prime[0];
		this->C_Prime[1] = C_Prime[1];
		this->C_Prime[2] = C_Prime[2];
	}
	Fp2_map& getCAtt_W() { return CAtt; }
	const Fp2_map& getCAtt() const { return CAtt; }
	Fp_map& getDAtt_W() { return DAtt; }
	const Fp_map& getDAtt() const { return DAtt; }
	const Fp12& getC() const { return C; }
	const Fp* getC_Prime() const { return C_Prime; }
	friend std::ostream& operator<<(std::ostream& os, const CypherTextG2& ct);
	friend std::istream& operator>>(std::istream& is, CypherTextG2& ct);

	void clear()
	{
		C.clear();

		for (size_t i = 0; i < NUM_OF_ARRAY(C_Prime); i++) {
			C_Prime[i].clear();
		}

		CAtt.clear();
		DAtt.clear();
	}

	void save(const char* filename)
	{
		std::ofstream fs(filename);
		fs << *this;
	}

private:
	Fp12 C;
	Fp C_Prime[3];
	Fp2_map CAtt;
	Fp_map DAtt;
};

/***
    Local Variables:
    c-basic-offset: 4
    indent-tabs-mode: t
    tab-width: 4
    End:
*/

