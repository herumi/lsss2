/* -*- coding: utf-8; mode: c++ -*- */
#pragma once
/**
	@file
	@brief Product of pairings (multipairing), and Montgomery inversion
    @author Zavattoni et al.
	@license modified new BSD license
	http://www.opensource.org/licenses/BSD-3-Clause
*/

#include "bn.h"

#include <vector>
#include <assert.h>
#define PUT(x) std::cout << #x "=" << (x) << std::endl;

#define BN_USE_MultiNormalizeJacobianFpAndFp2

namespace bn {
/*
	bn::Darray<Fp, 2> x(n); means Fp x[n][2];
*/
template<class T, size_t M>
struct Darray {
	std::vector<T> v_;
	Darray(size_t n) : v_(M * n) {}
	T* operator[](size_t i)
	{
		assert(i * M < v_.size());
		return &v_[i * M];
	}
	const T* operator[](size_t i) const
	{
		assert(i * M < v_.size());
		return &v_[i * M];
	}
	T (*get())[M] { return (T (*)[M])&v_[0]; }
	const T (*get() const)[M] { return (T (*)[M])&v_[0]; }
};

template<class Fp>
void MultiNormalizeFp(Fp P[][2], Fp PA[][3], int n)
{
	if (n == 0) return;
	if (n==1) {
		Fp P0[3];
		bn::ecop::NormalizeJac(P0,PA[0]);
		P[0][0] = P0[0];
		P[0][1] = P0[1];
		return;
	}
	int i;
	Fp d, d2t, d3t, d2, d3;
	Fp t0;
	std::vector<Fp> t(n * 2);

	for (i = 0; i < 2 * n; i++) t[i] = 1;

	d = PA[0][2];

	// (n-1)*(m)
	for (i = 1; i < n; i++) {
		Fp::mul(d, d, PA[i][2]);
	}

	Fp::inv(d, d);
	t[1] = PA[0][2];
	t[n + 1] = PA[n - 1][2];

	// (n-2)*(2*m)
	for (i = 2; i < n; i++) {
		Fp::mul(t[i], t[i - 1], PA[i - 1][2]);
		Fp::mul(t[n + i], t[n + i - 1], PA[n - i][2]);
	}

	// (n-2)*(m)
	for (i = 1; i < n - 1; i++) {
		Fp::mul(t[i], t[i], t[2 * n - i - 1]);
	}

	t[0] = t[2 * n - 1];

	// (n)*(4*m + s)
	for (i = 0; i < n; i++) {
		Fp::mul(t0, d, t[i]);
		Fp::square(d2, t0);
		Fp::mul(d3, d2, t0);
		Fp::mul(P[i][0], PA[i][0], d2);
		Fp::mul(P[i][1], PA[i][1], d3);
		//P[i][2]=1;
	}
}

template<class Fp>
void MultiNormalizeFp(bn::Darray<Fp, 2>& P, Fp PA[][3], int n)
{
	MultiNormalizeFp(P.get(), PA, n);
}

template<class Fp>
void MultiNormalizeFp2(Fp2T<Fp> Q[][2], Fp2T<Fp> QA[][3], int n)
{
	if (n == 0) return;
	if (n==1) {
		Fp2 Q0[3];
		bn::ecop::NormalizeJac(Q0,QA[0]);
		Q[0][0] = Q0[0];
		Q[0][1] = Q0[1];
		return;
	}

	typedef Fp2T<Fp> Fp2;
	int i;
	Fp2 d, d2t, d3t, d2, d3;
	Fp2 t0;
	std::vector<Fp2> t(n << 1);

	for (i = 0; i < 2 * n; i++) t[i] = 1;

	d = QA[0][2];

	// (n-1)*m2
	for (i = 1; i < n; i++) {
		Fp2::mul(d, d, QA[i][2]);
	}

	d.inverse();
	t[1] = QA[0][2];
	t[n + 1] = QA[n - 1][2];

	// (n-2)*(2*m2)
	for (i = 2; i < n; i++) {
		Fp2::mul(t[i], t[i - 1], QA[i - 1][2]);
		Fp2::mul(t[n + i], t[n + i - 1], QA[n - i][2]);
	}

	// (n-2)*(m2)
	for (i = 1; i < n - 1; i++) {
		Fp2::mul(t[i], t[i], t[2 * n - i - 1]);
	}

	t[0] = t[2 * n - 1];

	// (n)*(4*m2+s2)
	for (i = 0; i < n; i++) {
		Fp2::mul(t0, d, t[i]);
		Fp2::square(d2, t0);
		Fp2::mul(d3, d2, t0);
		Fp2::mul(Q[i][0], QA[i][0], d2);
		Fp2::mul(Q[i][1], QA[i][1], d3);
		//Q[i][2]=1;
	}
}

template<class Fp>
void MultiNormalizeFp2(bn::Darray<Fp2T<Fp>, 2>& Q, Fp2T<Fp> QA[][3], int n)
{
	MultiNormalizeFp2(Q.get(), QA, n);
}

template<class FF>
inline void NormalizeJacobianLatterPart(FF* out, const FF* in, const FF& inv)
{
	FF t0;
	FF::square(t0, inv);
	FF::mul(out[0], in[0], t0);
	FF::mul(t0, t0, inv);
	FF::mul(out[1], in[1], t0);
}

inline void InversionsByMontgomeryTrick(std::vector<mie::Fp>& outVecFp, std::vector<Fp2T<mie::Fp> >& outVecFp2, const mie::Fp* inVecFp, const size_t vecFpSize, const Fp2T<mie::Fp>* inVecFp2, const size_t vecFp2Size)
{
	typedef mie::Fp Fp;
	typedef Fp2T<mie::Fp> Fp2;

	assert(vecFpSize > 0);
	assert(vecFp2Size > 0);

	std::vector<Fp> succAccFp(vecFpSize);
	std::vector<Fp2> succAccFp2(vecFp2Size);

	succAccFp[0] = inVecFp[0 + 2];
	for (size_t i = 1; i < vecFpSize; i++) {
		succAccFp[i] = inVecFp[3 * i + 2] * succAccFp[i - 1];
	}

	// switch from Fp to Fp2.
	Fp2::mul_Fp_0(succAccFp2[0], inVecFp2[0 + 2], succAccFp[vecFpSize - 1]);
	for (size_t i = 1; i < vecFp2Size; i++) {
		succAccFp2[i] = inVecFp2[3 * i + 2] * succAccFp2[i - 1];
	}

	succAccFp2[vecFp2Size - 1].inverse();

	for (size_t i = vecFp2Size - 1; 0 < i; i--) {
		outVecFp2[i] = succAccFp2[i] * succAccFp2[i - 1];
		succAccFp2[i - 1] = succAccFp2[i] * inVecFp2[3 * i + 2];
	}

	// switch back from Fp2 to Fp.
	Fp2::mul_Fp_0(outVecFp2[0], succAccFp2[0], succAccFp[vecFpSize - 1]);
	const Fp2 t = succAccFp2[0] * inVecFp2[0 + 2];
	assert(!t.a_.isZero());
	assert(t.b_.isZero());
	succAccFp[vecFpSize - 1] = t.a_;

	for (size_t i = vecFpSize - 1; 0 < i; i--) {
		outVecFp[i] = succAccFp[i] * succAccFp[i - 1];
		succAccFp[i - 1] = succAccFp[i] * inVecFp[3 * i + 2];
	}
	outVecFp[0] = succAccFp[0];
}

/***
	Total cost:
	1 * invFp2 +
	numPointsFp2 * (6 * mulFp2 + 1 * sqFp2) - 2 * mulFp2 +
	numPointsFp * (6 * mulFp + sqFp) + 1 * mulFp.

	Translated by Fp without inversion:
	1 * invFp2
	+ (20 * m + 14 * r) * numPointsFp2
	+ (7 * m + 7 * r) * numPointsFp
	- (5 * m + 3*r)
*/
inline void MultiNormalizeJacobianFpAndFp2(mie::Fp* outPointsFp, Fp2T<mie::Fp>* outPointsFp2, const mie::Fp* inPointsFp, const size_t numPointsFp, const Fp2T<mie::Fp>* inPointsFp2, const size_t numPointsFp2)
{
	typedef mie::Fp Fp;
	typedef Fp2T<mie::Fp> Fp2;

	assert(numPointsFp > 0);
	assert(numPointsFp2 > 0);

	std::vector<Fp> invVecFp(numPointsFp);
	std::vector<Fp2> invVecFp2(numPointsFp2);

	InversionsByMontgomeryTrick(invVecFp, invVecFp2, inPointsFp,  numPointsFp, inPointsFp2, numPointsFp2);

	for (size_t i = 0; i < numPointsFp; i++) {
		NormalizeJacobianLatterPart(&outPointsFp[2 * i], &inPointsFp[3 * i], invVecFp[i]);
	}
	for (size_t i = 0; i < numPointsFp2; i++) {
		NormalizeJacobianLatterPart(&outPointsFp2[2 * i], &inPointsFp2[3 * i], invVecFp2[i]);
	}
}

template<class Fp>
void opt_ateMultiPairingJ(Fp12T<Fp6T<Fp2T<Fp> > >& f, int n, Fp2T<Fp> QA[][3], Fp PA[][3])
{
	typedef Fp2T<Fp> Fp2;
	typedef Fp6T<Fp2> Fp6;
	typedef Fp12T<Fp6> Fp12;
	Darray<Fp, 2> P(n);
	Darray<Fp2, 2> Q(n);
	Darray<Fp2, 3> T(n);

	Fp2 Q1[2];
	Fp6 l, l2;
	Fp12 ft;

	ft = 1;
	f = 1;

#ifdef BN_USE_MultiNormalizeJacobianFpAndFp2
	MultiNormalizeJacobianFpAndFp2(&P[0][0], &Q[0][0], &PA[0][0], n, &QA[0][0], n);
#else
	MultiNormalizeFp(P, PA, n);
	MultiNormalizeFp2(Q, QA, n);
#endif

	for (int i = 0; i < n; i++) {
		//Fp::neg(P[i][2], P[i][1]);
		T[i][0] = Q[i][0];
		T[i][1] = Q[i][1];
		T[i][2] = Fp2(1);
	}

	const int siTbl[] = {
		1, 1, 0, 0, 0,
		0, 0, 1, 1, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 1, 0, 0,
	};

	// 844kclk
	for (size_t i = 1; i < sizeof(siTbl) / sizeof(*siTbl); i++) {
		if (i > 1) Fp12::square(f);

		for (int j = 0; j < n; j++) {
			Fp6::pointDblLineEval(l, T[j], P[j]);
			Fp12::Dbl::mul_Fp2_024(f, l);
		}

		if (siTbl[i]) {
			for (int j = 0; j < n; j++) {
				Fp6::pointAddLineEval(l, T[j], Q[j], P[j]);
				Fp12::Dbl::mul_Fp2_024(f, l);
			}
		}
	}

	Fp6::neg(f.b_, f.b_);

	// addition step
	for (int j = 0; j < n; j++) {
		Fp2::neg(T[j][1], T[j][1]);
		ecop::FrobEndOnTwist_1(Q1, Q[j]);
		Fp6::pointAddLineEval(l, T[j], Q1, P[j]); // 5k
		ecop::FrobEndOnTwist_8(Q1, Q[j]);
		Fp6::pointAddLineEval(l2, T[j], Q1, P[j]); // 5k
		Fp12::Dbl::mul_Fp2_024_Fp2_024(ft, l, l2); // 2.7k
		Fp12::mul(f, f, ft); // 6.4k
	}

	// final exponentiation
	f.final_exp();
}

template<class Fp>
void PrecomputePairingKnownG2(Fp6T<Fp2> l[][70], int n, Fp2T<Fp> QA[][3])
{
	typedef Fp2T<Fp> Fp2;
	typedef Fp6T<Fp2> Fp6;

	Fp  P[2];
	Darray<Fp2, 2> Q(n);
	Darray<Fp2, 3> T(n);

	Fp2 Q1[2];

	int m=0;

	P[0] = P[1] = 1;
	MultiNormalizeFp2(Q,QA,n);

	for (int i = 0; i < n; i++) {
		T[i][0] = Q[i][0];
		T[i][1] = Q[i][1];
		T[i][2] = Fp2(1);
	}

	const int siTbl[] = {
		1,1,0,0,0,
		0,0,1,1,0, 0,0,0,0,0,
		0,0,0,0,0, 0,0,0,0,0,
		0,0,0,0,0, 0,0,0,0,0,
		0,0,0,0,0, 0,0,0,0,0,
		0,0,0,0,0, 0,0,0,0,0,
		0,0,0,0,0, 0,0,1,0,0,
	};

	for (size_t i = 1; i < sizeof(siTbl) / sizeof(*siTbl); i++) {
		for (int j=0;j<n;j++)
			Fp6::pointDblLineEval(l[j][m], T[j], P);
		m++;
		if (siTbl[i]) {
			for (int j = 0; j < n; j++)
				Fp6::pointAddLineEval(l[j][m], T[j], Q[j], P);
			m++;
		}
	}

	// addition step
	for (int j=0; j<n; j++) {
		Fp2::neg(T[j][1], T[j][1]);
		ecop::FrobEndOnTwist_1(Q1, Q[j]);
		Fp6::pointAddLineEval(l[j][m], T[j], Q1, P); // 5k
		ecop::FrobEndOnTwist_8(Q1, Q[j]);
		Fp6::pointAddLineEval(l[j][m+1], T[j], Q1, P); // 5k
	}
}

template<class Fp>
void opt_atePairingKnownG2(Fp12T<Fp6T<Fp2T<Fp> > > &f, Fp6T<Fp2> L[70], const Fp *_P)
{
	typedef Fp2T<Fp> Fp2;
	typedef Fp6T<Fp2> Fp6;
	typedef Fp12T<Fp6> Fp12;

	Fp P[3];
	P[0] = _P[0];
	P[1] = _P[1];
	int m=0;

	const int siTbl[] = {
		1,1,0,0,0,
		0,0,1,1,0, 0,0,0,0,0,
		0,0,0,0,0, 0,0,0,0,0,
		0,0,0,0,0, 0,0,0,0,0,
		0,0,0,0,0, 0,0,0,0,0,
		0,0,0,0,0, 0,0,0,0,0,
		0,0,0,0,0, 0,0,1,0,0,
	};

	Fp6 d, e;
	// at 1.
	d = L[0];
	e = L[1];
	Fp6::mul_Fp_b(d, P[1]); Fp6::mul_Fp_c(d, P[0]);
	Fp6::mul_Fp_b(e, P[1]); Fp6::mul_Fp_c(e, P[0]);
	Fp12::Dbl::mul_Fp2_024_Fp2_024(f, d, e);
	m = 2;

	// loop from 2.
	// 844kclk
	for (size_t i = 2; i < sizeof(siTbl) / sizeof(*siTbl); i++) {
		// 3.6k x 63
		d = L[m];
		m++;
		Fp6::mul_Fp_b(d, P[1]);
		Fp6::mul_Fp_c(d, P[0]);

		// 4.7k x 63
		Fp12::square(f);

		// 4.48k x 63
		Fp12::Dbl::mul_Fp2_024(f, d);

		if (siTbl[i]) {
			// 9.8k x 3
			// 5.1k
			d = L[m];
			m++;
			Fp6::mul_Fp_b(d, P[1]);
			Fp6::mul_Fp_c(d, P[0]);
			Fp12::Dbl::mul_Fp2_024(f, d);
		}
	}

	// addition step

	// @memo z < 0
	Fp6::neg(f.b_, f.b_);

	Fp12 ft;
	d = L[m];
	Fp6::mul_Fp_b(d, P[1]);
	Fp6::mul_Fp_c(d, P[0]);

	e = L[m+1];
	Fp6::mul_Fp_b(e, P[1]);
	Fp6::mul_Fp_c(e, P[0]);

	Fp12::Dbl::mul_Fp2_024_Fp2_024(ft, d, e); // 2.7k
	Fp12::mul(f, f, ft); // 6.4k

	// final exponentiation
	f.final_exp();
}

template<class Fp>
void opt_ateMultiPairingJKnownG2(Fp12T<Fp6T<Fp2T<Fp> > >& f, int n, Fp6T<Fp2> L[][70], Fp PA[][3])
{
	typedef Fp2T<Fp> Fp2;
	typedef Fp6T<Fp2> Fp6;
	typedef Fp12T<Fp6> Fp12;

	Darray<Fp, 2> P(n);
	int m = 0;

	Fp6 l, l2;
	Fp12 ft;
	f =1;

	MultiNormalizeFp(P,PA,n);

	const int siTbl[] = {
		1,1,0,0,0,
		0,0,1,1,0, 0,0,0,0,0,
		0,0,0,0,0, 0,0,0,0,0,
		0,0,0,0,0, 0,0,0,0,0,
		0,0,0,0,0, 0,0,0,0,0,
		0,0,0,0,0, 0,0,0,0,0,
		0,0,0,0,0, 0,0,1,0,0,
	};

	for (size_t i = 1; i < sizeof(siTbl) / sizeof(*siTbl); i++) {
		if (i > 1) Fp12::square(f);
		for (int j = 0; j < n; j++) {
			l=L[j][m];
			Fp6::mul_Fp_b(l, P[j][1]);
			Fp6::mul_Fp_c(l, P[j][0]);
			Fp12::Dbl::mul_Fp2_024(f, l);
		}
		m++;
		if (siTbl[i]==1) {
			for (int j=0;j<n;j++) {
				l=L[j][m];
				Fp6::mul_Fp_b(l, P[j][1]);
				Fp6::mul_Fp_c(l, P[j][0]);
				Fp12::Dbl::mul_Fp2_024(f, l);
			}
			m++;
		}
	}

	Fp6::neg(f.b_, f.b_);

	// addition step
	for (int j = 0; j < n; j++) {
		l = L[j][m];
		Fp6::mul_Fp_b(l, P[j][1]);
		Fp6::mul_Fp_c(l, P[j][0]);

		l2 = L[j][m+1];
		Fp6::mul_Fp_b(l2, P[j][1]);
		Fp6::mul_Fp_c(l2, P[j][0]);

		Fp12::Dbl::mul_Fp2_024_Fp2_024(ft, l, l2); // 2.7k
		Fp12::mul(f, f, ft); // 6.4k
	}

	// final exponentiation
	f.final_exp();
}

/*
 * PA expect to contain the corresponding P parameter for the L (precomputed Q parameters),
 * and QA (unknown Q parameter) in that precise order, n and nu indicates the number of pairs
 * for the L and QA respectively.
 */
template<class Fp>
void opt_atePairingKnownG2Mixed
(Fp12T<Fp6T<Fp2T<Fp> > >& f, int n, Fp6T<Fp2> L[][70], int nu, Fp2T<Fp> QA[][3], Fp PA[][3])
{
	typedef Fp2T<Fp> Fp2;
	typedef Fp6T<Fp2> Fp6;
	typedef Fp12T<Fp6> Fp12;

	Darray<Fp, 2> P(n + nu);
	Darray<Fp2, 2> Q(nu);
	Darray<Fp2, 3> T(nu);

	int m;
	m = 0;

	Fp2 Q1[2];
	Fp6 l, l2;
	Fp12 ft;
	f = 1;

#ifdef BN_USE_MultiNormalizeJacobianFpAndFp2
	MultiNormalizeJacobianFpAndFp2(&P[0][0], &Q[0][0], &PA[0][0], n + nu, &QA[0][0], nu);
#else
	MultiNormalizeFp (P, PA, n + nu);
	MultiNormalizeFp2(Q, QA, nu);
#endif

	for (int i = 0; i < nu; i++) {
		//Fp::neg(P[i][2], P[i][1]);
		T[i][0] = Q[i][0];
		T[i][1] = Q[i][1];
		T[i][2] = Fp2(1);
	}

    const int siTbl[] = {
		1,1,0,0,0,
		0,0,1,1,0, 0,0,0,0,0,
		0,0,0,0,0, 0,0,0,0,0,
		0,0,0,0,0, 0,0,0,0,0,
		0,0,0,0,0, 0,0,0,0,0,
		0,0,0,0,0, 0,0,0,0,0,
		0,0,0,0,0, 0,0,1,0,0,
    };
	for (size_t i = 1; i < sizeof(siTbl) / sizeof(*siTbl); i++) {
		if (i > 1) Fp12::square(f);
		for (int j = 0; j < n; j++) {
			l = L[j][m];
			Fp6::mul_Fp_b(l, P[j][1]);
			Fp6::mul_Fp_c(l, P[j][0]);
			Fp12::Dbl::mul_Fp2_024(f, l);
		}
		m++;
		for (int j = 0; j < nu; j++) {
			Fp6::pointDblLineEval(l, T[j], P[n + j]);
			Fp12::Dbl::mul_Fp2_024(f, l);
		}
		if (siTbl[i]==1) {
			for (int j = 0; j < n; j++) {
				l = L[j][m];
				Fp6::mul_Fp_b(l, P[j][1]);
				Fp6::mul_Fp_c(l, P[j][0]);
				Fp12::Dbl::mul_Fp2_024(f, l);
			}
			m++;
			for (int j = 0; j < nu; j++) {
				Fp6::pointAddLineEval(l, T[j], Q[j], P[n + j]);
				Fp12::Dbl::mul_Fp2_024(f, l);
			}
		}
	}

	Fp6::neg(f.b_, f.b_);

	// addition step
	for (int j = 0; j < n; j++) {
		l = L[j][m];
		Fp6::mul_Fp_b(l, P[j][1]);
		Fp6::mul_Fp_c(l, P[j][0]);

		l2 = L[j][m+1];
		Fp6::mul_Fp_b(l2, P[j][1]);
		Fp6::mul_Fp_c(l2, P[j][0]);

		Fp12::Dbl::mul_Fp2_024_Fp2_024(ft, l, l2); // 2.7k
		Fp12::mul(f, f, ft); // 6.4k
	}
	for (int j = 0; j < nu; j++) {
		Fp2::neg(T[j][1], T[j][1]);
		ecop::FrobEndOnTwist_1(Q1, Q[j]);
		Fp6::pointAddLineEval(l, T[j], Q1, P[n + j]);

		ecop::FrobEndOnTwist_8(Q1, Q[j]);
		Fp6::pointAddLineEval(l2, T[j], Q1, P[n + j]);

		Fp12::Dbl::mul_Fp2_024_Fp2_024(ft, l, l2);
		Fp12::mul(f, f, ft);
	}

	// final exponentiation
	f.final_exp();
}

} // bn

/***
	Local Variables:
	c-basic-offset: 4
	indent-tabs-mode: t
	tab-width: 4
	End:
*/
