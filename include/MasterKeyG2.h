#pragma once
/**
	@file
	@brief Master Secret Key (G2 version)
    @author Zavattoni et al.
	@license modified new BSD license
	http://www.opensource.org/licenses/BSD-3-Clause
*/

#include "cpabe.h"

class MasterKeyG2 {
public:
	void SetGalpha(const Fp2 g_alpha[3]);
	void getG_alpha(Fp2& x, Fp2& y, Fp2& z) const;

	friend std::ostream& operator<<(std::ostream& os, const MasterKeyG2& mk);
	friend std::istream& operator>>(std::istream& is, MasterKeyG2& mk);
private:
	Fp2 g_alpha[3];
};


