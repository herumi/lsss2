#pragma once
/**
	@file
	@brief Setup and KeyGen steps (G22 version)
    @author Zavattoni et al.
	@license modified new BSD license
	http://www.opensource.org/licenses/BSD-3-Clause
*/

#include "cpabe.h"
#include "PrecomputeG2.h"
#include "PublicKeyG2.h"
#include "MasterKeyG2.h"
#include "Bob.h"
#include "KeyG2.h"

class ServerG2 {
public:
	void Setup(PublicKeyG2& PK, PrecomputeG2& prec);
	void KeyGen(KeyG2& UserKey, const Bob& b, PublicKeyG2* PK, PrecomputeG2* prec);
	void save(std::ostream& of)
	{
		of << MK;
	};
	void save(const char* filename)
	{
		std::ofstream fs(filename);
		fs << MK;
	};
	void load(std::istream& of)
	{
		of >> MK;
	}
	void load(const char* filename)
	{
		std::ifstream fs(filename);
		fs >> MK;
	}
private:
	MasterKeyG2 MK;
};

/***
    Local Variables:
    c-basic-offset: 4
    indent-tabs-mode: t
    tab-width: 4
    End:
*/
