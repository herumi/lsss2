/**
	@file
	@brief General namespaces
    @author Zavattoni et al.
	@license modified new BSD license
	http://www.opensource.org/licenses/BSD-3-Clause
*/
#ifndef ATE_INCLUDE_PARAM_H_
#define ATE_INCLUDE_PARAM_H_
/**
	@file
	@brief parameter for BN
*/
#include "zm.h"

namespace mie {
namespace bn {


}
} // mie::bn

#endif
