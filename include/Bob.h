#pragma once
/**
	@file
	@brief Decryption and Delegate steps
    @author Zavattoni et al.
	@license modified new BSD license
	http://www.opensource.org/licenses/BSD-3-Clause
*/

#include "Key.h"
#include "CypherText.h"
#include "MatrizLSSS.h"
#include "Precompute.h"

#include "KeyG2.h"
#include "CypherTextG2.h"
#include "PrecomputeG2.h"
#include "PublicKeyG2.h"

class Bob {
public:
	void set(int, char**);
	void setAtr(const std::vector<std::string>& attr)
	{
		this->attr = attr;
	}
	const std::vector<std::string>& getAtr() const
	{
		return attr;
	}
	static Fp dec(CypherText* CT, const MatrizType& mlsss, const Key& SK, const Precompute& precompute, MatrizLSSS* M);
	void delegate(Key* key, const PublicKey& PK, const Key& UserKey, const std::vector<std::string>& S, const Precompute& precompute);
	static Fp decG2(CypherTextG2* CT, const MatrizType& mlsss, const KeyG2& SK, const PrecomputeG2& precompute, MatrizLSSS* M);
	void delegateG2(KeyG2* key, const PublicKeyG2& PK, const KeyG2& UserKey, const std::vector<std::string>& S, const PrecomputeG2& precompute);
private:
	std::vector<std::string> attr;
};

