#-----------------------------------------------------------------------
#  Makefile for cpabe

.PHONY: all clean LSSS cleanall mrproper

#  Compiler
#CC=gcc
#CPP=g++

#CFLAGS= -W -Wall -ansi -g  -Iinclude
LDFLAGS=
EXEC=cpabe

# Target
#all:  arbre libzm $(EXEC)

all: LSSS
	echo "OK"
#arbre

#arbre :
#	make -C policy

libzm:
	make -C src

LSSS:
	make -C protocole


cleanall: clean
	-make -C src clean
#	make -C policy clean

clean:
	-$(MAKE) -C protocole clean
	-$(MAKE) -C bin clean
#	make -C policy clean

mrproper: clean
	rm -f $(EXEC)

miracl/miracl.a :
	cd miracl && bash linux64
test:
	$(MAKE) -C bin test
test6:
	$(MAKE) -C bin test6
test20:
	$(MAKE) -C bin test20
testG2:
	$(MAKE) -C bin testG2
test6G2:
	$(MAKE) -C bin test6G2
test20G2:
	$(MAKE) -C bin test20G2
